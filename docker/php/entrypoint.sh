#!/bin/bash

php /usr/share/nginx/html/artisan migrate --seed
php /usr/share/nginx/html/artisan jwt:secret

/usr/bin/supervisord
