<?php

namespace App;

use App\Mail\ResetpasswordNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    public const ROLE_USER = 'user';
    public const ROLE_TUTOR = 'tutor';

    use Notifiable;

    public $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'profession',
        'name',
        'email',
        'password',
        'avatar',
        'phone',
        'skype',
        'whatsapp',
        'birthday',
        'role',
        'chek_payment',
        'date_payment',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        Mail::to($this->email)->queue(
            new ResetpasswordNotification($this->email, $token)
        );
    }

    public function isRoleUser(): bool
    {
        return $this->role === self::ROLE_USER;
    }

    public function isRoleTutor(): bool
    {
        return $this->role === self::ROLE_TUTOR;
    }
}
