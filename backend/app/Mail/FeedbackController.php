<?php

namespace App\Mail;

use App\Http\Models\Client;
use App\Http\Models\Meeting;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FeedbackController extends Mailable
{
    use Queueable, SerializesModels;

    /** @var User */



    /**
     * Create a new message instance.



     * @return void
     */
    public function __construct()
    {


    }


    public function build()
    {
        return $this->subject('Новая встреча запланирована!')
            ->markdown('mails.feedbackConfirmation')->with([
                'user' => 'test',

            ]);
    }
}
