<?php

namespace App\Mail;

use App\Http\Models\Client;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResetpasswordNotification extends Mailable
{
    use Queueable, SerializesModels;

    private $email;
    private $token;

    /**
     * Create a new message instance.
     * @param Client $client
     * @param User $user
     * @return void
     */
    public function __construct($email, $token)
    {
        $this->email = $email;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mails.resetPasswordNotification')
            ->subject('Восстановление пароля')
            ->with([
                'token' => env('APP_URL') . 'password-reset?token='
                    . $this->token . '&email=' . $this->email
            ]);
    }
}
