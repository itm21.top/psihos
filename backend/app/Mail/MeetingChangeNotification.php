<?php

namespace App\Mail;

use App\Http\Models\Client;
use App\Http\Models\Meeting;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MeetingChangeNotification extends Mailable
{
    use Queueable, SerializesModels;

    /** @var User */
    private $user;
    /** @var Client */
    private $client;
    /** @var Meeting */
    private $meeting;

    /**
     * Create a new message instance.
     * @param Client $client
     * @param User $user
     * @param Meeting $meeting
     * @return void
     */
    public function __construct(User $user, Client $client, Meeting $meeting)
    {
        $this->user = $user;
        $this->client = $client;
        $this->meeting = $meeting;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Встреча перенесена!')
            ->markdown('mails.meetingChangeNotification')->with([
            'user' => $this->user,
            'client' => $this->client,
            'meeting' => $this->meeting
        ]);
    }
}
