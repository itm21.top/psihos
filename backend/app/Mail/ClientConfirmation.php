<?php

namespace App\Mail;

use App\Http\Models\Client;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ClientConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    /** @var User */
    private $user;
    /** @var Client */
    private $client;

    /**
     * Create a new message instance.
     * @param Client $client
     * @param User $user
     * @return void
     */
    public function __construct(User $user, Client $client)
    {
        $this->user = $user;
        $this->client = $client;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Приглашение от терапевта!')
            ->markdown('mails.clientConfirmation')->with([
            'user' => $this->user,
            'client' => $this->client
        ]);
    }
}
