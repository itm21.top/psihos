<?php

namespace App\Http\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Meeting extends Model
{
    use SoftDeletes;

    public $table = 'meetings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'duration',
        'tutor_id',
        'client_id',
        'scheduled_for',
        'periodic_type',
        'grouping_type',
        'type',
        'notify_by_sms',
        'notify_by_email',
        'is_visited',
        'is_fixed',
        'namelast',
        'paymeet',
        'freemeet',
        'scheduled_last',
    ];

    public function client()
    {
        return $this->belongsTo(Client::class)->withTrashed();
    }

    public function tutor()
    {
        return $this->belongsTo(User::class, 'user_id', 'tutor_id');
    }

    public function files()
    {
        return $this->hasMany(MeetingFile::class);
    }

}
