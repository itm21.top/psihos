<?php

namespace App\Http\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class TaskComment extends Model
{
    public $table = 'task_comments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'task_id',
        'user_id',
        'text',
        'created_by',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
