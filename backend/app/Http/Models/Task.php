<?php

namespace App\Http\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;

    public $table = 'tasks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tutor_id',
        'client_id',
        'name',
        'is_complete',
        'created_by',
        'deadline_date',
    ];

    public function client()
    {
        return $this->belongsTo(Client::class)->withTrashed();
    }

    public function tutor()
    {
        return $this->belongsTo(User::class, 'user_id', 'tutor_id');
    }

    public function comments()
    {
        return $this->hasMany(TaskComment::class);
    }

}
