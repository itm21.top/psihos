<?php

namespace App\Http\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserSocials extends Model
{
    const SOCIAL_PROVIDER_GOOGLE = 'google';
    const SOCIAL_PROVIDER_FACEBOOK = 'facebook';

    public $table = 'user_socials';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'social_id', 'social',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
