<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{

    public $table = 'password_resets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'token',
    ];
}
