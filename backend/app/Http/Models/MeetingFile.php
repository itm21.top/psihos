<?php

namespace App\Http\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class MeetingFile extends Model
{
    public $table = 'meeting_files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'meeting_id',
        'name',
        'file_name',
    ];
}
