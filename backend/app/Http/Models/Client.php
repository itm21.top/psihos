<?php

namespace App\Http\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;
    public $table = 'clients';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tutor_id',
        'invite_code',
        'user_id',
        'full_name',
        'phone',
        'skype',
        'whatsapp',
        'email',
        'notice',
        'is_confirmed',
        'is_archive',
        'avatar',
        'birthday',
        'paid_meetings',

    ];

    protected $casts = [
        'created_at'  => 'date:d.m.Y',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function meetings()
    {
        return $this->hasMany(Meeting::class)
            ->orderBy('is_fixed', 'DESC')
            ->orderBy('scheduled_for', 'DESC');
    }

    public function tasks()
    {
        return $this->hasMany(Task::class)
            ->orderBy('is_complete', 'asc')
            ->orderBy('id', 'desc');
    }

    public function latestMeeting()
    {
        return $this->hasOne(Meeting::class)
            ->latest();
    }

    public function closestMeeting()
    {
        return $this->hasOne(Meeting::class)
            ->where('scheduled_for','>' , date('Y-m-d H:i:s'))
            ->orderBy('scheduled_for', 'asc')
            ->latest();
    }
}
