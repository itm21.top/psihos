<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Models\Client;
use App\Http\Models\Meeting;
use App\Http\Models\MeetingFile;
use App\Mail\MeetingChangeNotification;
use App\Mail\MeetingNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use \JWTAuth;

class MeetingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meetings = Meeting::with(['client', 'files'])->orderBy('id', 'desc')->get();
        return response()->json($meetings);
    }

    /**
     * Store a newly created resource in storage.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = Client::find($request->get('client_id'));
        $meeting = Meeting::create([
            'tutor_id' => Auth::user()->id,
            'client_id' => $request->get('client_id'),
            'scheduled_for' => $request->get('scheduled_for'),
            'type' => $request->get('type'),
            'name' => $request->get('name'),
            'namelast' => $request->get('namelast'),
            'duration' => $request->get('duration'),
            'is_visited' => $request->get('is_visited'),
            'notify_by_email' => $request->get('notify_by_email'),
            'paymeet' => $request->get('paymeet'),
            'freemeet' => $request->get('freemeet'),
            'scheduled_last' => $request->get('scheduled_last'),

        ]);

        $uploadFiles = $request->file('files');
        if ($uploadFiles) {
            foreach ($uploadFiles as $uploadFile) {
                $hashedFileName = $this->hashFileName($uploadFile->getClientOriginalExtension());
                if ($uploadFile->storeAs(
                    $this->getFilePath($meeting->id),
                    $hashedFileName
                )) {
                    MeetingFile::create([
                        'meeting_id' => $meeting->id,
                        'name' => $uploadFile->getClientOriginalName(),
                        'file_name' => $hashedFileName,
                    ]);
                }
            }
        }
        if ($meeting->notify_by_email) {
            Mail::to($client->email)->queue(
                new MeetingNotification(Auth::user(), $client, $meeting)
            );
        }
        $client->paid_meetings > 0 && $client->paid_meetings--;
        $client->save();
        return response()->json($meeting);
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $meeting = Meeting::with('client', 'files')->find($id);
        return response()->json($meeting);
    }

    /**
     * Update the specified resource in storage.
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $meeting = Meeting::find($id);
        $z = '';
        $z2 = '';
        if($request->get('name')!=''&&$request->get('name')!='null')
        {
            $z = $request->get('name');
        }
        if($request->get('namelast')!=''&&$request->get('namelast')!='null')
        {
            $z2 = $request->get('namelast');
        }
        
        $meeting->update([
            'scheduled_for' => $request->get('scheduled_for'),
            'type' => $request->get('type'),
            'is_visited' => $request->get('is_visited'),
            'name' => $z,
            'namelast' => $z2,
            'duration' => $request->get('duration'),
            'is_fixed' => $request->get('is_fixed', 0),
            'notify_by_email' => $request->get('notify_by_email', 0),
            'paymeet' => $request->get('paymeet', 0),
            'freemeet' => $request->get('freemeet', 0),
            'scheduled_last' => $request->get('scheduled_last'),
        ]);

        $uploadFiles = $request->file('files');
        if ($uploadFiles) {
            foreach ($uploadFiles as $uploadFile) {
                $hashedFileName = $this->hashFileName($uploadFile->getClientOriginalExtension());
                if ($uploadFile->storeAs(
                    $this->getFilePath($meeting->id),
                    $hashedFileName
                )) {
                    MeetingFile::create([
                        'meeting_id' => $meeting->id,
                        'name' => $uploadFile->getClientOriginalName(),
                        'file_name' => $hashedFileName,
                    ]);
                }
            }
        }
        if ($meeting->notify_by_email) {
            Mail::to($meeting->client->email)->queue(
                new MeetingChangeNotification(Auth::user(), $meeting->client, $meeting)
            );
        }

        return $this->show($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $meeting = Meeting::find($id);
        foreach ($meeting->files as $file) {
            $file->delete();
            unlink(storage_path('app/' . $this->getFilePath($id) . '/' . $file->file_name));
        }
        $meeting->delete();
    }

    /**
     * @param string $extension file extension
     * @return string
     */
    protected function hashFileName($extension)
    {
        return uniqid() . '.' . $extension;
    }

    protected function getFilePath($meetingId)
    {
        return 'public/meeting/' . $meetingId;
    }

    public function getFile($meetingId, $fileId)
    {
        $file = MeetingFile::find($fileId);
        return response()->download(
            storage_path('app/' . $this->getFilePath($meetingId) . '/' . $file->file_name),
            $file->name,
            ['filename' => $file->name]
        );
    }

    public function deleteFile($meetingId, $fileId)
    {
        $file = MeetingFile::find($fileId);
        $file->delete();
        unlink(storage_path('app/' . $this->getFilePath($meetingId) . '/' . $file->file_name));
        return response()->json(['status' => 'OK']);
    }

    public function getClientsList() {
        $user = JWTAuth::parseToken()->authenticate();
        $clients = Client::where('tutor_id', $user->id)
            ->select('full_name', 'id', 'deleted_at', 'is_archive')
            ->withTrashed()
            ->get();
        return response()->json($clients);
    }
}
