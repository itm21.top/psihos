<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Models\Client;
use App\Http\Models\Meeting;
use App\Http\Services\DataTable;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;
use JWTAuth;

class CalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fromDate = $request->get('from', date('Y-m-d'));
        $toDate = $request->get('to', date('Y-m-t'));

        $user = JWTAuth::parseToken()->authenticate();
        $startOfMonth = strtotime($fromDate . ' 00:00:00');
        $endOfMonth = strtotime($toDate . ' 23:59:59');

        $meetings = Meeting::with(['client', 'files'])
            ->whereBetween('scheduled_for', [
                date('Y-m-d H:i:s', $startOfMonth),
                date('Y-m-d H:i:s', $endOfMonth)
            ])
            ->orderBy('scheduled_for', 'asc');

        if (Auth::user()->role === User::ROLE_TUTOR) {
            $meetings->where(['tutor_id' => Auth::user()->id]);
        } else {
            $clientsIds = Client::where('user_id', Auth::user()->id)->get()->pluck('id')->all();
            $meetings->whereIn('client_id', $clientsIds);
        }

        return $meetings->get();
    }

    public function meetingsList(DataTable $dataTable, Request $request)
    {
        if ($request->get('filters')) {
            $filters = $request->get('filters');
            $fromDate = isset($filters['from']) ? $filters['from'] : date('Y-m-01');
            $toDate   = isset($filters['to']) ? $filters['to'] : date('Y-m-t');
        }

        $user = JWTAuth::parseToken()->authenticate();
        $startOfMonth = strtotime($fromDate . ' 00:00:00');
        $endOfMonth = strtotime($toDate . ' 23:59:59');

        $countSelect = '(select count(*) from meeting_files where meeting_files.meeting_id = meetings.id) as files_count';
        $meetings = Meeting::with(['client', 'files'])
            ->select(DB::raw('meetings.*, ' . $countSelect))
            ->join('clients as client', 'meetings.client_id', '=', 'client.id')
            ->whereBetween('meetings.scheduled_for', [
                date('Y-m-d H:i:s', $startOfMonth),
                date('Y-m-d H:i:s', $endOfMonth)
            ]);

        if (Auth::user()->role === User::ROLE_TUTOR) {
            $meetings->where(['meetings.tutor_id' => Auth::user()->id]);
        } else {
            $clientsIds = Client::where('user_id', Auth::user()->id)->get()->pluck('id')->all();
            $meetings->whereIn('meetings.client_id', $clientsIds);
        }

        return $dataTable
            ->of($meetings);
    }
}
