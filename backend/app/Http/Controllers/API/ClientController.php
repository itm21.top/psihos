<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Models\Client;
use App\Http\Services\DataTable;
use App\Mail\ClientConfirmation;
use App\Mail\FeedbackController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use JWTAuth;

class ClientController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(DataTable $dataTable, Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $clients = Client::with('user', 'latestMeeting', 'closestMeeting', 'tasks')
            ->where('tutor_id', $user->id)
            ->addSelect(['latest_meeting' => function ($query) {
                $query->from('meetings')
                    ->select('scheduled_for')
                    ->orderBy('scheduled_for', 'desc')
                    ->whereColumn('client_id', 'clients.id')
                    ->limit(1);
            }])
            ->addSelect(['closest_meeting' => function ($query) {
                $query->from('meetings')
                    ->select('scheduled_for')
                    ->orderBy('scheduled_for', 'asc')
                    ->whereColumn('client_id', 'clients.id')
                    ->where('scheduled_for', '>', date('Y-m-d H:i:s'))
                    ->limit(1);
            }]);
        $filters = $request->get('filters');
        if ($filters) {
            if ($filters['archive']) {
                $clients->withTrashed();
            } else {
                $clients->where('is_archive', false);
            }
        }

        return $dataTable
            ->setSearchable(['full_name', 'phone'])
            ->of($clients);
    }

    /**
     * Store a newly created resource in storage.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:clients',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $client = Client::create([
            'tutor_id' => Auth::user()->id,
            'email' => $request->get('email'),
            'full_name' => $request->get('full_name'),
            'phone' => $request->get('phone'),
            'skype' => $request->get('skype'),
            'whatsapp' => $request->get('whatsapp'),
            'birthday' => $request->get('birthday'),
            'chek_payment' => $request->get('chek_payment'),
            'date_payment' => $request->get('date_payment'),
            'invite_code' => Str::random(8)
        ]);
        Mail::to($client->email)->queue(
            new ClientConfirmation(Auth::user(), $client)
        );
        return response()->json($client);
    }

    public function feedback(Request $request)
    {

        Mail::to('itm21.top@gmail.com')->queue(
            new FeedbackController()
        );
        return response()->json($request);
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query = Client::withTrashed()
            ->with(['user', 'meetings', 'meetings.files', 'closestMeeting', 'tasks', 'tasks.comments']);
        if ($id === 'me') {
            $client = $query->where('user_id', Auth::user()->id)->first();
        } else {
            $client = $query->find($id);
        }
        return response()->json($client->toArray());
    }

    /**
     * Update the specified resource in storage.
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $clientData = $request->only([
            'full_name',
            'phone',
            'birthday',
            'is_archive',
            'skype',
            'whatsapp',
            'paid_meetings',
        ]);
        $client = Client::find($id);
        $client->update($clientData);

        return $this->show($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::find($id);
        $result = $client->delete();
        response()->json([$result]);
    }

    /**
     * Update the specified resource in storage.
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAvatar(Request $request, $clientId)
    {
        $avatarFile = $request->file('avatar');
        $client = Client::find($clientId);

        $hashedFileName = $this->hashFileName($avatarFile->getClientOriginalExtension());
        $filePath = storage_path('app/public/avatar/client' . $clientId . '/');

        File::isDirectory($filePath) || File::makeDirectory($filePath, 0777, true, true);

        $image = Image::make($avatarFile)
            ->resize(100, 100)
            ->save($filePath . $hashedFileName);

        $client->update(['avatar' => '/public/avatar/client' . $client->id . '/' . $hashedFileName]);
        return response()->json(compact('client'));
    }

    /**
     * @param string $extension file extension
     * @return string
     */
    protected function hashFileName(string $extension): string
    {
        return uniqid('', true) . '.' . $extension;
    }
}
