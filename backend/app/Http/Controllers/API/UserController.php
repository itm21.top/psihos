<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use JWTAuth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::get();
        return response()->json($users);
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userData = $request->only([
            'name',
            'email',
            'phone',
            'skype',
            'whatsapp',
            'birthday',
            'profession',
            'chek_payment',
            'date_payment',
        ]);
        if ($id === 'me') {
            $user = JWTAuth::parseToken()->authenticate();
        } else {
            $user = User::find($id);
        }

        $user->update($userData);
        return response()->json(compact('user'));
    }

    public function updatePayment(Request $request)
    {
        $userData = $request->only([
            'chek_payment',
            'date_payment',
        ]);

        $user = User::where('email',$request['email']) -> first();
        print_r($user);
        if($user)
        {
             $user->update($userData);
        }
        else
        {
             User::create([
                 'name' => '',
                 'email' => $request['email'],
                 'password' => Hash::make($request['password']),
                 'role' => 'tutor',
            ]);

            $user = User::where('email',$request['email']) -> first();
            print_r($user);
            if($user)
            {
                $user->update($userData);
            }
        }



        //return response()->json(compact('user'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function updateAvatar(Request $request)
    {
        $avatarFile = $request->file('avatar');
        $user = JWTAuth::parseToken()->authenticate();

        $hashedFileName = $this->hashFileName($avatarFile->getClientOriginalExtension());
        $filePath = storage_path('app/public/avatar/' . $user->id . '/');

        File::isDirectory($filePath) || File::makeDirectory($filePath, 0777, true, true);

        $image = Image::make($avatarFile)
            ->resize(100, 100)
            ->save($filePath . $hashedFileName);

        $user->update(['avatar' => '/public/avatar/' . $user->id . '/' . $hashedFileName]);
        return response()->json(compact('user'));
    }

    /**
     * @param string $extension file extension
     * @return string
     */
    protected function hashFileName($extension)
    {
        return uniqid() . '.' . $extension;
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
