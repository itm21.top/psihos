<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Models\Task;
use App\Http\Models\TaskComment;
use App\Http\Services\DataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \JWTAuth;
use phpDocumentor\Reflection\Types\Integer;

class TaskCommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return [];
    }

    /**
     * Store a newly created resource in storage.
     * @param int $taskId
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(int $taskId, Request $request)
    {
        $task = TaskComment::create([
            'task_id' => $taskId,
            'user_id' => Auth::user()->id,
            'text' => $request->get('text'),
        ]);

        return response()->json($task);
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = TaskComment::with('client', 'comments')->find($id);
        return response()->json($task);
    }

    /**
     * Update the specified resource in storage.
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $taskId, $id)
    {
        $task = TaskComment::find($id);
        $task->update([
            'text' => $request->get('text'),
        ]);

        return response()->json($task);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $taskId, $id)
    {
        $task = TaskComment::find($id);
        $task->delete();
    }

}
