<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Models\Client;
use App\Http\Models\Task;
use App\Http\Services\DataTable;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \JWTAuth;

class TaskController extends Controller
{

    /**
     * @param $tasks
     * @param Request $request
     */
    private function addFilters($tasks, Request $request): void
    {
        $filters = $request->get('filters');
        if ($filters) {
            if (!array_key_exists('completed', $filters) || !$filters['completed']) {
                $tasks->where('tasks.is_complete', 0);
            }

            if ($filters['dateRange']) {
                switch ($filters['dateRange']) {
                    case 'WEEK':
                        $tasks->whereBetween('tasks.created_at',[
                            date('Y-m-d 00:00:00', strtotime('-7 Days')),
                            date('Y-m-d 23:59:59')
                        ]);
                        break;
                    case 'TODAY':
                        $tasks->whereBetween('tasks.created_at',[
                            date('Y-m-d 00:00:00'),
                            date('Y-m-d 23:59:59')
                        ]);
                        break;
                    case 'ALL':
                    default:
                        break;
                }
            }
        }
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(DataTable $dataTable, Request $request)
    {
        $tasks = Task::with(['client', 'comments'])
            ->select(DB::raw('tasks.*'))
            ->addSelect(['comments_number' => function ($query) {
                $query->from('task_comments')
                    ->select(DB::raw('count(*)'))
                    ->whereColumn('task_id', 'tasks.id');
            }])

            ->leftJoin('clients as client', 'tasks.client_id', '=', 'client.id');

        if (Auth::user()->role === User::ROLE_TUTOR) {
            $tasks->where(['tasks.tutor_id' => Auth::user()->id]);
        } else {
            $clientsIds = Client::where('user_id', Auth::user()->id)->get()->pluck('id')->all();
            $tasks->whereIn('client_id', $clientsIds);
        }
        $this->addFilters($tasks, $request);

        return $dataTable
            ->setSearchable(['client.full_name', 'tasks.name'])
            ->of($tasks);
    }

    /**
     * Store a newly created resource in storage.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = Client::find($request->get('client_id'));
        $task = Task::create([
            'tutor_id' => $request->get('tutor_id'),
            'created_by' => Auth::user()->id,
            'client_id' => $client ? $client->id : 0,
            'name' => $request->get('name'),
            'deadline_date' => $request->get('deadline_date'),
        ]);

        return response()->json($task);
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::with('client', 'comments')->find($id);
        return response()->json($task);
    }

    /**
     * Update the specified resource in storage.
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::find($id);
        $task->update([
            'name' => $request->get('name'),
            'deadline_date' => $request->get('deadline_date'),
            'is_complete' => $request->get('is_complete'),
            'client_id' => $request->get('client_id'),
        ]);

        return response()->json($task);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);
        $task->delete();
    }

}
