<?php

namespace App\Http\Controllers;

use App\Http\Models\Client;
use App\User;
use App\Http\Models\PasswordReset;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    use SendsPasswordResetEmails;

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $credentials2 = $request->only('profession');

        if (empty(User::where(['email' => $credentials['email']])->first())) {
            return response()->json(['error' => 'unknown_user'], 400);
        }



        if (empty(User::where(['profession' => $credentials2['profession']])->first())) {
            return response()->json(['error' => 'invalid_tutor'], 400);
        }

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_password'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $user = JWTAuth::setToken($token)->authenticate();

        if($credentials2['profession']!='')
        {
            if ($user->profession=='Клиент') {
                return response()->json(['error' => 'invalid_tutor'], 400);
            }
        }
        else
        {

            if ($user->profession=='Специалист') {
                return response()->json(['error' => 'invalid_tutor2'], 400);
            }
        }

        return response()->json(compact('user', 'token'));
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'role' => 'required|in:' . User::ROLE_TUTOR . ',' . User::ROLE_USER,
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::create([
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'role' => $request->get('role')
        ]);

        if (!empty($request->get('code'))) {
            $client = Client::where('invite_code', $request->get('code'))->first();
            if ($client) {
                $client->user_id = $user->id;
                $client->is_confirmed = 1;
                $client->save();

                $user->name = $client->full_name;
                $user->save();
            }
        }
        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user', 'token'), 201);
    }

    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }

        return response()->json(compact('user'));
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        $response = $this->broker()->sendResetLink(
            $this->credentials($request)
        );

        if ($response != Password::RESET_LINK_SENT) {
            throw ValidationException::withMessages([
                'email' => [trans($response)],
            ]);
        }
        return response()->json(['message' => $response]);
    }

    public function resetPassword(Request $request)
    {
        $user = null;
        $passwordReset = PasswordReset::where([
            'email' => $request->get('email'),
        ])->firstOrFail();

        if (Hash::check($request->get('token'), $passwordReset->token)) {
            $user = User::where('email', $request->get('email'))->first();
            $user->password = Hash::make($request->get('password'));
            $user->save();

            PasswordReset::where([
                'email' => $request->get('email'),
            ])->delete();
        }
        return response()->json(['user' => $user]);
    }

    /**
     * @param Request $request
     */
    public function getClientByCode(Request $request)
    {
        $code = $request->get('code', false);
        $client = Client::where('invite_code', $code)->first();
        if (!$code || !$client) {
            abort(404, 'Client not found');
        }

        return response()->json([
            'email' => $client->email,
            'full_name' => $client->full_name,
            'code' => $client->invite_code,
        ]);
    }

}
