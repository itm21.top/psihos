<?php

namespace App\Http\Controllers;

use App\Http\Models\UserSocials;
use App\User;
use Illuminate\Support\Facades\DB;
use Laravel\Socialite\Facades\Socialite;
use JWTAuth;

class GoogleController extends Controller
{
    public function getLoginUrl()
    {
        return response()->json([
            'url' => Socialite::driver('google')->stateless()->redirect()->getTargetUrl(),
        ]);
    }

    public function loginCallback()
    {
        try {
            $socialUser = Socialite::driver('google')->stateless()->user();
        } catch (\Exception $e) {
            return response()->json(['error' => 'Lib error'])->status(500);
        }

        $user = User::firstOrCreate(
            ['email' => $socialUser->email],
            [
                'name' => $socialUser->getName(),
                'avatar' => $socialUser->avatar,
                'role' => User::ROLE_TUTOR
            ]
        );

        $socialAccount = UserSocials::firstOrCreate(
            [
                'user_id' => $user->id,
                'social_id' => $socialUser->getId(),
                'social_provider' => UserSocials::SOCIAL_PROVIDER_GOOGLE,
            ], [
                'social_name' => $socialUser->getName()
            ]
        );

        $token = JWTAuth::fromUser($user);
        return response()->json(compact('user', 'token', 'socialUser'));
    }
}
