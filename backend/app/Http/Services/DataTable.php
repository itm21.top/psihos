<?php


namespace App\Http\Services;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataTable
{
    protected $request;
    protected $searchable = [];

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function of($objects)
    {
        $this->sort($objects);
        $this->search($objects);

        $recordsTotal = $objects->count();
        $this->paginate($objects);
        $data = $objects->get();
        return [
            'draw' => $this->request->get('draw', 0),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => count($data),
            'data' => $data,
            'input' => $this->request->all()
        ];
    }

    public function setSearchable(array $searchable) {
        $this->searchable = $searchable;
        return $this;
    }

    protected function appendSearchQuery(
        $query,
        $col,
        $value,
        $regex = false,
        $first = false
    )
    {
        $appendType = $first ? 'where' : 'orWhere';
        if ($regex) {
            $query->{$appendType}($col, 'LIKE', '%' . $value . '%');
        } else {
            $query->{$appendType}($col, 'REGEXP', $value);
        }
    }

    protected function search(Builder $builder)
    {
        $columns = $this->request->get('columns', null);
        $search = $this->request->get('search', null);
        if ($columns
            && is_array($columns)
            && !empty($search['regex'])
            && !empty($search['value'])
        ) {
            $colsForSearch = empty(!$this->searchable)
                ? $this->searchable
                : array_reduce($columns, function ($acc, $column) {
                    if (!empty($column['searchable'])) {
                        $acc[] = $column['data'];
                    }
                    return $acc;
                }, []);

            if (!empty($colsForSearch)) {
                $builder->where(function ($query) use ($colsForSearch, $search) {
                    foreach ($colsForSearch as $key => $col) {
                        $this->appendSearchQuery(
                            $query,
                            $col,
                            $search['value'],
                            $search['regex'],
                            $key === 0
                        );
                    }
                    return $query;
                });
            }
        }
    }

    protected function paginate(Builder $builder)
    {
        $builder->limit($this->request->get('length', 0));
        $builder->offset($this->request->get('start', 0));
    }

    protected function sort(Builder $builder)
    {
        $orderData = $this->request->get('order', null);
        $columns = $this->request->get('columns', null);
        if (is_array($orderData)) {
            foreach ($orderData as $order) {
                if  (key_exists('column', $order) && is_array($columns)) {
                    if (!empty($columns[$order['column']]['data'])) {
                        $builder->orderBy($columns[$order['column']]['data'], $order['dir']);
                    }
                }
            }
        }
    }
}
