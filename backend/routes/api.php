<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('get-client-by-code', 'AuthController@getClientByCode');

Route::post('/pay/success', 'API\UserController@updatePayment');
Route::get('/feedback', 'API\ClientController@feedback');

Route::post('reset-password', 'AuthController@resetPassword');
Route::post('send-reset-link', 'AuthController@sendResetLinkEmail');
Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@authenticate');

Route::get('google/login', 'GoogleController@getLoginUrl');
Route::get('google/callback', 'GoogleController@loginCallback');

Route::get('facebook/login', 'FacebookController@getLoginUrl');
Route::get('facebook/callback', 'FacebookController@loginCallback');

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('/', 'HomeController@index')->name('main');
    Route::get('me', 'AuthController@getAuthenticatedUser');
    Route::post('me/avatar', 'API\UserController@updateAvatar');

    Route::post('client/{clientId}/avatar', 'API\ClientController@updateAvatar');
    // meeting files
    Route::get('meeting/{meetingId}/file/{fileId}', 'API\MeetingController@getFile');
    Route::delete('meeting/{meetingId}/file/{fileId}', 'API\MeetingController@deleteFile');
    Route::get('meeting/clients', 'API\MeetingController@getClientsList');

    Route::get('calendar/list', 'API\CalendarController@meetingsList');

    Route::apiResources([
        'user' => 'API\UserController',
        'client' => 'API\ClientController',
        'meeting' => 'API\MeetingController',
        'calendar' => 'API\CalendarController',
        'task/{taskId}/comment' => 'API\TaskCommentsController',
        'task' => 'API\TaskController',
    ]);
});
