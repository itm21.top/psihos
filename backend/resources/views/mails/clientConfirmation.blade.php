@component('mail::message')
    <p> Добрый день! </p>
    <p> Терапевт <?=$user->name?> хочет добавить Вас в нашу систему. </p>
    @component('mail::button', ['url' => env('APP_URL') . '/client-confirmation?code=' . $client->invite_code])
        Принять приглашение
    @endcomponent
    С Уважением <br/> <?= env('APP_NAME') ?>
@endcomponent
