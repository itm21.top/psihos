@component('mail::message')
    <p> Добрый день! </p>
    <p> Терапевт <?=$user->name?> назначил новую встречу. </p>
    <p> <?= date('Y-m-d H:i', strtotime($meeting->scheduled_for)) ?> </p>
    @component('mail::button', ['url' => env('APP_URL') . '/my-card'])
        Посмотреть на сайте
    @endcomponent
    С Уважением <br/> <?= env('APP_NAME') ?>
@endcomponent
