@component('mail::message')
    <p> Добрый день! </p>
    <p> Вам отправлена ссылка на восcтановление пароля. Если это не вы, просто проигнорируйте это сообщение </p>
    @component('mail::button', ['url' => $token])
        Сменить пароль
    @endcomponent
    С Уважением <br/> <?= env('APP_NAME') ?>
@endcomponent
