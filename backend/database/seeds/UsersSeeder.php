<?php

use App\Role;
use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $users = [
            [
                'name' => 'User',
                'email' => 'user@gmail.com',
                'password' => bcrypt('123456789'),
            ],
            [
                'name' => 'Admin',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('123456789'),
            ]
        ];
        $count = User::get()->count();
        if ($count === 0){
            foreach ($users as $user) {
                User::create($user);
            }
        }
    }
}
