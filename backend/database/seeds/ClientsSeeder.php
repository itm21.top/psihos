<?php

use App\Role;
use Illuminate\Database\Seeder;
use App\User;
use App\Http\Models\Client;

class ClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $count = Client::get()->count();
        if ($count === 0) {
            for ($i = 0; $i < 10; $i++) {
                $user = User::create([
                    'name' => $faker->lastName,
                    'email' => $faker->unique()->safeEmail,
                    'email_verified_at' => now(),
                    'phone' => $faker->e164PhoneNumber,
                    'skype' => strtolower($faker->userName),
                    'birthday' => $faker->date('Y-m-d', '1999-01-01'), //date('Y-m-d', strtotime()),
                    'chek_payment' => $faker->text,
                    'date_payment' => $faker->date('Y-m-d', '1999-01-01'), //date('Y-m-d', strtotime()),
                ]);

                Client::create([
                    'full_name' => $faker->lastName,
                    'email' => $faker->unique()->safeEmail,
                    'phone' => $faker->e164PhoneNumber,
                    'skype' => strtolower($faker->userName),
                    'birthday' => $faker->date('Y-m-d', '1999-01-01'),
                    'tutor_id' => 1,
                    'user_id' => $user->id,
                    'invite_code' => $faker->regexify('[A-Za-z0-9]{8}'),
                    'notice' => $faker->text,

                    'chek_payment' => $faker->text,
                    'date_payment' => $faker->date('Y-m-d', '1999-01-01'), //date('Y-m-d', strtotime()),
                ]);
            }
        }
    }
}
