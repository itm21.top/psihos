<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMeetingsSetDefaults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $meetings = \App\Http\Models\Meeting::all();
        foreach ($meetings as $meeting) {
            $meeting->name = !empty($meeting->name) ? $meeting->name : 'Название встречи';
            $meeting->save();
        }
        Schema::table('meetings', function (Blueprint $table) {
            $table->text('name')->nullable()->change();
            $table->integer('duration')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
