<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterClientsAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->string('phone', 20)->nullable();
            $table->string('skype', 40)->nullable();
            $table->string('whatsapp', 40)->nullable();
            $table->string('full_name', 40)->nullable();
            $table->date('birthday')->nullable();
            $table->boolean('is_confirmed')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn('phone');
            $table->dropColumn('skype');
            $table->dropColumn('whatsapp');
            $table->dropColumn('full_name');
            $table->dropColumn('birthday');
        });
    }
}
