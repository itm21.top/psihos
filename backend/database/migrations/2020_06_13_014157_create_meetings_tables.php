<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeetingsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meetings', function (Blueprint $table) {
            $table->id();
            $table->integer('tutor_id');
            $table->integer('client_id')->default(0);
            $table->dateTime('scheduled_for');
            $table->enum('periodic_type', ['once', 'schedule'])->default('once');
            $table->enum('grouping_type', ['personal', 'group'])->default('personal');
            $table->enum('type', ['offline', 'online'])->default('offline');
            $table->boolean('notify_by_sms')->default(0);
            $table->timestamps();
        });

        Schema::create('meeting_files', function (Blueprint $table) {
            $table->id();
            $table->integer('meeting_id');
            $table->string('name');
            $table->string('file_name');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meetings');
        Schema::dropIfExists('meeting_files');
    }
}
