<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUsersAndClientsChangeColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone', 20)->nullable();
            $table->string('skype', 40)->nullable();
            $table->string('whatsapp', 40)->nullable();
            $table->date('birthday')->nullable();
        });

        Schema::table('clients', function (Blueprint $table) {
            $table->integer('tutor_id')->after('id');
            $table->dropColumn('phone');
            $table->dropColumn('skype');
            $table->dropColumn('whatsapp');
            $table->dropColumn('full_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
