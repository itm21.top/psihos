import {
    Login,
    RestorePassword,
    Registration,
    GoogleCallback,
    FacebookCallback,
    ResetPassword,
    Terms,
    Policy,
    User_agreement,
    Pay,
    ClientConfirmation
} from "./Pages";

import Main from "./components/Layouts/Main";

export default {
    items: [
        {
            name: 'GoogleCallback',
            path: '/login/google/callback',
            component: GoogleCallback,
        },
        {
            name: 'FacebookCallback',
            path: '/login/facebook/callback',
            component: FacebookCallback,
        },
        {
            name: 'RestorePassword',
            path: '/password-reset',
            component: ResetPassword,
        },
        {
            name: 'Login',
            path: '/login',
            exact: true,
            component: Login,
        },
        {
            name: 'Register',
            path: '/registration',
            exact: true,
            component: Registration,
        },
        {
            name: 'RestorePassword',
            path: '/restore-password',
            exact: true,
            component: RestorePassword,
        },
        {
            name: 'ClientConfirmation',
            path: '/client-confirmation',
            exact: true,
            component: ClientConfirmation,
        },
        {
            name: 'Terms',
            path: '/terms',
            exact: true,
            component: Terms,
        },
        {
            name: 'Policy',
            path: '/policy',
            exact: true,
            component: Policy,
        },
        {
            name: 'User_agreement',
            path: '/agreement',
            exact: true,
            component: User_agreement,
        },
        {
            name: 'Pay',
            path: '/pay',
            exact: true,
            component: Pay,
        },
        {
            name: 'Home',
            path: '/',
            component: Main,
        },
    ]
}
