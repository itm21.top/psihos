import Table from "./components/Table";
import TBody from "./components/TBody";
import THead from "./components/THead";
import THeadCell from "./components/THeadCell";
import TRow from "./components/TRow";
import TCell from "./components/TCell";

export {
    Table,
    TBody,
    THead,
    THeadCell,
    TRow,
    TCell,
}