import React from 'react';

export default ({
    children,
    first = false,
    className = '',
    onClick = () => {},
    style = {}
}) => {
    const cellClass = `flex-row ${first ? 'first' : ''} ${className}`;
    return (
        <div className={cellClass}
             role="columnheader"
             onClick={onClick}
             style={style}
        >
            {children}
        </div>
    )
}