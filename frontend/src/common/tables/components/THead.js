import React from 'react';

export default ({children, style = {}}) => {
    return (
        <div className="flex-table header" role="rowgroup" style={style}>
            {children}
        </div>
    )
}