import React from "react";
import '../table.scss';
export default ({children}) => {
    return (
        <div className="table-container" role="table" aria-label="Destinations">
            {children}
        </div>
    );
};