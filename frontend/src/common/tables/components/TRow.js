import React from 'react';

export default ({
    children,
    rowspan = false,
    onClick = () => {},
    style={style}
}) => {
    const rowClass = `flex-table ${rowspan ?? 'rowspan'}`
    return (
        <div className={rowClass} role="rowgroup" onClick={onClick} style={style}>
            {children}
        </div>
    )
}