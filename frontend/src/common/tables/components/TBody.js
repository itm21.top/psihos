import React from 'react';

export default ({children}) => {
    return (
        <div className="table-body" role="cell">
            {children}
        </div>
    )
}