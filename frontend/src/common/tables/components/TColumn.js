import React from 'react';

export default ({children}) => {
    return (
        <div className="column" role="cell-column">
            {children}
        </div>
    )
}