import React from 'react';

export default ({
    children,
    first = false,
    name = 'name',
    style = {}
}) => {
    const cellClass = `flex-row ${first ? 'first' : ''}`;
    return (
        <div className={cellClass} data-name={name} role="cell" style={style}>
            {children}
        </div>
    )
}