import React from "react";
import './miniPopup.scss';

export default ({
    children,
    onClose = () => {
    },
    className = null
}) => {
    return (
        <React.Fragment>
            <div className={`pop-up-mini ${className}`}>
                <div onClick={onClose} className="pop-up-cover"/>
                <div className={`pop-up-content display`}>
                    <div className="popup-body">
                        {children}
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
};