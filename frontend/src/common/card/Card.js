import React from 'react';
import './card.scss';

export default ({
    children,
    bordersLeftRect = false,
    bordersRightRect = false,
    className = ''
}) => {
    className = className + ' '
        + (bordersLeftRect ? 'borders-left-rect ' : '')
        + (bordersRightRect ? 'borders-right-rect ' : '')
    return (
        <div className={`common-card ${className}`}>
            {children}
        </div>
    )
}
