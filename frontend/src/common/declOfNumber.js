export default (number, textDeclinations) => {
    if (textDeclinations.length !== 3) {
        throw new Error('Unexpected number of text variations');
    }

    number = Math.abs(number) % 100;
    const numberPer10 = number % 10;

    if (number > 10 && number < 20) {
        return textDeclinations[2];
    }
    if (numberPer10 > 1 && numberPer10 < 5) {
        return textDeclinations[1];
    }
    if (numberPer10 === 1) {
        return textDeclinations[0];
    }
    return textDeclinations[2];
}