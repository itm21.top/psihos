import React from 'react';
import {Route, Switch, useHistory} from 'react-router-dom';
import isAuthorized from "../auht/selectors/isAuthorized";
import {Redirect} from "react-router-dom";

const RouteRender = (props) => {
    if (!!props.protected && !isAuthorized()) {
        return <Redirect to="/login" />
    }
    return (
        <props.component {...props} />
    )
}

export default ({config}) => {
    const history = useHistory();
    return (
        <Switch>
            {
                config.items.map((route, key) =>
                    <Route
                        history={history}
                        key={key}
                        path={route.path}
                        name={route.name}
                        exact={route.exact}
                        strict={route.strict}
                        render={({match}) => (<RouteRender match={match} {...route} />)}
                    />
                )
            }
        </Switch>
    )
}