import React, {useState} from 'react';
import DatePicker from "react-datepicker";
import ru from 'date-fns/locale/ru';
import "react-datepicker/dist/react-datepicker.css";

export default ({
                    label,
                    value,
                    isValid = true,
                    errorMessage,
                    onChange,
                    refElement = null,
                    disabled = false,
                    showErrors = true,
                    groupIcon = null,
                    groupIconAppend = null,
                    className = null,
                    peekNextMonth = null,
                    showMonthDropdown = null,
                    showYearDropdown = null,
                    showTimeSelect = null,
                    timeFormat = null,
                    timeIntervals = 15,
                    dateFormat = null
                }) => {
    const [isEdited, setIsEdited] = useState(false);
    const inputClass = ('input-box')
        + (className ? ' ' + className : '')

    let pickerValue = value;
    if (value === '') {
        pickerValue = new Date('2000-01-01');
    }

    return (
        <div className={inputClass}>
            {label &&
            <label className="forTextInputs">
                {label}
            </label>
            }
            <div className="input-group-wrapper">
                {groupIcon &&
                <div className="input-group-prepend">
                    <span className="input-group-text">
                        <i className={groupIcon}></i>
                    </span>
                </div>
                }
                <DatePicker
                    dateFormat="dd.MM.yyyy"
                    className={''
                    + (isEdited && !isValid && showErrors ? 'is-invalid' : '')}
                    onChange={onChange}
                    peekNextMonth={peekNextMonth}
                    showMonthDropdown={showMonthDropdown}
                    showYearDropdown={showYearDropdown}
                    selected={pickerValue}
                    locale={ru}
                />
                {groupIconAppend &&
                <div className="input-group-append">
                        <span className="input-group-text">
                            <i className={groupIconAppend}></i>
                        </span>
                </div>
                }
            </div>
            {isEdited && !isValid && showErrors && (
                <div className="invalid-feedback show">
                    <strong>{errorMessage}</strong>
                </div>
            )}
        </div>
    );
}
