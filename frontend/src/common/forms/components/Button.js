import React from 'react';

export default ({
    name = null,
    onClick = () => {},
    disabled = false,
    className = ''
}) => (
    <button
        type="button"
        className={'button ' + className}
        onClick={onClick}
        disabled={disabled}
    >
        {name}
    </button>
);
