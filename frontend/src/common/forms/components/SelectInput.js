import CreatableSelect from "react-select/creatable/dist/react-select.esm";
import Select from "react-select";
import React, {useState} from "react";

const selectStyles = {
    control: () => ({
        minHeight: '32px',
        minWidth: '92px',
        height: '32px',
    })
}

export default ({
    label,
    hint,
    value = null,
    options,
    isValid = true,
    errorMessage,
    placeholder,
    onChange,
    styles={},
    onKeyDown,
    refElement = null,
    disabled = false,
    className = '',
    showErrors = true,
    isMulti = false,
    isCreatable = false,
    onCreate = () => null,
}) => {
    const customStyles = {}; //Object.assign(selectStyles, styles);
    const [isEdited, setIsEdited] = useState(false);
    return (
        <div className="input-box">
            {label &&
            <label className="forTextInputs">
                {label}
            </label>
            }
            <div className="input-group-wrapper">
                {
                    isCreatable ?
                        <CreatableSelect
                            styles={customStyles}
                            className={className}
                            isMulti={isMulti}
                            value={value}
                            options={options}
                            onChange={onChange}
                            onCreateOption={onCreate}
                            isDisabled={disabled}
                            placeholder={placeholder}
                        />
                        :
                        <Select
                            styles={customStyles}
                            className={className}
                            isMulti={isMulti}
                            value={value}
                            options={options}
                            onChange={(value) => {
                                setIsEdited(true);
                                return onChange(value)
                            }}
                            isDisabled={disabled}
                            placeholder={placeholder}
                        />
                }
            </div>
            {isEdited && !isValid && showErrors && (
                <div className="invalid-feedback show">
                    <strong>{errorMessage}</strong>
                </div>
            )}
        </div>

    )
};
