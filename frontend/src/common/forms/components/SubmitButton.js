import React from 'react';

export default ({
    name = null,
    onClick = () => {},
    disabled = false,
    submitting = false,
    className = ''
}) => (
    <button
        type="button"
        className={'submit-button ' + className}
        onClick={onClick}
        disabled={disabled}
    >
        {name} {submitting && <i className="spinner-border spinner-border-sm"/>}
    </button>
);
