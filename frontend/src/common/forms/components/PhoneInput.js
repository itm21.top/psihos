import React, {useState} from 'react';
import PhoneInput from 'react-phone-number-input/input'

export default ({
    label,
    value,
    isValid = true,
    errorMessage,
    placeholder,
    onChange,
    onKeyDown,
    autoComplete,
    refElement = null,
    disabled = false,
    showErrors = true,
    groupIcon = null,
    groupIconAppend = null,
    groupIconText = null,
    prependText = null,
    className = null,
    defaultCountry = null
}) => {
    const [isEdited, setIsEdited] = useState(false);
    const inputOnChange = (value) => {
        onChange(value);
        setIsEdited(true);
    }
    const inputClass = ('input-box')
        + (className ? ' ' + className : '')

    return (
        <div className={inputClass}>
            {label &&
                <label className="forTextInputs">
                    {label}
                </label>
            }
            <div className="input-group-wrapper">
                {prependText &&
                    <div className="input-group-prepend">
                        <span className="input-group-text">
                            {prependText}
                        </span>
                    </div>
                }
                {groupIcon &&
                    <div className="input-group-prepend">
                        <span className="input-group-text">
                            <i className={groupIcon}></i>
                        </span>
                    </div>
                }
                <PhoneInput
                    country={defaultCountry}
                    className={'' + (isEdited && !isValid && showErrors ? 'is-invalid' : '')}
                    value={value}
                    placeholder={placeholder}
                    autoComplete={autoComplete}
                    onKeyDown={onKeyDown}
                    onChange={inputOnChange}
                    ref={refElement}
                    disabled={disabled}
                    international={true}
                />
                {groupIconAppend &&
                    <div className="input-group-append">
                            <span className="input-group-text">
                                <i className={groupIconAppend}></i>
                            </span>
                    </div>
                }
                {groupIconText &&
                    <div className="input-group-append">
                            <span className="input-group-text-amount">
                                {groupIconText}
                            </span>
                    </div>
                }
            </div>
            {isEdited && !isValid && showErrors && (
                <div className="invalid-feedback show">
                    <strong>{errorMessage}</strong>
                </div>
            )}
        </div>
    );
}
