import React from 'react';

export default ({
    // name = null,
    children,
    onClick = () => {},
    disabled = false,
    className = ''
}) => (
    <div
        className={'text-link ' + className}
        onClick={onClick}
        disabled={disabled}
    >
        {/*{name}*/}
        {children}
    </div>
);
