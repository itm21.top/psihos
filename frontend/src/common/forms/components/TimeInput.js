import React, {useState} from 'react';
import Meeting from "../../../components/Meeting/models/Meeting";
import TimeField from "react-simple-timefield";

export default ({
                    label,
                    value,
                    isValid = true,
                    errorMessage,
                    placeholder,
                    onChange,
                    onKeyDown,
                    autoComplete,
                    refElement = null,
                    disabled = false,
                    showErrors = true,
                    type = 'text',
                    groupIcon = null,
                    groupIconAppend = null,
                    groupIconText = null,
                    className = null,
                }) => {
    const [isEdited, setIsEdited] = useState(false);
    const inputOnChange = (e) => {
        onChange(e.target.value);
        setIsEdited(true);
    }
    const inputClass = ('input-box')
        + (className ? ' ' + className : '')

    return (
        <div className={inputClass}>
            {label &&
            <label className="forTextInputs">
                {label}
            </label>
            }
            <div className="input-group-wrapper">
                {groupIcon &&
                <div className="input-group-prepend">
                    <span className="input-group-text">
                        <i className={groupIcon}></i>
                    </span>
                </div>
                }
                <TimeField
                    className={className}
                    value={value}
                    onChange={inputOnChange}
                    colon=":"
                    style={{width: 'none'}}
                />
                {groupIconAppend &&
                <div className="input-group-append">
                        <span className="input-group-text">
                            <i className={groupIconAppend}></i>
                        </span>
                </div>
                }
                {groupIconText &&
                <div className="input-group-append">
                        <span className="input-group-text-amount">
                            {groupIconText}
                        </span>
                </div>
                }
            </div>
            {isEdited && !isValid && showErrors && (
                <div className="invalid-feedback show">
                    <strong>{errorMessage}</strong>
                </div>
            )}
        </div>
    );
}
