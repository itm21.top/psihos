import React from 'react';

export default ({
                    label,
                    value,
                    isValid = true,
                    errorMessage,
                    placeholder,
                    onChange,
                    onKeyDown,
                    refElement = null,
                    disabled = false,
                    loading = false,
                }) => (
    <div className="input-box">
        {label &&
        <label className="forTextInputs">
            {label}
        </label>
        }
        <div className="input-group-wrapper">
            {loading && (
                <div className="input-group-prepend">
                    <span className="input-group-text" id="basic-addon1">
                        <i className="spinner-border spinner-border-sm text-primary"/>
                    </span>
                </div>
            )}

            <input type="number"
                   className={'' + (!isValid ? 'is-invalid' : '') + (!loading && ' rounded ')}
                   placeholder={placeholder}
                   onKeyDown={onKeyDown}
                   onChange={(e) => onChange(e.target.value)}
                   value={value}
                   ref={refElement}
                   disabled={disabled}
            />
        </div>
        {!isValid && (
            <div className="invalid-feedback show">
                <strong>{errorMessage}</strong>
            </div>
        )}

    </div>
);
