import React, {useEffect, useState} from 'react';

let checkboxCounter = 0;

export default ({
    name = null,
    onClick = () => {},
    disabled = false,
    className = '',
    checked = false
}) => {
    const [counter, setCounter] = useState(checkboxCounter);
    useEffect(() => {
        setCounter(checkboxCounter++);
    }, [])

    const inputClass = ('input-box')
        + (className ? ' ' + className : '')
    return (
        <div className={inputClass}>
            <input type="checkbox"
                   id={`checkbox${counter}`}
                   className="check"
                   checked={checked}
                   disabled={disabled}
            />
            <label onClick={onClick} htmlFor={`checkbox${counter}`}>{name}</label>
        </div>

    )
}
;
