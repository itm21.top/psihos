import React, { useState, useEffect, useRef } from 'react';

export default ({
                    label,
                    value,
                    isValid = true,
                    errorMessage,
                    onChange,
                    // refElement = null,
                    disabled = false,
                    maxlength = false,
                    showErrors = true,
                }) => {
    const [isEdited, setIsEdited] = useState(false);
    const textAreaRef= useRef(null);
    const [textAreaHeight, setTextAreaHeight] = useState("auto");
    const [parentHeight, setParentHeight] = useState("auto");

    useEffect(() => {
        setParentHeight(`${textAreaRef.current.scrollHeight}px`);
        setTextAreaHeight(`${textAreaRef.current.scrollHeight}px`);
    }, [isEdited]);
    return (
        <div className="">
            {label &&
            <label className="forTextInputs">
                {label}
            </label>
            }
            <div className="input-group-wrapper" style={{minHeight: parentHeight,}}>
             <textarea
                 className={'' + (!isValid && showErrors ? 'is-invalid' : '')}
                 rows="3"
                 onChange={(e) => {
                     setTextAreaHeight("auto");
                     setParentHeight(`${textAreaRef.current.scrollHeight}px`);
                     setIsEdited(true);
                     return onChange(e.target.value)
                 }}
                 disabled={disabled}
                 value={value}
                 style={{height: textAreaHeight,}}
                 ref={textAreaRef}
             />
            </div>
            {maxlength && <CharactersUsed maxlength={maxlength} length={value.length}/>}
            {isEdited && !isValid && showErrors && (
                <div className="invalid-feedback show">
                    <strong>{errorMessage}</strong>
                </div>
            )}
        </div>
    );
}
