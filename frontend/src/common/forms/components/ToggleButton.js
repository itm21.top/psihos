import React, {useEffect, useState} from 'react';

let toggleButtonCounter = 0;

export default ({
    name = null,
    onClick = () => {},
    disabled = false,
    className = '',
    selected = false,
}) => {
    const [counter, setCounter] = useState(toggleButtonCounter);
    useEffect(() => {
        setCounter(toggleButtonCounter++);
    }, [])

    return (
        <div className={`input-box ${className}`}>
            <input type="checkbox"
                   id={`checkbox${counter}`}
                   checked={selected}
                   disabled={disabled}
                   onChange={() => {}}
            />
            <label htmlFor={`checkbox${counter}`}
                   onClick={onClick}
            >{name}</label>
        </div>
    )
}
;
