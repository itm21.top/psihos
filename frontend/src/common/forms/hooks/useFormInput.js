export default (
    validationRules,
    getter,
    setter,
    defaultValue = '',
) => {
    const getValue = () => {
        try {
            return getter();
        } catch (e) {
            return defaultValue
        }
    };
    const value = getValue();
    const errorMessage = (
        validationRules.find(rule => rule(value)) || (() => undefined)
    )(value);

    const isValid = errorMessage === undefined;
    return {
        value,
        isValid,
        errorMessage,
        onChange: setter,
    };
};
