import {useCallback, useState} from "react";

export default (
    callback,
    errorHandlers = () => {}
) => {
    const [inProgress, setInProgress] = useState(false);
    const [failed, setFailed] = useState(false);
    const handler = useCallback(async () => {
        if(inProgress) {
            return;
        }
        try {
            setInProgress(true);
            setFailed(false);
            await callback();
            setInProgress(false);
        } catch (e) {
            errorHandlers(e);
            setInProgress(false);
            setFailed(true);
        }
    }, []);

    return [handler, inProgress, failed];
}
