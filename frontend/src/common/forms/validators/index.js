import regex from "./regex";
import required from "./required";
import email from "./email";
import password from "./password";
import phone from "./phone";

export {
    email,
    regex,
    required,
    password,
    phone,
}