export default (regExp, message) => value => !regExp.test(value) ? message : undefined;
