export default message => value => !value || value === 0
        || (Array.isArray(value) && value.length === 0) ? message : undefined;
