import { isPossiblePhoneNumber } from 'react-phone-number-input'

export default message => value => !isPossiblePhoneNumber(value) ? message : undefined;
