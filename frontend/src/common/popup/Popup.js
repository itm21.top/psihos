import React, {useEffect, useState} from 'react';
import './popup.scss';

export default ({
                    children,
                    onClose = () => {
                    },
                    header = '',
                    headerRender = null,
                    className = null
                }) => {
    const [displayed, setDisplay] = useState(false);

    useEffect(() => {
        setTimeout(() => {
            setDisplay(true);
            document.body.style.overflow = "hidden";
            // document.html.style.overflow = "hidden";
            }, 100);
    }, [])

    const onCloseAnimate = () => {
        setDisplay(false);
        document.body.style.overflow = "unset";
        // document.html.style.overflow = "unset";
        setTimeout(onClose, 500);
    }

    return (
        <React.Fragment>
            <div className={className ? `pop-up ${className}` : 'pop-up'}>
                <div onClick={onCloseAnimate} className="pop-up-cover"/>
                <div className={`pop-up-content ${displayed ? 'display' : ''}`}>
                    <div className="popup-head">
                        {headerRender
                            ? headerRender
                            : <p className="h4">{header}</p>
                        }
                        <div onClick={onCloseAnimate}
                             className="pop-up-close">
                            <i className="cil-plus"/>
                        </div>
                    </div>
                    <div className="popup-body">
                        <div className="popup-body-scroll">
                            <div className="popup-body-scroll-inner">
                                {children}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
};