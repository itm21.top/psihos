import axios from 'axios';

export const initApi = () => {
    // Set config defaults when creating the apiInstance
    const apiInstance = axios.create({
        baseURL: process.env.API_URL,
    });

    apiInstance.defaults.timeout = 60000000;
    apiInstance.interceptors.request.use((config) =>  {
        const token = localStorage.getItem('auth-token');
        config.headers.Authorization =  token ? `Bearer ${token}` : '';
        return config;
    })

    apiInstance.interceptors.response.use(function (response) {
        return response;
    }, function (error) {
        if (error.response.status === 401) {
            localStorage.removeItem('auth-token');
            window.location.href = '/login';
            return;
        }
        return Promise.reject(error);
    });

    return apiInstance;
}

export default initApi();