import User from "../../models/User";


export default () => {
    const rolesNames = {
        user: 'Клиент',
        tutor: User.current.profession,
    };
    return rolesNames[User.current.role] ?? 'Специалист';
}
