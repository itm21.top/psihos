import User from "../../models/User";
import UserRoles from "../constants/UserRoles";


export default () => {
    return User.current.role === UserRoles.TUTOR;
}
