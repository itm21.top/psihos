export default () => {
    return !!localStorage.getItem('auth-token');
}