import React from 'react';
import loginWithGoogle from "../actions/loginWithGoogle";

export default ({
}) => {
    const onClick = async () => {
        await loginWithGoogle();
    }
    return (
        <div className="col-auto pb-3 pb-sm-0 pb-lg-3 pb-xl-0">
            <div onClick={onClick} className="social-button google">Google</div>
        </div>
    );
}
