import React from 'react';
import loginWithFacebook from "../actions/loginWithFacebook";

export default ({
}) => {
    const onClick = async () => {
        await loginWithFacebook();
    }
    return (
        <div className="col-auto pb-3 pb-sm-0 pb-lg-3 pb-xl-0">
            <div onClick={onClick} className="social-button facebook">facebook</div>
        </div>
    );
}
