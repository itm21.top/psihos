import api from "../../api";

export default async () => {
    const response = await api.get('/google/login');
    window.location.replace(response.data.url);
}