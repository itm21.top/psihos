import api from "../../api";
import User from "../../models/User";
import mapUser from "../../models/selectors/mapUser";
import saveToken from "./saveToken";

export default async () => {
    const response = await api.post('/register', {
        email: User.current.email,
        password: User.current.password,
        role: User.current.role,
        code: User.current?.code ?? null,
    });

    saveToken(response.data.token);
    User.current = mapUser(response.data.user);
}