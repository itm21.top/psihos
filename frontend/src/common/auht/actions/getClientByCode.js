import api from "../../api";
import User from "../../models/User";
import UserRoles from "../constants/UserRoles";

export default async (getParams) => {
    try {
        const response = await api.get('/get-client-by-code', {
            params: {...getParams}
        });
        console.log(response);
        User.current.name = response.data.full_name;
        User.current.email = response.data.email;
        User.current.role = UserRoles.USER;
        User.current.code = response.data.code;
    } catch (e) {
        // @TODO: redirect to page 404
    }
}