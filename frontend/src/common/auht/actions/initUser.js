import api from "../../api";
import User from "../../models/User";
import mapUser from "../../models/selectors/mapUser";

export default async () => {
    const response = await api.get('/me');
    User.current = mapUser(response.data.user);
}