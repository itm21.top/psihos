import api from "../../api";
import User from "../../models/User";

export default async () => {
    const response = await api.post('/send-reset-link', {
        email: User.current.email,
    });
    return response;
}