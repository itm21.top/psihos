import User from "../../models/User";

export default (field, value) => {
    User.current[field] = value;
}