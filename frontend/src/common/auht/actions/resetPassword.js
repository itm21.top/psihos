import api from "../../api";
import User from "../../models/User";

export default async (params) => {
    const response = await api.post('/reset-password', {
        ...params,
        password: User.current.password
    });
}