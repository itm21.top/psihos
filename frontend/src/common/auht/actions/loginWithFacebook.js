import api from "../../api";

export default async () => {
    const response = await api.get('/facebook/login');
    window.location.replace(response.data.url);
}