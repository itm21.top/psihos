import api from "../../api";
import saveToken from "./saveToken";
import User from "../../models/User";
import mapUser from "../../models/selectors/mapUser";

export default async (callbackParams) => {
    const response = await api.get('/google/callback', {
        params: {...callbackParams}
    });
    saveToken(response.data.token);
    User.current = mapUser(response.data.user);
    window.location.href = '/';
}