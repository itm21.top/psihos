import React, {useEffect, useRef, useState} from "react";
import './contextMenu.scss';

export default ({
                    children
                }) => {

    const [clickedOutside, setClickedOutside] = useState(true);
    const myRef = useRef(null);
    const myRef2 = useRef(null);

    const handleClickOutside = e => {
            if (myRef2.current.contains(e.target)) {
                clickedOutside ? setClickedOutside(false) : setClickedOutside(true);
            }
            if (!myRef.current.contains(e.target)) {
                setClickedOutside(true);
            }
        }
    ;

    const handleClickInside = () => setClickedOutside(false);
    useEffect(() => {
        window.addEventListener('click', handleClickOutside);

        return (() => {
            window.removeEventListener('click', handleClickOutside);

        });
    });

    return (
        <React.Fragment>
            <div className="col-1 context-menu-wrapper">
                <div className={`context-menu ${clickedOutside ? '' : 'active'}`}
                     ref={myRef} onClick={handleClickInside}>
                    <i className="cil-options" ref={myRef2}></i>
                    <div className="context-menu-amount">
                        {children}
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}