import api from "../../../common/api";
import qs from 'qs';
import {toJS} from "mobx";

export default async ({url, map, config}) => {
    config = toJS(config)
    const response = await api.get(url, {
        params: config, paramsSerializer: config => {
            return qs.stringify(config)
        }
    });
    map(response);
}