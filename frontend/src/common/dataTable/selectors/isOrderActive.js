export default function (config, index, dir) {
    return config?.order && config.order.find(item => item.column === index && item.dir === dir) ?
        'active' : '';
}