import React, {useEffect} from 'react';
import {Table, TBody, TCell, THead, THeadCell, TRow} from "../tables";
import {observer} from "mobx-react";
import isOrderActive from "./selectors/isOrderActive";
import TextLink from "../forms/components/TextLink";
import MobileFilters from "./MobileFilters";
import "./datatable.scss";
import {toJS} from "mobx";
import {useWindowSize} from "@react-hook/window-size";

const getCellStyle = (column) => {
    return {
        justifyContent: column?.props?.align ?? 'left'
    }
}

const gridStyles = (columns, windowWidth) => {
    if (windowWidth <= 768) {
        return {};
    }
    const columnsArray = toJS(columns);
    const concatWidth = columnsArray.reduce((acc, column) => acc + (column?.props?.width ?? ''), '');
    const defaultWidth = concatWidth.length ? 'auto' : ((100 / columnsArray.length) + '%');
    const gridColumnsTemplate = columnsArray.reduce((acc, column) => {
        return acc + (acc.length > 0 ? ' ' : '')  + (!column?.isNotVisible ? (column?.props?.width ?? defaultWidth) : '');
    }, '');

    return {
        gridTemplateColumns: gridColumnsTemplate
    }
}

export default observer(({
    config = {},
    data,
    fetch,
    total,
    showMobileFilters = true
}) => {
    const [windowWidth] = useWindowSize();
    const orderByColumn = (config, column) => {
        const currentOrder = config.order[0];
        config.order = [{
            column: column,
            dir: currentOrder.dir === 'desc' ? 'asc' : 'desc'
        }];
        fetch();
    }

    useEffect(() => {
        fetch();
    }, []);

    return (
        <React.Fragment>
            {showMobileFilters &&
                <MobileFilters config={config}
                               onOrderBy={orderByColumn}
                />
            }
            <Table>
                <THead style={gridStyles(config.columns, windowWidth)}>
                    {config.columns.map((col, index) => !col?.isNotVisible && (
                        <THeadCell key={index} first={index === 0}
                                   style={getCellStyle(col)}
                                   className={col.orderable ? 'orderable' : ''}
                                   onClick={col.orderable
                                       ? () => orderByColumn(config, index)
                                       : () => {}
                                   }
                        >
                            {col.name}
                            {col.orderable &&
                                <div className="filters">
                                    <i className={`cil-arrow-top ${isOrderActive(config, index, 'asc')}`}></i>
                                    <i className={`cil-arrow-bottom ${isOrderActive(config, index, 'desc')}`}></i>
                                </div>
                            }
                        </THeadCell>
                    ))}
                </THead>
                <TBody>
                    {data.map((row, index) => (
                        <TRow key={index}
                              onClick={config.onRowClick ? () => config.onRowClick(row) : () => null}
                              style={gridStyles(config.columns, windowWidth)}
                        >
                            {config.columns.map((col, index) => !col?.isNotVisible && (
                                <TCell key={index} first={index === 0} name={col.name} style={getCellStyle(col)}>
                                    {col?.render
                                        ? col.render(row)
                                        : row[col.data]
                                    }
                                </TCell>
                            ))}
                        </TRow>
                    ))}
                </TBody>
            </Table>
            {total > data.length &&
                <div className="client-card-bottom">
                    <TextLink onClick={() => {
                        config.length += 10;
                        fetch();
                    }}>Показать больше</TextLink>
                </div>
            }
        </React.Fragment>
    )
});