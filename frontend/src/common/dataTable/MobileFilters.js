import React from 'react';
import isOrderActive from "./selectors/isOrderActive";

export default ({
    config,
    onOrderBy,
}) =>
<div id="accordion" className="filter-list-style">
    <div className="card-head" id="headingOne">
        <p className="h4 collapsed" data-toggle="collapse" data-target="#collapseOne"
           aria-expanded="false" aria-controls="collapseOne">
            Фильтры
            <span className="close-wrapper">
                <i className="cil-list-filter"></i>
            </span>
        </p>
    </div>
    <div id="collapseOne" className="collapse" aria-labelledby="headingOne">
        <ul className="filter-list">
            {config.columns.filter(col => col.orderable).map((col, index) => (
                <li key={index}
                    onClick={() => onOrderBy(config, index)}
                >
                    {col.name}
                    <div className="filters">
                        <i className={`cil-arrow-top ${isOrderActive(config, index, 'asc')}`}></i>
                        <i className={`cil-arrow-bottom ${isOrderActive(config, index, 'desc')}`}></i>
                    </div>
                </li>
            ))}
        </ul>
    </div>
</div>