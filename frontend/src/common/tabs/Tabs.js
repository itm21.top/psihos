import React, {useState} from "react";
import "./tabs.scss"

export default ({children}) => {

    const tabHeaders = React.Children.map(children, child => {
        return child.props.name;
    });

    const [active, setActive] = useState(0);
    const content = children[active];

    const onClickTab = (tabIndex) => {
        setActive(tabIndex);
    }
    return (
        <React.Fragment>
            <div className="tab-wrapper">
                <ul className="tab-list">
                    {tabHeaders.map((tabName, index) =>
                        <li key={index}
                            className={index === active ? 'active' : ''}
                            onClick={() => onClickTab(index)}
                        >
                            <span>{tabName}</span>
                        </li>
                    )}
                </ul>
                <div className="tab-content">
                    {content}
                </div>
            </div>
        </React.Fragment>
    );
};