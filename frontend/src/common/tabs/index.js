import Tabs from './Tabs.js'
import Tab from './Tab.js'

export {
    Tabs,
    Tab,
}