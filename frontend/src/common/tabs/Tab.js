import React from "react";

export default ({
                    children,
                    name = 'tabname',
                    className = null
                }) => {
    const tabClass = ('')
        + (className ? ' ' + className : '');
    return (
        <div className={tabClass}>
            {children}
        </div>
    );
};