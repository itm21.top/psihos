import {observable} from "mobx";
import UserRoles from "../auht/constants/UserRoles";

const User = observable({
    current: {
        email: '',
        role: UserRoles.TUTOR,
        password: '',
        birthday: '',
        phone: '',
        whatsapp: '',
        name: '',
        avatar: '',
        code: '',
        profession: 'Специалист',
        chek_payment: '',
        date_payment: '',

    },
    loginError: null
});

export default User;

