import moment from "moment";

export default (user) => {
    console.log(user);
    return {
        ...user,
        name: user.name ?? null,
        email: user.email ?? '',
        avatar: user.avatar,
        password: null,
        phone: user.phone ?? '',
        whatsapp: user.whatsapp ?? '',
        role: user.role,
        birthday: user.birthday ? moment(user.birthday).toDate() : moment('2000-01-01').toDate(),

        chek_payment: user.chek_payment ?? '',
        date_payment: user.date_payment ? moment(user.date_payment).toDate() : 'Не оплачено',
    }
}