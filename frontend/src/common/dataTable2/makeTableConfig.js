import {observable} from "mobx";

export default (config) => {
    return observable(config);
}