import React, {useState} from 'react';
import TextInput from "../../common/forms/components/TextInput";
import useFormInput from "../../common/forms/hooks/useFormInput";
import {email as emailValidator, required} from "../../common/forms/validators";
import User from "../../common/models/User";
import {observer} from "mobx-react";
import asyncHandler from "../../common/forms/hooks/asyncHandler";
import SubmitButton from "../../common/forms/components/SubmitButton";
import { useHistory } from 'react-router-dom';
import changeCurrentUser from "../../common/auht/actions/changeCurrentUser";
import restorePassword from "../../common/auht/actions/restorePassword";
import AuthBackground from "../components/AuthBackground";

export default observer(() => {

    const history = useHistory();
    const goToLogin = () => {
        history.push('/login');
    }
    const goToRegistration = () => {
        history.push('/registration')
    }
    const [showResultMessage, setShowResultMessage] = useState(false);
    const [formError, setFormError] = useState(null);
    const email = useFormInput(
        [
            required('Обязательное поле'),
            emailValidator('Вы ввели email в неверном формате'),
        ],
        () => User.current.email,
        (value) => changeCurrentUser('email', value)
    );

    const [submit, submitting, submitFailed] = asyncHandler(async () => {
        setFormError(null);
        await restorePassword();
        setShowResultMessage(true);
    }, (requestError) => {
        setFormError('Вы ввели неверный E-mail!');
    });

    const submitButtonActive = [
        email,
    ].every(field => field.isValid);

    return (
        <div className="row justify-content-center no-gutters flex-body">
            <AuthBackground />
            <div className="col-12 col-lg-6 col-xl-form bg-white">
                <div className="card-body-enter">
                    <div className="logo-wrapper">
                        <div className="logo-bg"></div>
                        <div className="logo-name">life-story</div>
                    </div>
                    {showResultMessage ?
                        <React.Fragment>
                            <h1 className="mt-auto h-with-sub">Не волнуйтесь</h1>
                            <p className="mb-5">Мы отправили ссылку для восстановления доступа на ваш адрес электронной почты:</p>
                            <p><strong>{email.value}</strong></p>
                        </React.Fragment>
                        :<React.Fragment>
                            <h1 className="mt-auto h-with-sub">Не удаётся войти?</h1>
                            <p className="mb-5">Мы отправим ссылку для восстановления доступа на указанный e-mail</p>
                            <TextInput
                                disabled={submitting}
                                className="mb-3"
                                label="E-mail"
                                {...email}
                            />
                            {submitFailed && formError ?
                                <div className="alert alert-danger" role="alert">
                                    {formError}
                                </div>
                                : ''
                            }
                            <SubmitButton
                                className=""
                                name="Отправить ссылку"
                                onClick={submit}
                                submitting={submitting}
                                disabled={!submitButtonActive}
                            />
                        </React.Fragment>
                    }
                    <div className="mt-auto row justify-content-between align-items-center">
                        <div className="col-12 col-sm-auto col-lg-12 col-xl-auto pb-3 pb-sm-0 pb-lg-3 pb-xl-0">
                            <div className="text-link"
                                 onClick={goToLogin}
                            >
                                Войти в систему
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    );
});

