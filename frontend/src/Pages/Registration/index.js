import React, {useState} from 'react';
import TextInput from "../../common/forms/components/TextInput";
import useFormInput from "../../common/forms/hooks/useFormInput";
import {
    required,
    email as emailValidator,
    password as passwordValidator
} from "../../common/forms/validators";
import User from "../../common/models/User";
import {observer} from "mobx-react";
import asyncHandler from "../../common/forms/hooks/asyncHandler";
import SubmitButton from "../../common/forms/components/SubmitButton";
import register from "../../common/auht/actions/register";
import {useHistory} from 'react-router-dom';
import changeCurrentUser from "../../common/auht/actions/changeCurrentUser";
import AuthBackground from "../components/AuthBackground";

export default observer(() => {
    const history = useHistory();
    const goToLogin = () => {
        history.push('/login');
    };
    const goToTerms = () => {
        history.push('/terms');
    };
    const goToPolicy = () => {
        history.push('/policy');
    };
    const [formError, setFormError] = useState(null);
    const email = useFormInput(
        [
            emailValidator('Вы ввели email в неверном формате'),
            required('Обязательное поле'),
        ],
        () => User.current.email,
        (value) => changeCurrentUser('email', value)
    );
    const password = useFormInput(
        [
            required('Обязательное поле'),
            passwordValidator('Пароль должен быть не меньше 8 символов, содержать буквы латинского алфавита, как минимум одну заглавную букву и одну цифру'),
        ],
        () => User.current.password,
        (value) => changeCurrentUser('password', value)
    );
    const passwordConfirm = useFormInput(
        [
            required('Обязательное поле'),
            passwordValidator('Пароль должен быть не меньше 8 символов, содержать буквы латинского алфавита, как минимум одну заглавную букву и одну цифру'),
            value => value !== User.current.password ? 'Пароли должны совпадавть' : undefined
        ],
        () => User.current.password_confirm,
        (value) => changeCurrentUser('password_confirm', value)
    )

    const [submit, submitting, submitFailed] = asyncHandler(async () => {
        setFormError(null);
        await register()
        history.push('/dashboard');
    }, (requestError) => {
        setFormError(requestError.response.data);
    });

    const submitButtonActive = [
        email,
        password,
        passwordConfirm
    ].every(field => field.isValid);

    return (
        <div className="row justify-content-center no-gutters flex-body">
            <AuthBackground/>
            <div className="col-12 col-lg-6  col-xl-form bg-white">
                <div className="card-body-enter">
                    <div className="logo-wrapper">
                        <div className="logo-bg"></div>
                        <div className="logo-name">life-story</div>
                    </div>
                    <h1 className="mt-auto">Начните работу с сервисом</h1>

                    <TextInput
                        disabled={submitting}
                        className="mb-3"
                        label="E-mail"
                        {...email}
                    />
                    <TextInput
                        disabled={submitting}
                        className="mb-3"
                        label="Пароль"
                        type="password"
                        {...password}
                    />
                    <TextInput
                        disabled={submitting}
                        className="mb-3"
                        label="Повторите пароль"
                        type="password"
                        {...passwordConfirm}
                    />
                    {submitFailed && formError ?
                        <div className="alert alert-danger" role="alert">
                            Пользователь с таким E-mail уже существует
                        </div>
                        : ''
                    }
                    <p className="go-to-policy text-capture">Регистрируясь, вы подтверждаете, что принимаете наши
                        <span className="text-link"
                             onClick={goToTerms}
                        >
                             Условия использования</span>и<span className="text-link"
                               onClick={goToPolicy}
                        >
                            Политику конфиденциальности.</span>
                    </p>
                    <SubmitButton
                        className=""
                        name="Зарегистрироваться"
                        onClick={submit}
                        submitting={submitting}
                        disabled={!submitButtonActive}
                    />

                    <div className="mt-auto row justify-content-between align-items-center">
                        <div className="col-12 col-sm-auto col-lg-12 col-xl-auto pb-3 pb-sm-0 pb-lg-3 pb-xl-0">
                            <div className="text-link"
                                 onClick={goToLogin}
                            >
                                Войти в систему
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
});

