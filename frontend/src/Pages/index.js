import Login from './Login';
import Registration from "./Registration";
import RestorePassword from "./RestorePassword";
import ResetPassword from "./ResetPassword";
import GoogleCallback from "./Callbacks/GoogleCallback";
import FacebookCallback from "./Callbacks/FacebookCallback";
import Terms from "./Terms";
import Policy from "./Policy";
import Pay from "./Pay";
import ClientConfirmation from "./ClientConfirmation";

export {
    Login,
    Registration,
    RestorePassword,
    GoogleCallback,
    FacebookCallback,
    Terms,
    Policy,
    Pay,
    ResetPassword,
    ClientConfirmation,
};