import React, {useEffect, useState} from 'react';
import TextInput from "../../common/forms/components/TextInput";
import useFormInput from "../../common/forms/hooks/useFormInput";
import {email as emailValidator, password as passwordValidator, required} from "../../common/forms/validators";
import User from "../../common/models/User";
import {observer} from "mobx-react";
import asyncHandler from "../../common/forms/hooks/asyncHandler";
import SubmitButton from "../../common/forms/components/SubmitButton";
import {useHistory, useLocation} from 'react-router-dom';
import changeCurrentUser from "../../common/auht/actions/changeCurrentUser";
import AuthBackground from "../components/AuthBackground";
import resetPassword from "../../common/auht/actions/resetPassword";

const parseCallbackParams = (queryString) => {
    const callbackParams = {};

    const urlParams = new URLSearchParams(decodeURI(queryString.substr(1)));
    urlParams.forEach((value, key) => {
        callbackParams[key] = value;
    })
    return callbackParams;
}

export default observer(() => {

    const location = useLocation();
    const [formError, setFormError] = useState(null);

    const history = useHistory();
    const goToLogin = () => {
        history.push('/login');
    }
    const goToRegistration = () => {
        history.push('/registration')
    }
    const password = useFormInput(
        [
            required('Обязательное поле'),
            passwordValidator('Пароль должен быть не меньше 8 символов, содержать буквы латинского алфавита, как минимум одну заглавную букву и одну цифру'),
        ],
        () => User.current.password,
        (value) => changeCurrentUser('password', value)
    );
    const passwordConfirm = useFormInput(
        [
            required('Обязательное поле'),
            passwordValidator('Пароль должен быть не меньше 8 символов, содержать буквы латинского алфавита, как минимум одну заглавную букву и одну цифру'),
            value => value !== User.current.password ? 'Пароли должны совпадавть' : undefined
        ],
        () => User.current.password_confirm,
        (value) => changeCurrentUser('password_confirm', value)
    )

    const [submit, submitting, submitFailed] = asyncHandler(async () => {
        const params = parseCallbackParams(location.search)
        await resetPassword(params)
        history.push('/login');
    }, (requestError) => {
        setFormError(requestError.response.data);
    });

    const submitButtonActive = [
        password,
        passwordConfirm
    ].every(field => field.isValid);

    return (
        <div className="row justify-content-center no-gutters flex-body">
            <AuthBackground />
            <div className="col-12 col-lg-6 col-xl-form bg-white">
                <div className="card-body-enter">
                    <div className="logo-wrapper">
                        <div className="logo-bg"></div>
                        <div className="logo-name">life-story</div>
                    </div>
                    <h1 className="mt-auto h-with-sub">Восстановление пароля</h1>
                    <TextInput
                        disabled={submitting}
                        className="mb-3"
                        label="Новый пароль"
                        type="password"
                        {...password}
                    />
                    <TextInput
                        disabled={submitting}
                        className="mb-3"
                        label="Повторите пароль"
                        type="password"
                        {...passwordConfirm}
                    />
                    {submitFailed && formError ?
                        <div className="alert alert-danger" role="alert">
                            Ошибка при восстановлении пароля!
                        </div>
                        : ''
                    }
                    <SubmitButton
                        className=""
                        name="Сохранить"
                        onClick={submit}
                        submitting={submitting}
                        disabled={!submitButtonActive}
                    />
                    <div className="mt-auto row justify-content-between align-items-center">
                        <div className="col-12 col-sm-auto col-lg-12 col-xl-auto pb-3 pb-sm-0 pb-lg-3 pb-xl-0">
                            <div className="text-link"
                                 onClick={goToLogin}
                            >
                                Войти в систему
                            </div>
                        </div>
                        <div className="col-auto d-none d-sm-block d-lg-none d-xl-block">
                            <div className="">•</div>
                        </div>
                        <div className="col-12 col-sm-auto col-lg-12 col-xl-auto">
                            <div className="text-link"
                                 onClick={goToRegistration}
                            >
                                Зарегистрировать аккаунт!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
});

