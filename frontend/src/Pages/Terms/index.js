import React from 'react';
import {observer} from "mobx-react";
import Header from "../../components/Layouts/components/Header";


export default observer(() => {
    return (
        <React.Fragment>
            <div className="c-wrapper c-fixed-components">
                <Header />
                <div className="c-body">
                    <main className="c-main">
                        <div className="container">
                            <div className="fade-in">
                                <p className="h1">Условия использования</p>
                            </div>
                        </div>
                    </main>
                </div>
            </div>
        </React.Fragment>
    )
})