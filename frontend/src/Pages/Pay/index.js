import React from 'react';
import {observer} from "mobx-react";
import Header from "../../components/Layouts/components/Header";
import api from "../../common/api";



function useQuery(){
   return new URLSearchParams(window.location.search);
}

console.log(useQuery().get('Success'));

api.post('/pay/success', {
    chek_payment: useQuery().get('chek_payment'),
    email: useQuery().get('email'),
    date_payment: useQuery().get('date_payment'),
    password: useQuery().get('password'),
});

//http://localhost/pay/success?chek_payment=z&email=itm21.top@gmail.com&date_payment=2021-11-21
//<p className="h1 text-center">Спасибо! Даные для входа отправлены на почту</p> <br/><p className="h1 text-center">Если вы не обнаружите письмо в папке Входящие, проверьте папку спам</p>

export default observer(() => {
    if (useQuery().get('Success')=='true')
    {
        return (
            <React.Fragment>
                <div className="c-wrapper c-fixed-components">
                    <Header />
                    <div className="c-body">
                        <main className="c-main">
                            <div className="container">
                                <div className="fade-in">

                                    <p className="h1 text-center">Спасибо! Оплата прошла успешно</p> <br/>


                                    <a className="text-center lina" href='/'>перейти на сайт</a>
                                </div>
                            </div>
                        </main>
                    </div>
                </div>
            </React.Fragment>
        )
    }
    else {
        return (
            <React.Fragment>
                <div className="c-wrapper c-fixed-components">
                    <Header />
                    <div className="c-body">
                        <main className="c-main">
                            <div className="container">
                                <div className="fade-in">

                                    <p className="h1 text-center">Ошибка! Оплата не произведена</p>
                                    <a className="text-center lina" href='/'>перейти на сайт</a>

                                </div>
                            </div>
                        </main>
                    </div>
                </div>
            </React.Fragment>
        )
    }





})


