import React from 'react';

export default () =>
<div className="d-none d-lg-block col-lg-6 col-xl-background">
    <div className="row row-images h-100">
        <div className="col-images">
            <div className="image-box"></div>
        </div>
        <div className="col-images">
            <div className="image-box"></div>
        </div>
        <div className="col-images">
            <div className="image-box"></div>
        </div>
        <div className="col-images">
            <div className="image-box"></div>
        </div>
    </div>
</div>