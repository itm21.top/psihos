import React, {useEffect} from 'react';
import { useLocation } from 'react-router-dom';
import AuthBackground from "../components/AuthBackground";
import loginWithFacebookCallback from "../../common/auht/actions/loginWithFacebookCallback";

const parseCallbackParams = (queryString) => {
    const callbackParams = {};

    const urlParams = new URLSearchParams(decodeURI(queryString.substr(1)));
    urlParams.forEach((value, key) => {
        callbackParams[key] = value;
    })
    return callbackParams;
}

export default () => {
    const location = useLocation();

    useEffect(() => {
        loginWithFacebookCallback(parseCallbackParams(location.search));
    });

    return (
        <div className="row justify-content-center no-gutters flex-body">
            <AuthBackground />
            <div className="col-md-6">
                <div className="card-group">
                    <div className="card p-4">
                        <div className="card-body">
                            Добро пожаловать!
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

