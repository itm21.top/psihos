import React from 'react';
import {observer} from "mobx-react";
import Header from "../../components/Layouts/components/Header";

export default observer(() => {
    return (
        <React.Fragment>
            <div className="c-wrapper c-fixed-components polispan">
                <Header />
                <div className="c-body">
                    <main className="c-main">
                        <div className="container">
                            <div className="fade-in">
                                <p className="h1">Политика конфиденциальности</p>
                                <ul>
                                    <li><b> ОБЩИЕ ПОЛОЖЕНИЯ </b></li>
                                </ul>
                                <span >Политика обработки персональных данных (далее – Политика) разработана в соответствии с Федеральным законом от 27.07.2006. №152-ФЗ «О персональных данных» (далее – ФЗ-152). </span>

                                <span >Настоящая Политика определяет порядок обработки персональных данных и меры по обеспечению безопасности персональных данных в ООО «Лайф Стори» ОГРН 1215000040750 ИНН 5003144384 КПП 500301001 Юридический адрес: 142715, МОСКОВСКАЯ ОБЛАСТЬ, Г.О. ЛЕНИНСКИЙ, Д БЛИЖНИЕ ПРУДИЩИ, ВЛД. 1 СТР. 1, ОФИС 3-19/4 (далее – Оператор) с целью защиты прав и свобод человека и гражданина при обработке его персональных данных, в том числе защиты прав на неприкосновенность частной жизни, личную и семейную тайну. </span>

                                <span >Настоящая Политика применима только к данному Сайту. Владелец сайта не контролирует и не несет ответственность за сайты третьих лиц, на которые Пользователь может перейти по ссылкам, доступным на Сайте. На таких сайтах у Пользователя может собираться или запрашиваться иная персональная информация, а также могут совершаться иные действия.</span>

                                <span >Владелец сайта исходит из того, что Пользователь предоставляет достоверную и достаточную персональную информацию по вопросам при регистрации и личного кабинета и поддерживает эту информацию в актуальном состоянии. </span>

                                <span >Последствия предоставления недостоверной или недостаточной информации определены в Пользовательском соглашении</span>

                                <span >В Политике используются следующие основные понятия</span><span >: </span>

                                <span >автоматизированная обработка персональных данных – обработка персональных данных с помощью средств вычислительной техники; </span>

                                <span >блокирование персональных данных - временное прекращение обработки персональных данных (за исключением случаев, если обработка необходима для уточнения персональных данных); </span>

                                <span >информационная система персональных данных - совокупность содержащихся в базах данных персональных данных, и обеспечивающих их обработку информационных технологий и технических средств; </span>

                                <span >обезличивание персональных данных - действия, в результате которых невозможно определить без использования дополнительной информации принадлежность персональных данных конкретному субъекту персональных данных; </span>

                                <span >обработка персональных данных - любое действие (операция) или совокупность действий (операций), совершаемых с использованием средств автоматизации или без использования таких средств с персональными данными, включая сбор, запись, систематизацию, накопление, хранение, уточнение (обновление, изменение), извлечение, использование, передачу (распространение, предоставление, доступ), обезличивание, блокирование, удаление, уничтожение персональных данных; </span>

                                <span >оператор - государственный орган, муниципальный орган, юридическое или физическое лицо, самостоятельно или совместно с другими лицами организующие и (или) осуществляющие обработку персональных данных, а также определяющие цели обработки персональных данных, состав персональных данных, подлежащих обработке, действия (операции), совершаемые с персональными данными; </span>

                                <span >персональные данные – любая информация, относящаяся к прямо или косвенно определенному или определяемому физическому лицу (субъекту персональных данных); </span>

                                <span >предоставление персональных данных – действия, направленные на раскрытие персональных данных определенному лицу или определенному кругу лиц; </span>

                                <span >распространение персональных данных - действия, направленные на раскрытие персональных данных неопределенному кругу лиц (передача персональных данных) или на ознакомление с персональными данными неограниченного круга лиц, в том числе обнародование персональных данных в средствах массовой информации, размещение в информационнотелекоммуникационных сетях или предоставление доступа к персональным данным каким-либо иным способом; </span>

                                <span >трансграничная передача персональных данных - передача персональных данных на территорию иностранного государства органу власти иностранного государства, иностранному физическому или иностранному юридическому лицу.  </span>

                                <span >уничтожение персональных данных - действия, в результате которых невозможно восстановить содержание персональных данных в информационной системе персональных данных и (или)  результате которых уничтожаются материальные носители персональных данных;</span>

                                <span >файлы cookie - данные, которые автоматически передаются Оператору в процессе использования Сайта с помощью установленного на устройстве Пользователя программного обеспечения, в том числе  IP-адрес, географическое местоположение, информация о браузере и виде операционной системы устройства Пользователя, технические характеристики оборудования и программного обеспечения, используемых Пользователем, дата и время доступа к Сайту.</span>

                                <span >Компания обязана опубликовать или иным образом обеспечить неограниченный доступ к настоящей Политике обработки персональных данных и конфиденциальной информации в соответствии с ч. 2 ст. 18.1. ФЗ-152. </span>

                                <span >Проставляя галочку о согласии с Политикой обработки персональных данных и конфиденциальной информации и Пользовательским соглашением на сайте по адресу life-story.online. Пользователь тем самым дает свое согласие на обработку его персональных данных, указанных в Политике обработки персональных данных и конфиденциальной информации и Пользовательском соглашении, также подтверждает, что он согласен со всеми условиями Политики обработки персональных данных и конфиденциальной информации  и Пользовательского соглашения и что положения ему ясны. </span>
                                <ol >
                                    <li><b> ПРИНЦИПЫ И УСЛОВИЯ ОБРАБОТКИ ПЕРСОНАЛЬНЫХ ДАННЫХ </b></li>
                                </ol>
                                <span >2.1 Принципы обработки персональных данных и цели.</span>

                                <span >Обработка персональных данных у Оператора осуществляется на основе следующих принципов:  </span>

                                <span >- законности и справедливой основы; </span>

                                <span >- ограничения обработки персональных данных достижением конкретных, заранее определенных и законных целей; </span>

                                <span >- недопущения обработки персональных данных, несовместимой с целями сбора персональных данных;</span>

                                <span > - недопущения объединения баз данных, содержащих персональные данные, обработка которых осуществляется в целях, несовместимых между собой; </span>

                                <span >- обработки только тех персональных данных, которые отвечают целям их обработки; </span>

                                <span >- соответствия содержания и объема обрабатываемых персональных данных заявленным целям обработки; </span>

                                <span >- недопущения обработки персональных данных, избыточных по отношению к заявленным целям их обработки; </span>

                                <span >- обеспечения точности, достаточности и актуальности персональных данных по отношению к целям обработки персональных данных;  </span>

                                <span >- уничтожения либо обезличивания персональных данных по достижении целей их обработки или в случае утраты необходимости в достижении этих целей, при невозможности устранения Оператором допущенных нарушений персональных данных, если иное не предусмотрено федеральным законом. </span>

                                <span >2.2. Обработка персональных данных Пользователей осуществляется на следующих правовых основаниях: </span>

                                <span >— Гражданский кодекс Российской Федерации; </span>

                                <span >— Пользовательское соглашение и иные соглашения, расположенные на Сайте  по адресу: ________________ </span>

                                <span >2.3. Оператор обрабатывает персональные данные Пользователей исключительно в следующих целях: </span>

                                <span >2.3.1. Регистрация Пользователя на Сайте, предоставление Пользователю возможности полноценного использования сервисов Сайта. </span>

                                <span >2.3.2. Идентификация Пользователя на Сайте. </span>

                                <span >2.3.3. Отображение профиля Пользователя для иных Пользователей Сайта в целях поддержания коммуникации, в том числе при оказании услуг дистанционным способом.</span>

                                <span >2.3.4. Установление и поддержание связи между Пользователем и Оператором, консультирование по вопросам оказания услуг.  </span>

                                <span >2.3.5. Исполнение Оператором обязательств перед Пользователем по соглашениям, заключенным с  Оператором (в частности, Пользовательскому соглашению, размещенному и доступному по адресу life-story.online. Исполнение обязательств, в частности, включает в себя информирование о дате и времени проведения занятий (в том числе путем обзвона и направления смс-сообщений).</span>

                                <span >2.3.6. Направление Оператором на адрес электронной почты Пользователя сообщений рекламного характера; таргетирование рекламных материалов, маркетинговых материалов. </span>

                                <span >2.3.7. Размещение на сайте   Оператора, доступной по адресу: ____________в официальных группах социальных сетей и иных сообществах Оператора в сети Интернет, прочих рекламных и информационных источниках, в целях, не связанных с установлением личности Пользователя: </span>

                                <span >— видеоматериалов, полученных в процессе оказания услуг, </span>

                                <span >— оставленных Пользователем отзывов об услугах, оказываемых Оператором. </span>

                                <span >2.3.8. Улучшение качества обслуживания Пользователей и модернизация Сайта Оператора путем обработки запросов и заявок от Пользователя. </span>

                                <span >2.3.9. Статистические и иные исследования на основе обезличенной информации, предоставленной Пользователем.  </span>

                                <span >2.2.10. Оформление заявки на получение Пользователем кредитных средств от банка-партнера Оператора.</span>

                                <span >2.3 Условия обработки персональных данных </span>

                                <span >Оператор производит обработку персональных данных при наличии хотя бы одного из следующих условий: </span>

                                <span >- обработка персональных данных осуществляется с согласия субъекта персональных данных на обработку его персональных данных;  обработка персональных данных необходима для достижения целей, предусмотренных международным договором Российской Федерации или законом, для осуществления и выполнения возложенных законодательством Российской Федерации на оператора функций, полномочий и обязанностей; </span>

                                <span >- обработка персональных данных необходима для осуществления правосудия, исполнения судебного акта, акта другого органа или должностного лица, подлежащих исполнению в соответствии с законодательством Российской Федерации об исполнительном производстве; </span>

                                <span >- обработка персональных данных необходима для исполнения договора, стороной которого либо выгодоприобретателем или поручителем по которому является субъект персональных данных, а также для заключения договора по инициативе субъекта персональных данных или договора, по которому субъект персональных данных будет являться выгодоприобретателем или поручителем; </span>

                                <span >- обработка персональных данных необходима для осуществления прав и законных интересов оператора или третьих лиц либо для достижения общественно значимых целей при условии, что при этом не нарушаются права и свободы субъекта персональных данных; </span>

                                <span >- осуществляется обработка персональных данных, доступ неограниченного круга лиц к которым предоставлен субъектом персональных данных либо по его просьбе (далее - общедоступные персональные данные); </span>

                                <span >- осуществляется обработка персональных данных, подлежащих опубликованию или обязательному раскрытию в соответствии с федеральным законом. </span>

                                <span >2.4 Конфиденциальность персональных данных  </span>

                                <span >Оператор и иные лица, получившие доступ к персональным данным, обязаны не раскрывать третьим лицам и не распространять персональные данные без согласия субъекта персональных данных, если иное не предусмотрено федеральным законом. </span>

                                <span >2.5 Общедоступные источники персональных данных </span>

                                <span >В целях информационного обеспечения у Оператора могут создаваться общедоступные источники персональных данных субъектов, в том числе справочники и  адресные книги. В общедоступные источники персональных данных с письменного согласия субъекта могут включаться его фамилия, имя, отчество, дата и место рождения, должность, номера контактных телефонов, адрес электронной почты и иные персональные данные, сообщаемые субъектом персональных данных. </span>

                                <span >Сведения о субъекте должны быть в любое время исключены из общедоступных источников персональных данных по требованию субъекта либо по решению суда или иных уполномоченных государственных органов. </span>

                                <span > 2.6 Специальные категории персональных данных </span>

                                <span >Обработка Оператором специальных категорий персональных данных, касающихся расовой, национальной принадлежности, политических взглядов, религиозных или философских убеждений, состояния здоровья, интимной жизни, допускается в случаях, если: </span>

                                <span >- субъект персональных данных дал согласие в письменной форме на обработку своих персональных данных; </span>

                                <span >- персональные данные сделаны общедоступными субъектом персональных данных;  </span>

                                <span >- обработка персональных данных осуществляется в соответствии с законодательством о государственной социальной помощи, трудовым законодательством, законодательством Российской Федерации о пенсиях по государственному пенсионному обеспечению, о трудовых пенсиях; </span>

                                <span >- обработка персональных данных необходима для защиты жизни, здоровья или иных жизненно важных интересов субъекта персональных данных либо жизни, здоровья или иных жизненно важных интересов других лиц и получение согласия субъекта персональных данных невозможно;  обработка персональных данных осуществляется в медико-профилактических целях, в целях установления медицинского диагноза, оказания медицинских и медикосоциальных услуг при условии, что обработка персональных данных осуществляется лицом, профессионально занимающимся медицинской деятельностью и обязанным в соответствии с законодательством Российской Федерации сохранять врачебную тайну; </span>

                                <span > - обработка персональных данных необходима для установления или осуществления прав субъекта персональных данных или третьих лиц, а равно и в связи с осуществлением правосудия;  </span>

                                <span >- обработка персональных данных осуществляется в соответствии с законодательством об обязательных видах страхования, со страховым законодательством.  </span>

                                <span >Обработка специальных категорий персональных данных должна быть незамедлительно прекращена, если устранены причины, вследствие которых осуществлялась их обработка, если иное не установлено федеральным законом. </span>

                                <span >Обработка персональных данных о судимости может осуществляться Оператором исключительно в случаях и в порядке, которые определяются в соответствии с федеральными законами. </span>

                                <span >2.7 Биометрические персональные данные </span>

                                <span >Сведения, которые характеризуют физиологические и биологические особенности человека, на основании которых можно установить его личность - биометрические персональные данные - могут обрабатываться Оператором только при наличии согласия в письменной форме субъекта. </span>

                                <span >2.8. Поручение обработки персональных данных другому лицу </span>

                                <span >Оператор вправе поручить обработку персональных данных другому лицу с согласия субъекта персональных данных, если иное не предусмотрено федеральным законом, на основании заключаемого с этим лицом договора. Лицо, осуществляющее обработку персональных данных по поручению Оператора, обязано соблюдать принципы и правила обработки персональных данных, предусмотренные ФЗ-152. </span>

                                <span >2.9 Трансграничная передача персональных данных </span>

                                <span >Оператор обязана убедиться в том, что иностранным государством, на территорию которого предполагается осуществлять передачу персональных данных, обеспечивается адекватная защита прав субъектов персональных данных, до начала осуществления такой передачи. </span>

                                <span >Трансграничная передача персональных данных на территории иностранных государств, не обеспечивающих адекватной защиты прав субъектов персональных данных, может осуществляться в случаях: </span>

                                <span >- наличия согласия в письменной форме субъекта персональных данных на трансграничную передачу его персональных данных; </span>

                                <span >- исполнения договора, стороной которого является субъект персональных данных. </span>
                                <ol >
                                    <li><b> ПРАВА СУБЪЕКТА ПЕРСОНАЛЬНЫХ ДАННЫХ </b></li>
                                </ol>
                                <span >3.1 Согласие субъекта персональных данных на обработку его персональных данных </span>

                                <span >Субъект персональных данных принимает решение о предоставлении его персональных данных и дает согласие на их обработку свободно, своей волей и в своем интересе. Согласие на обработку персональных данных может быть дано субъектом персональных данных или его представителем в любой позволяющей подтвердить факт его получения форме, если иное не установлено федеральным законом. </span>

                                <span >Обязанность предоставить доказательство получения согласия субъекта персональных данных на обработку его персональных данных или доказательство наличия оснований, указанных в ФЗ-152, возлагается на Оператора. </span>

                                <span >3.2 Права субъекта персональных данных  и перечень персональных данных, обрабатываемых Оператором.</span>

                                <span >Субъект персональных данных имеет право на получение у Оператора информации, касающейся обработки его персональных данных, если такое право не ограничено в соответствии с федеральными законами. Субъект персональных данных вправе требовать от Оператора уточнения его персональных данных, их блокирования или уничтожения в случае, если персональные данные являются неполными, устаревшими, неточными, незаконно полученными или не являются необходимыми для заявленной цели обработки, а также принимать предусмотренные законом меры по защите своих прав. </span>

                                <span >Обработка персональных данных в целях продвижения товаров, работ, услуг на рынке путем осуществления прямых контактов с потенциальным потребителем с помощью средств связи, а также в целях политической агитации допускается только при условии предварительного согласия субъекта персональных данных. Указанная обработка персональных данных признается осуществляемой без предварительного согласия субъекта персональных данных, если Компания не докажет, что такое согласие было получено. </span>

                                <span >Оператор обязан немедленно прекратить по требованию субъекта персональных данных обработку его персональных данных в вышеуказанных целях. </span>

                                <span >Запрещается принятие на основании исключительно автоматизированной обработки персональных данных решений, порождающих юридические последствия в отношении субъекта персональных данных или иным образом затрагивающих его права и законные интересы, за исключением случаев, предусмотренных федеральными законами, или при наличии согласия в письменной форме субъекта персональных данных. </span>

                                <span >Если субъект персональных данных считает, что Оператор осуществляет обработку его персональных данных с нарушением требований ФЗ-152 или иным образом нарушает его права и свободы, субъект персональных данных вправе обжаловать действия или бездействие Оператора в Уполномоченный орган по защите прав субъектов персональных данных или в судебном порядке. </span>

                                <span >Субъект персональных данных имеет право на защиту своих прав и законных интересов, в том числе на возмещение убытков и (или) компенсацию морального вреда в судебном порядке. </span>

                                <span >3.3. Персональные данные, обрабатываемые Оператором:</span>
                                <ul>
                                    <li ><span >фамилия и имя и отчество</span></li>
                                    <li ><span >адрес электронной почты</span></li>
                                    <li ><span >номер телефона</span></li>
                                    <li ><span >все данные, которые указаны в аккаунтах социальной сети: ВКонтакте, Facebook, Twitter (если регистрация осуществляется путем авторизации через социальную сеть или другой электронный сервис) </span></li>
                                    <li ><span >пол</span></li>
                                    <li ><span >дата рождения</span></li>
                                    <li ><span >файлы cookie</span></li>
                                    <li ><span >фото – изображения</span></li>
                                </ul>
                                <b>4 ОБЕСПЕЧЕНИЕ БЕЗОПАСНОСТИ ПЕРСОНАЛЬНЫХ ДАННЫХ </b>

                                <span >Безопасность персональных данных, обрабатываемых Оператора, обеспечивается реализацией правовых, организационных и технических мер, необходимых для обеспечения требований федерального законодательства в области защиты персональных данных. </span>

                                <span >Для предотвращения несанкционированного доступа к персональным данным Оператором применяются следующие организационно-технические меры: </span>

                                <span >- назначение должностных лиц, ответственных за организацию обработки и защиты персональных данных; </span>

                                <span >- ограничение состава лиц, имеющих доступ к персональным данным; </span>

                                <span >- ознакомление субъектов с требованиями федерального законодательства и нормативных документов Оператора по обработке и защите персональных данных; </span>

                                <span >- организация учета, хранения и обращения носителей информации; </span>

                                <span >- определение угроз безопасности персональных данных при их обработке, формирование на их основе моделей угроз; </span>

                                <span >- разработка на основе модели угроз системы защиты персональных данных; </span>

                                <span >- проверка готовности и эффективности использования средств защиты информации; </span>

                                <span >- разграничение доступа пользователей к информационным ресурсам и программноаппаратным средствам обработки информации; </span>

                                <span >- регистрация и учет действий пользователей информационных систем персональных данных; </span>

                                <span >- использование антивирусных средств и средств восстановления системы защиты персональных данных; </span>

                                <span >- применение в необходимых случаях средств межсетевого экранирования, обнаружения вторжений, анализа защищенности и средств криптографической защиты информации;  организация пропускного режима на территорию Оператора, охраны помещений с техническими средствами обработки персональных данных. </span>

                                <span >4.1. Распространение персональных данных может осуществляться Оператором исключительно в следующих случаях: </span>

                                <span >4.1.1. При обработке персональных данных с целью отображение профиля Пользователя для иных Пользователей Сайта для поддержания коммуникации. </span>

                                <span >4.1.2. С целью размещения отзывов об услугах, оказываемых Оператором, оставленных Пользователями, в различных источниках информации.</span>

                                <span >4.1.3. С целью размещения видео-материалов и фото- материалов, фото- изображений,  полученных в процессе оказания услуг, в различных источниках информации,  а также предоставленных Пользователем.</span>

                                <span >4.2. Оператор вправе осуществлять передачу персональных данных третьим лицам с соблюдением следующих условий: </span>

                                <span >— Третье лицо осуществляет обработку персональных данных с использованием баз данных на территории Российской Федерации. </span>

                                <span >— Третье лицо обеспечивает конфиденциальность персональных данных при их обработке и использовании; обязуется не раскрывать иным лицам, а также не распространять персональные данные Пользователей без их согласия. </span>

                                <span >— Третье лицо гарантирует соблюдение следующих мер по обеспечению безопасности персональных данных при их обработке: использование средств защиты информации; обнаружение и фиксация фактов несанкционированного доступа к персональным данным и принятие мер по восстановлению персональных данных; ограничение доступа к персональным данным; регистрация и учет действий с персональными данными; контроль и оценка эффективности применяемых мер по обеспечению безопасности персональных данных.</span>
                                <ol >
                                    <li><b> ЗАКЛЮЧИТЕЛЬНЫЕ ПОЛОЖЕНИЯ </b></li>
                                </ol>
                                <span >Иные права и обязанности Оператора, как оператора персональных данных определяются законодательством Российской Федерации в области персональных данных. </span>

                                <span >Обработка персональных данных Пользователя производится Оператором с использованием баз данных на территории Российской Федерации.</span>

                                <span >Оператор оставляет за собой право вносить изменения в Политику. На Пользователе лежит обязанность при каждом использовании Сайта знакомиться с текстом Политики.</span>

                                <span >Новая редакция Политики вступает в силу с момента ее размещения в соответствующем разделе сайта Оператора по адресу: life-story.online. Продолжение пользования Сайтом или его сервисами после публикации новой редакции Политики означает принятие Политики и ее условий Пользователем. В случае несогласия с условиями Политики Пользователь должен незамедлительно прекратить использование Сайта и его сервисов. </span>

                                <span > Должностные лица Оператора, виновные в нарушении норм, регулирующих обработку и защиту персональных данных, несут материальную, дисциплинарную, административную, гражданско-правовую или уголовную ответственность в порядке, установленном федеральными законами</span>
                            </div>
                        </div>
                    </main>
                </div>
            </div>
        </React.Fragment>
    )
})
