import React, {useState} from 'react';
import TextInput from "../../common/forms/components/TextInput";
import useFormInput from "../../common/forms/hooks/useFormInput";
import {required, email as emailValidator} from "../../common/forms/validators";
import User from "../../common/models/User";
import {observer} from "mobx-react";
import asyncHandler from "../../common/forms/hooks/asyncHandler";
import SubmitButton from "../../common/forms/components/SubmitButton";
import login from "../../common/auht/actions/login";
import { useHistory } from 'react-router-dom';
import changeCurrentUser from "../../common/auht/actions/changeCurrentUser";
import AuthBackground from "../components/AuthBackground";
import GoogleLoginButton from "../../common/auht/components/GoogleLoginButton";
import {Tab, Tabs} from "../../common/tabs";
import FacebookLoginButton from "../../common/auht/components/FacebookLoginButton";

const errorMessages = {
    unknown_user: 'Пользователь с таким Email не существует',
    invalid_password: 'Неверный пароль',
    invalid_tutor: 'Перейдите во вкладку специалист',
    invalid_tutor2: 'Перейдите во вкладку клиент',
}

export default observer(() => {
    const history = useHistory();
    const goToRegistration = () => {
        history.push('/registration')
    }

    const goToRestorePassword = () => {
        history.push('/restore-password')
    }
    const [formError, setFormError] = useState(null);

    const email = useFormInput(
        [
            required('Обязательное поле'),
            emailValidator('Вы ввели email в неверном формате')
        ],
        () => User.current.email,
        (value) => changeCurrentUser('email', value)
    );

    const password = useFormInput(
        [
            required('Обязательное поле'),
        ],
        () => User.current.password,
        (value) => changeCurrentUser('password', value)
    );
    const profession = useFormInput(
        [
            required('Обязательное поле'),
        ],
        () => User.current.profession,
        (value) => changeCurrentUser('profession', value)
    );


    const [submit, submitting, submitFailed] = asyncHandler(async () => {
        setFormError(null);
        User.current.profession = 'Специалист';
        await login();

        history.push('/dashboard');
    }, (requestError) => {
        setFormError(requestError.response.data.error);
    });

    const [submit2, submitting2, submitFailed2] = asyncHandler(async () => {
        setFormError(null);
        User.current.profession = 'Клиент';

        await login();

        history.push('/dashboard');
    }, (requestError) => {
        setFormError(requestError.response.data.error);
    });

    const submitButtonActive = [
        email,
        password
    ].every(field => field.isValid);

    const submitButtonActive2 = [
        email,
        password
    ].every(field => field.isValid);

    return (

        <div className="row justify-content-center no-gutters flex-body">
            <AuthBackground />
            <div className="col-12 col-lg-6 col-xl-form bg-white">
                <div className="card-body-enter">
                    <div className="logo-wrapper">
                        <div className="logo-bg"></div>
                        <div className="logo-name">life-story</div>
                    </div>
                    <h1 className="mt-auto">Добро пожаловать в life-story.online</h1>
                    <Tabs>
                        <Tab name="Специалист" onClick={() => console.log('dsdsd')}>
                            <TextInput
                                disabled={submitting}
                                className="mb-3"
                                label="E-mail"
                                {...email}
                            />
                            <TextInput
                                disabled={submitting}
                                className="mb-3"
                                label="Пароль"
                                type="password"
                                {...password}
                            />
                            <TextInput
                                disabled={submitting}
                                className="d-none"

                                type="hidden"
                                {...profession}
                            />
                            {submitFailed && formError ?
                                <div className="alert alert-danger" role="alert">
                                    {errorMessages[formError]}
                                </div>
                                : ''
                            }
                            <SubmitButton
                                name="Войти"
                                onClick={submit}
                                className=""
                                submitting={submitting}
                                disabled={!submitButtonActive}
                            />
                            <div className="row align-items-center justify-content-lg-between pb-5">
                                <div className="col-12 col-sm-auto col-lg-12 col-xl-auto pb-3 pb-sm-0 pb-lg-3 pb-xl-0">
                                    <p className="text-capture">Или с помощью:</p>
                                </div>
                                <GoogleLoginButton />
                                <FacebookLoginButton/>

                            </div>

                            <div className="mt-auto row justify-content-between align-items-center">
                                <div className="col-12 col-sm-auto col-lg-12 col-xl-auto pb-3 pb-sm-0 pb-lg-3 pb-xl-0">
                                    <div className="text-link"
                                         onClick={goToRestorePassword}
                                    >
                                        Не удается войти?
                                    </div>
                                </div>
                                <div className="col-auto d-none d-sm-block d-lg-none d-xl-block">
                                    <div className="">•</div>
                                </div>
                                <div className="col-12 col-sm-auto col-lg-12 col-xl-auto">
                                    <div className="text-link"
                                         onClick={goToRegistration}
                                    >
                                        Зарегистрировать аккаунт!
                                    </div>
                                </div>
                            </div>
                        </Tab>
                        <Tab name="Клиент" onClick={() => User.current.profession="Клиент"}>
                            <TextInput
                                disabled={submitting}
                                className="mb-3"
                                label="E-mail"
                                {...email}
                            />
                            <TextInput
                                disabled={submitting}
                                className="mb-3"
                                label="Пароль"
                                type="password"
                                {...password}
                            />

                            {submitFailed2 && formError ?
                                <div className="alert alert-danger" role="alert">
                                    {errorMessages[formError]}
                                </div>
                                : ''
                            }
                            <SubmitButton
                                name="Войти"
                                onClick={submit2}
                                className=""
                                submitting={submitting2}
                                disabled={!submitButtonActive2}
                            />
                            <div className="mt-auto row justify-content-between align-items-center">
                                <div className="col-12 col-sm-auto col-lg-12 text-center">
                                    <div className="text-link"
                                         onClick={goToRestorePassword}
                                    >
                                        Не удается войти?
                                    </div>
                                </div>
                            </div>
                        </Tab>
                    </Tabs>
                </div>
            </div>
        </div>
    );
});

