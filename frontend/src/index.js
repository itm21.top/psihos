import React from 'react';
import ReactDOM from 'react-dom';
import './scss/style.scss';
import '@coreui/coreui/dist/js/coreui.bundle.min.js';
import Router from './common/router'
import routerConfig from "./routerConfig";
import {BrowserRouter} from "react-router-dom";

import WebFont from 'webfontloader';
WebFont.load({
    google: {
        families: ['Roboto', 'Inter']
    }
});


const App = () => {
    return (
        <BrowserRouter>
            <Router config={routerConfig}/>
        </BrowserRouter>
    )
};

ReactDOM.render(<App/>, document.getElementById('root'));


