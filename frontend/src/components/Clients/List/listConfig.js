import React from "react";
import Avatar from "../../Layouts/components/Avatar/Avatar";
import Meeting from "./Cols/MeetingCol";
import makeTableConfig from "../../../common/dataTable/makeTableConfig";
import getClientRole from "../models/selectors/getClientRole";

const stopPropagation = (e) => {
    e.stopPropagation();
    e.nativeEvent.stopImmediatePropagation();
}
const config = {
    columns: [
        {
            data: 'full_name',
            name: 'Клиент',
            props: {
                width: '30%',
            },
            searchable: true,
            orderable: true,
            render: (row) => <div className="row col-12">
                <Avatar avatar={row?.avatar}
                        name={row.full_name}
                        role={getClientRole(row)}
                />
            </div>
        },
        /*{
            data: 'closest_meeting',
            name: 'Ближайшая встреча',
            props: {
                width: 'auto',
            },
            searchable: false,
            orderable: true,
            render: (row) => {
                if (!row.closest_meeting && !row.user) {
                    return '-';
                }
                return row.closest_meeting && <Meeting meeting={row.closest_meeting}/>
            }
        },
        {
            data: 'id',
            name: 'Задачи',
            props: {
                width: '75px',
            },
            render: (row) => <React.Fragment>
                <span className="task left active">{row?.tasksStats?.completed ?? '-'}</span>
                / <span className="task right passive">{row?.tasksStats?.total ?? '-'}</span>
            </React.Fragment>
        },
        {
            data: 'id',
            name: 'Контакты',
            props: {
                width: '250px',
            },
            render: (row) => <div className="row row-contacts flex-xl-nowrap">
                <div className="col-sm-7 pb-1 pt-1">
                    {row.phone}
                </div>
                <div className="col-sm-auto pb-1 pt-1">
                    <nav className="client-card-contacts">
                        {row.whatsapp
                            ? <a className="active"
                                 onClick={stopPropagation}
                                 href={`whatsapp://send?phone=${row.whatsapp}`}>
                                <i className="cib-whatsapp"></i>
                              </a>
                            : ''
                        }
                        {row.skype
                            ? <a className="active"
                                 onClick={stopPropagation}
                                 href={`skype:${row.skype}?chat`}>
                                <i className="cib-skype"></i>
                              </a>
                            : ''
                        }
                        <a className="active"
                           onClick={stopPropagation}
                           href={`mailto:${row.email}`}><i className="cil-envelope-letter"></i></a>

                    </nav>
                </div>
            </div>
        },*/
    ],
    order: [{column: 0, dir: 'desc'}],
    start: 0,
    length: 10,
    onRowClick: () => {},
    search: {
        value: "",
        regex: false
    },
    filters: {
        archive: 0,
        deleted: 0
    }
}

export default makeTableConfig(config);
