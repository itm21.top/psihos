import React from 'react';

export default ({meeting}) => {
    return (
        <div className="row  mt-4 mt-sm-0 row-meeting-list">
            <div className="col-auto">
               
                    {meeting.date}
               
            </div>
            <div className="col-auto">
                {meeting.time}
            </div>
            <div className="col-auto">
                <span className={`client-card-time ${meeting.type === 'online' ? 'active' : '' }`}>
                    {meeting.type === 'online' ? 'онлайн' : 'очно'}
                </span>
            </div>
        </div>
    )
}
