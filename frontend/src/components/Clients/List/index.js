import React, {useEffect} from 'react';
import Client from "../models/Client";
import {observer} from "mobx-react";
import {useHistory} from "react-router-dom";
import listConfig from "./listConfig";
import DataTable from "../../../common/dataTable";
import fetchDatatable from "../actions/fetchDatatable";
import declOfNumber from "../../../common/declOfNumber";
import TextInput from "../../../common/forms/components/TextInput";
import Button from "../../../common/forms/components/Button";
import useFormInput from "../../../common/forms/hooks/useFormInput";
import ToggleButton from "../../../common/forms/components/ToggleButton";
import addNewClient from "../actions/addNewClient";
import User from "../../../common/models/User";
import moment from "moment";
import isTutor from "../../../common/auht/selectors/isTutor";

export default observer(() => {

    const history = useHistory();
    const showClientCard = (client) => {
        history.push('/client/' + client.id)
        Client.current = client;
    };

    useEffect(() => {
        listConfig.onRowClick = showClientCard;
    }, []);

    const search = useFormInput(
        [],
        () => listConfig.search.value,
        (value) => {
            listConfig.search.value = value;
            fetchDatatable();
        }
    );
    let tempdate = new Date(User.current.date_payment);
    const datelimit = moment(tempdate, 'YYYY-MM-DDTHH:mm:ss.SSSZ');
    const now = moment();
    return (
        <React.Fragment>
            <div className="common-card-sticky-head">
                <div className="client-card-head">
                    <div className="">
                        <div className="saba-head">
                            <p className="h3">Клиенты</p>
                            <p className="client-card-qty">
                                <span>{Client.list.length} </span>
                                {declOfNumber(Client.recordsTotal, ['клиент', 'клиента', 'клиентов'])}
                                <span> из {Client.recordsTotal}</span>
                            </p>
                        </div>
                        {!now.isAfter(datelimit) && <Button className="button-style but-tusck but-dob-mob" name="+ &nbsp; Добавить клиента"
                                                            onClick={addNewClient}/>}
                    </div>
                    <div className="client-card-search-wrapper">
                        <TextInput
                            placeholder="Поиск по списку клиентов"
                            groupIcon="cil-search"
                            {...search}
                        />
                    </div>
                </div>
                <div className="client-card-sub-head">
                    <ToggleButton name="Показывать архивных и удаленных"
                                  onClick={() => {
                                      listConfig.filters.archive = +!listConfig.filters.archive;
                                      fetchDatatable();
                                  }}
                                  selected={listConfig.filters.archive}
                    />
                    {!now.isAfter(datelimit) && <Button className="button-style but-tusck but-dob-desc" name="+ &nbsp; Добавить клиента"
                            onClick={addNewClient}/>}
                </div>
            </div>
            <DataTable config={listConfig}
                       fetch={fetchDatatable}
                       data={Client.list}
                       total={Client.recordsTotal}
            />
        </React.Fragment>
    )
});
