import {observable} from "mobx";

const Client = observable({
    current: {
        email: '',
        name: '',
        phone: '',
        skype: '',
        whatsapp: '',
        birthday: '--.--.----',
        meetings: [],
        is_archive: 0,
        paid_meetings: 0,
    },
    backUpCurrent: null,
    loginError: null,
    recordsTotal: 0,
    currentMeetings: [],
    list: [],
    avaliableList: [],
    isShowForm: false,
    isShowMiniPopup: false,
});

export default Client;

