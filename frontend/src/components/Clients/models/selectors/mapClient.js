import moment from "moment";
import mapMeeting from "../../../Meeting/models/selectors/mapMeeting";
import mapTask from "../../../Task/models/selectors/mapTask";
import countTasksStats from "../../../Task/models/selectors/countTasksStats";

export default (client) => {
    const birthday = client.birthday ? moment(client.birthday + ' 00:00:00') : moment();
    return {
        ...client,
        birthday: birthday.toDate(),
        birthday_string: client.birthday ? birthday.format('DD.MM.YYYY') : null,
        meetings: client.meetings ? client.meetings.map(mapMeeting) : [],
        tasks: client.tasks ? client.tasks.map(mapTask) : [],
        tasksStats: countTasksStats(client.tasks),
        latest_meeting: client.latest_meeting ? mapMeeting(client.latest_meeting) : null,
        closest_meeting: client.closest_meeting ? mapMeeting(client.closest_meeting) : null
    }
}