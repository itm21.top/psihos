import React from "react";

export default (row) => {
    if (row.deleted_at) {
        return <span style={{color:'#ED0000'}}>Удаленный клиент</span>
    }
    if (row.is_archive) {
        return <span style={{color:'#F2994A'}}>Архивный клиент</span>
    }
    return 'Клиент'
}