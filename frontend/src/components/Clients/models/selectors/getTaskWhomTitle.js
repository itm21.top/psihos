
export default (client, task) => {
    return client?.user?.id && client?.user?.id === task.created_by ? 'Мне' : 'Клиенту';
}