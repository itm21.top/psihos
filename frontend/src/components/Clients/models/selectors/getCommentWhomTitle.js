
export default (client, comment) => {
    return client?.user?.id && client?.user?.id === comment.user_id ? 'Терапевту' : 'Клиенту';
}