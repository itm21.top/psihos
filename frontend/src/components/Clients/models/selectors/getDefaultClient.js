export default () => {
    return {
        email: 'test'+ Math.floor(Math.random()*999)+'@gmail.com',
        notice: '',
        full_name: '',
        phone: '',
        skype: '',
        whatsapp: '',
        birthday: new Date(),
        is_confirmed: 0,
        is_archive: 0,
        meetings: [],
    }
}