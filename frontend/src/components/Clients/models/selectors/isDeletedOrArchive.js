import Client from "../Client";

export default () => {
    return Client.current?.deleted_at ? !!Client.current.deleted_at : false
        || Client.current?.is_archive;
}