import React, {useState} from 'react';
import toggleClientAddForm from "../actions/toggleClientForm";
import {observer} from "mobx-react";
import useFormInput from "../../../common/forms/hooks/useFormInput";
import {email as emailValidator, phone as phoneValidator, required} from "../../../common/forms/validators";
import Client from "../models/Client";
import TextInput from "../../../common/forms/components/TextInput";
import asyncHandler from "../../../common/forms/hooks/asyncHandler";
import changeCurrentClient from "../actions/changeCurrentClient";
import saveClient from "../actions/saveClient";
import Popup from "../../../common/popup/Popup";
import DateInput from "../../../common/forms/components/DateInput";
import SubmitButton from "../../../common/forms/components/SubmitButton";
import PhoneInput from "../../../common/forms/components/PhoneInput";
import register from "../../../common/auht/actions/register";

export default observer(() => {
    const [success, setSuccess] = useState(null);
    const name = useFormInput(
        [
            required('Имя обязательное поле'),
        ],
        () => Client.current.full_name,
        (value) => changeCurrentClient('full_name', value)
    );
    const [formError, setFormError] = useState(null);
    const email = useFormInput(
        [
            required('Email обязательное поле'),
            emailValidator('Email введён неверно')
        ],
        () => Client.current.email,
        (value) => changeCurrentClient('email', value)
    );

    const phone = useFormInput(
        [
            phoneValidator('Неверный номер телефона')
        ],
        () => Client.current.phone,
        (value) => changeCurrentClient('phone', value)
    );

    const birthday = useFormInput(
        [],
        () => Client.current.birthday,
        (value) => changeCurrentClient('birthday', value)
    );

    const skype = useFormInput(
        [],
        () => Client.current.skype,
        (value) => changeCurrentClient('skype', value)
    );

    const whatsapp = useFormInput(
        [
            phoneValidator('Неверный номер телефона')
        ],
        () => Client.current.whatsapp,
        (value) => changeCurrentClient('whatsapp', value)
    );

    const [isShowWhatsapp, setIsShowWhatsapp] = useState(0);
    const set = () => {
        setIsShowWhatsapp(1)
    };

    const [isShowWhatsapp0, setIsShowWhatsapp0] = useState(0);
    const set0 = () => {
        setIsShowWhatsapp0(1)
    };


    const paidMeetingsOnChange = (value) => {
        const reg = /^\d+$/;
        if (!reg.test(value) && value !== '') {
            return;
        }
        Client.current.paid_meetings = value ?? 0;
    }

    const paidMeetings = useFormInput(
        [],
        () => Client.current.paid_meetings,
        paidMeetingsOnChange
    );

    const [submit, submitting, submitFailed] = asyncHandler(async () => {
        setFormError(null);
        await saveClient();
        if (Client.current.id) {
            Client.isShowForm = false;
        } else {
            setSuccess('Клиент добавлен!');
        }
    }, (requestError) => {
        setFormError(requestError.response.data);
    });


    const submitButtonActive = [
        name,
        email,
        birthday
    ].every(field => field.isValid);



    return (
        <Popup header={Client.current.id ? 'Редактировать клиента' : 'Добавить клиента'}
               onClose={toggleClientAddForm}>

            
            <TextInput
                disabled={submitting || success}
                className="mb-3"
                label="ФИО или ваш код клиента *"
                {...name}
            />
            <TextInput
                disabled={submitting || success}
                className="mb-3"
                label="Skype"
                {...skype}
            />
            <PhoneInput
                disabled={submitting || success}
                className="mb-3"
                prependText=""
                // defaultCountry="RU"
                label="Номер телефона"
                {...phone}
            />
            {isShowWhatsapp0 === 0 &&
            <div className="input-box mb-3  d-none">
                <div className="whatsapp-wrapper" onClick={set0}>
                    <i className="cil-envelope-letter"></i>
                    <p className="pb-0">Кликни если нужено указать почту</p>
                </div>
            </div>
            }
            {isShowWhatsapp0 === 1 &&
                <TextInput
                    disabled={submitting || success}
                    className="mb-3"
                    label="E-mail *"
                    {...email}
                />
            }

            {isShowWhatsapp === 0 &&
            <div className="input-box mb-3">
                <div className="whatsapp-wrapper" onClick={set}>
                    <i className="cib-whatsapp"></i>
                    <p className="pb-0">Кликни если нужен номер для Whatsapp</p>
                </div>
            </div>
            }
            {isShowWhatsapp === 1 &&
            <PhoneInput
                disabled={submitting || success}
                prependText=""
                className="mb-3"
                label="WhatsApp"
                // defaultCountry="RU"
                {...whatsapp}
            />
            }
            <DateInput
                showMonthDropdown
                showYearDropdown
                disabled={submitting || success}
                className="mb-4 input-date-wrapper"
                label="Дата рождения"
                groupIconAppend="cil-calendar"
                {...birthday}
            />
            {Client.current.id ?
                <TextInput
                    disabled={submitting || success}
                    className="mb-3"
                    label="Оплаченных встреч *"
                    {...paidMeetings}
                /> : ''
            }
            {submitFailed && formError ?
                <div className="alert alert-danger" role="alert">
                    Пользователь с таким E-mail уже существует
                </div>
                : ''
            }
            {success &&
            <div className="col-sm-12 alert alert-success mt-10" role="alert">
                {success}
            </div>
            }
            {!success
                ? <SubmitButton name={Client.current.id ? 'Редактировать' : 'Добавить'}
                                onClick={submit} submitting={submitting} disabled={!submitButtonActive}/>
                : <SubmitButton name={'Закрыть'}
                                onClick={toggleClientAddForm}/>
            }
        </Popup>
    )
});
