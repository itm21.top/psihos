import React from 'react';
import {observer} from "mobx-react";
import ContextMenu from "../../../common/contextMenu/ContextMenu";
import editComment from "../../Task/actions/editComment";
import deleteComment from "../../Task/actions/deleteComment";
import getCommentWhomTitle from "../models/selectors/getCommentWhomTitle";
import fetchDatatable from "../actions/fetchDatatable2";

export default observer(({client, task}) => {
    const commentsList = task.comments ?? [];

    return (
        <React.Fragment>
            {commentsList.map((comment, key) =>
                <div className="comments_z" key={key}>
                    <div className="row row-col-meeting-head">
                        <div className="col-auto">
                            <span className="whom">{getCommentWhomTitle(client, comment)}</span>
                        </div>
                        <div className="col-auto">
                            <span className="date">{comment.creationDate?.format('DD.MM.YYYY')}</span>
                        </div>
                        <div className="col justify-content-end">
                            <ContextMenu>
                                <div className="context-menu-inner">
                                    <p className="h5">Комментарий</p>
                                    <ul className="menu">
                                        <li onClick={() => editComment(comment)}>Редактировать</li>
                                        <li onClick={() => deleteComment(comment)}><span className="trash">Удалить</span><i
                                            className="cil-trash"></i></li>
                                    </ul>
                                </div>
                            </ContextMenu>
                        </div>
                    </div>
                    <div className="row row-col-meeting-body">
                        <div className="col-auto col-task-name">
                            {comment.text}
                        </div>
                    </div>
                </div>

            )}

        </React.Fragment>

    )

})
