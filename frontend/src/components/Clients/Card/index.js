import React, {useEffect} from 'react';
import {useParams} from "react-router-dom";
import Client from "../models/Client";
import './user.scss'
import fetchClient from "../actions/fetchClient";
import {observer} from "mobx-react";
import ClientInfo from "./ClientInfo";
import ClientHistory from "./ClientHistory";
import ClientTasks from "./ClientTasks";
import ClientInfoTabs from "./ClientInfoTabs";


export default observer(() => {

    const {id} = useParams();
    useEffect(() => {
        fetchClient(id);
    }, []);
    return (
        <div className="row justify-content-center client-card">
            <div className="col-xl-8">
                <div className="row row-info-data">
                    <div className="col-md-6 client-info">
                        <ClientInfo/>
                    </div>
                    <div className="col-md-6 client-info">
                        <ClientInfoTabs />
                    </div>
                </div>
                <ClientTasks />

            </div>
            <div className="col-xl-4">
                <ClientHistory />
            </div>
        </div>
    )
});
