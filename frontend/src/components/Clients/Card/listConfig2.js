import React from "react";
import makeTableConfig from "../../../common/dataTable/makeTableConfig";
import editTask from "../../Task/actions/editTask";
import toggleCompleteTask from "../../Task/actions/toggleCompleteTask";
import deleteTask from "../../Task/actions/deleteTask";
import ContextMenu from "../../../common/contextMenu/ContextMenu";
import fetchDatatable from "../../Task/actions/fetchDatatable";
import User from "../../../common/models/User";
import UserRoles from "../../../common/auht/constants/UserRoles";
import addTaskComment from "../../Task/actions/addTaskComment";
import TaskComment from "../../Task/models/TaskComment";
import AddComment from "../../Task/Form/AddComment";
import Client from "../models/Client";
import ClientTaskComments from "./ClientTaskComments";
import moment from "moment";


const toggleTaskState = async (taskId) => {

    await toggleCompleteTask(taskId);
    await fetchDatatable();
    location.href='/client/'+Client.current.id;

}

const deleteTaskAction = async (taskId) => {
    await deleteTask(taskId);
    await fetchDatatable();
}

export const getConfig = () => ({
    columns: [

        {
            data: 'id',
            isNotVisible: User.current.role !== UserRoles.TUTOR,
            name: 'Кому',
            props: {
                width: 'auto',
            },
            render: (row) =>
                <div className="col-12 col-meeting">
                    <div className="row row-col-meeting-head">
                        <div className="col-auto">{console.log(row)}<span className="whom">{row.client_id ? 'Клиенту' : 'Мне'}</span></div>
                        <div className="col-auto"><span className="date">{moment(row.created_at).format('DD.MM.YYYY')}</span></div>
                        <div className="col-auto"><span className="date">Дедлайн: {row.deadline_date ? row.deadline_date_formatted : '---'}</span></div>
                        <div className="col-auto"><span className="date"> {row.sucsekful ? <i className="cil-check-circle"></i> : '' }</span></div>

                        <div className="col-auto"></div>

                        <div className="col justify-content-end">
                            <ContextMenu>
                                <div className="context-menu-inner">
                                    <p className="h5">Задача</p>
                                    <ul className="menu">
                                        <li onClick={() => addTaskComment(row.id)}>Комментировать</li>
                                        <li onClick={() => editTask(row.id)}>Редактировать</li>
                                        <div className="border-line"></div>
                                        {(!row.is_complete
                                                ? <li onClick={() => toggleTaskState(row.id)}>Отметить выполненой<i className="cil-check-circle"></i></li>
                                                : <li onClick={() => toggleTaskState(row.id)}>Отметить невыполненой<i className="cil-check-circle"></i></li>
                                        )}
                                        <li onClick={() => deleteTaskAction(row.id)}><span className="trash">Удалить</span><i className="cil-trash"></i></li>
                                    </ul>
                                </div>
                            </ContextMenu>
                        </div>
                    </div>
                    <div className="row row-col-meeting-body">
                        <div className="col-auto col-task-name">{row.name }</div>
                        <div className="offset-1 col-11 gfgf">
                            <div className="row row-col-meeting-head">
                                <div className="col-auto">
                                    <div className="row row-col-meeting-body">

                                        <ClientTaskComments task={row} client={Client.current}/>
                                        {TaskComment.isShowForm && row.id === TaskComment.current.task_id && <AddComment task={row}/>}
                                    </div>

                                </div>


                            </div>

                        </div>
                    </div>
                </div>




        },







    ],
    order: [{column: 0, dir: 'desc'}],
    start: 0,
    length: 10,
    onRowClick: () => {},
    search: {
        value: "",
        regex: false
    },
    filters: {
        dateRange: 'ALL',
        completed: 0,
        deleted: 0
    }
});

export default makeTableConfig(getConfig());
