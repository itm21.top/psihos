import React from 'react';
import { useState } from 'react';
import Card from "../../../common/card/Card";
import {Tab, Tabs} from "../../../common/tabs";
import Client from "../models/Client";
import editMeeting from "../../Meeting/actions/editMeeting";
import getMeetingsTillNow from "../../Meeting/models/selectors/getMeetingsTillNow";
import {observer} from "mobx-react";
import toggleMeetingFixation from "../../Meeting/actions/toggleMeetingFixation";
import mapMeetingType from "../../Meeting/models/selectors/mapMeetingType";
import ToggleButton from "../../../common/forms/components/ToggleButton";
import isTutor from "../../../common/auht/selectors/isTutor";
import ReactFancyBox from "react-fancybox";
import FancyVideo from "react-videojs-fancybox";
import deleteFile from "../../Meeting/actions/deleteFile";
import Meeting from "../../Meeting/models/Meeting";




export default observer(() => {
    const me = (a, b, c) =>{
        let myArr = b.indexOf('png');
        let myArr2 = b.indexOf('jpg');
        let myArr3 = b.indexOf('gif');
        let myArr4 = b.indexOf('svg');
        let myArr5 = b.indexOf('ttf');
        let myArr6 = b.indexOf('woff');
        let myArr16 = b.indexOf('jpeg');

        let myArr7 = b.indexOf('mp4');
        let myArr8 = b.indexOf('avi');
        let myArr9 = b.indexOf('mov');

        let myArr10 = b.indexOf('MP4');
        let myArr11 = b.indexOf('AVI');
        let myArr12 = b.indexOf('MOV');

        console.log(myArr);
        if(myArr>0||myArr2>0||myArr3>0||myArr4>0||myArr5>0||myArr6>0||myArr16>0)
        {
            return  <ReactFancyBox thumbnail={"/public/meeting/"+a+"/"+b} image={"/public/meeting/"+a+"/"+b}/>
        }
        else if(myArr7>0||myArr8>0||myArr9>0||myArr10>0||myArr11>0||myArr12>0)
        {
            return <video width="250" className="videocod"  autoplay loop controls playsinline >
                <source src={"/public/meeting/"+a+"/"+b} type="video/mp4"/>
                <source src={"/public/meeting/"+a+"/"+b} type="video/ogv"/>
                <source type="video/webm" src={"/public/meeting/"+a+"/"+b}/>
            </video>

        }
        else
        {
            return <div></div>
        }
    }
    return (
    <Card className="client-history-list">
        <div className="row client-history-list-head">
            <div className="col d-flex">
                <p className="h4">История встреч</p>
            </div>
            <div className="col-auto input-box">
                <ToggleButton name="Не посещенные"
                              onClick={() => {

                              }}
                />
            </div>
        </div>

        <Tabs>
            <Tab name="Все">
                <div className="row row-meeting">
                    {Client.current.meetings && getMeetingsTillNow(Client.current.meetings).map((meeting, key) =>
                        <React.Fragment key={key}>
                            <div className="col-11 col-meeting" key={key}
                                 onClick={() => isTutor() && editMeeting(meeting.id)}>
                                <div className={`row ${meeting.is_visited == '0' ? 'not-visited' : ''}`}>
                                    <div className="col-auto">
                                        <span
                                            className={`client-card-time ${meeting.type == 'online' ? 'active' : ''}`}>
                                            {meeting.meetingDate.format('DD.MM.YYYY')}
                                        </span>
                                    </div>
                                    <div className="col-auto">
                                        {meeting.time}
                                    </div>
                                    <div className="col">
                                        {mapMeetingType(meeting.type)}
                                    </div>

                                </div>
                            </div>
                            {isTutor() &&
                            <React.Fragment>
                                <div className="col-1">
                                    <a className="" data-toggle="tooltip"
                                       title={meeting.is_fixed ? 'Открепить' : 'Закрепить'}
                                       onClick={() => isTutor() && toggleMeetingFixation(meeting)}>
                                        <i className={meeting.is_fixed ? 'cib-reverbnation fixed-style' : 'cib-reverbnation'}></i>
                                    </a>
                                </div>
                                <ul className="col-12">
                                    {meeting?.files && meeting.files.map((file, key) => (
                                        <li key={key}>
                                            <i className="cil-paperclip"></i>
                                            {file.name}
                                            {console.log(meeting)}
                                            {me(meeting.id, file.file_name, file.id)}
                                            <span className="vail"></span>
                                            <span className="delete" onClick={() => deleteFile(meeting.id, file.id)}></span>
                                        </li>
                                    ))}
                                </ul>
                            </React.Fragment>
                            }
                        </React.Fragment>
                    )}
                </div>
            </Tab>
            <Tab name="Индивидуальные">
                <div className="row row-meeting">
                    {Client.current.meetings && getMeetingsTillNow(Client.current.meetings).map((meeting, key) =>
                        <React.Fragment key={key}>
                            <div className="col-11 col-meeting" key={key}
                                 onClick={() => isTutor() && editMeeting(meeting.id)}>
                                <div className={`row ${meeting.is_visited == '0' ? 'not-visited' : ''}`}>
                                    <div className="col-auto">
                                        <span
                                            className={`client-card-time ${meeting.type == 'online' ? 'active' : ''}`}>
                                            {meeting.meetingDate.format('DD.MM.YYYY')}
                                        </span>
                                    </div>
                                    <div className="col-auto">
                                        {meeting.time}
                                    </div>
                                    <div className="col">
                                        {mapMeetingType(meeting.type)}
                                    </div>

                                </div>
                            </div>
                            {isTutor() &&
                            <React.Fragment>
                                <div className="col-1">
                                    <a className="" data-toggle="tooltip"
                                       title={meeting.is_fixed ? 'Открепить' : 'Закрепить'}
                                       onClick={() => toggleMeetingFixation(meeting)}>
                                        <i className={meeting.is_fixed ? 'cib-reverbnation fixed-style' : 'cib-reverbnation'}></i>
                                    </a>
                                </div>
                                <ul className="col-12">
                                    {meeting?.files && meeting.files.map((file, key) => (
                                        <li key={key}>
                                            <i className="cil-paperclip"></i>
                                            {file.name}
                                            {console.log(meeting)}
                                            {me(meeting.id, file.file_name, file.id)}
                                            <span className="vail"></span>
                                            <span className="delete" onClick={() => deleteFile(meeting.id, file.id)}></span>
                                        </li>
                                    ))}
                                </ul>
                            </React.Fragment>
                            }
                        </React.Fragment>
                    )}
                </div>
            </Tab>
            <Tab name="Групповые">
                <div className="row row-meeting">
                    {Client.current.meetings && getMeetingsTillNow(Client.current.meetings).map((meeting, key) =>
                        <React.Fragment key={key}>
                            <div className="col-11 col-meeting" key={key}
                                 onClick={() => isTutor() && editMeeting(meeting.id)}>
                                <div className={`row ${meeting.is_visited == '0' ? 'not-visited' : ''}`}>
                                    <div className="col-auto">
                                        <span
                                            className={`client-card-time ${meeting.type == 'online' ? 'active' : ''}`}>
                                            {meeting.meetingDate.format('DD.MM.YYYY')}
                                        </span>
                                    </div>
                                    <div className="col-auto">
                                        {meeting.time}
                                    </div>
                                    <div className="col">
                                        {mapMeetingType(meeting.type)}
                                    </div>

                                </div>
                            </div>
                            {isTutor() &&
                            <React.Fragment>
                                <div className="col-1">
                                    <a className="" data-toggle="tooltip"
                                       title={meeting.is_fixed ? 'Открепить' : 'Закрепить'}
                                       onClick={() => toggleMeetingFixation(meeting)}>
                                        <i className={meeting.is_fixed ? 'cib-reverbnation fixed-style' : 'cib-reverbnation'}></i>
                                    </a>
                                </div>
                                <ul className="col-12">
                                    {meeting?.files && meeting.files.map((file, key) => (
                                        <li key={key}>
                                            <i className="cil-paperclip"></i>
                                            {file.name}
                                            {console.log(meeting)}
                                            {me(meeting.id, file.file_name, file.id)}
                                            <span className="vail"></span>
                                            <span className="delete" onClick={() => deleteFile(meeting.id, file.id)}></span>
                                        </li>
                                    ))}
                                </ul>
                            </React.Fragment>
                            }
                        </React.Fragment>
                    )}
                </div>
            </Tab>
        </Tabs>
    </Card>
    )
})