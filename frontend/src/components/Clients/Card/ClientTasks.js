import React, {useState} from 'react';
import Card from "../../../common/card/Card";
import ToggleButton from "../../../common/forms/components/ToggleButton";
import AddTask from "../../Task/Form";
import fetchClient from "../actions/fetchClient";
import Task from "../../Task/models/Task";
import addNewTask from "../../Task/actions/addNewTask";
import {observer} from "mobx-react";
import Client from "../models/Client";
import editTask from "../../Task/actions/editTask";
import ContextMenu from "../../../common/contextMenu/ContextMenu";
import deleteTask from "../../Task/actions/deleteTask";
import toggleCompleteTask from "../../Task/actions/toggleCompleteTask";
import TaskComment from "../../Task/models/TaskComment";
import addTaskComment from "../../Task/actions/addTaskComment";
import AddComment from "../../Task/Form/AddComment";
import ClientTaskComments from "./ClientTaskComments";
import toggleCompletedTasksSwitch from "../actions/toggleCompletedTasksSwitch";
import getTasksForClient from "../../Task/models/selectors/getTasksForClient";
import isTutor from "../../../common/auht/selectors/isTutor";
import getTaskWhomTitle from "../models/selectors/getTaskWhomTitle";
import isClientTask from "../../Task/models/selectors/isClientTask";
import fetchDatatable from "../../Task/actions/fetchDatatable";
import User from "../../../common/models/User";
import moment from "moment";


export default observer(() => {
    const [active, setActive] = useState(null);

    const toggleCompleteTask2 = async (taskId) => {
        await toggleCompleteTask(taskId);

        location.href='/client/'+Client.current.id;
    }
    let tempdate = new Date(User.current.date_payment);
    const datelimit = moment(tempdate, 'YYYY-MM-DDTHH:mm:ss.SSSZ');
    const now = moment();

    return(
    <Card className="client-task-list">
        <div className="row client-task-list-head">
            <div className="col d-flex">
                <p className="h4">Задачи</p>
            </div>
            <div className="col-auto">
                {isTutor() && !now.isAfter(datelimit) && <button className="btn add-style"
                                      type="button"
                                      onClick={addNewTask}
                >
                    <i className="cil-plus"></i>Создать задачу
                </button>
                }
            </div>
            <div className="col-md-auto d-flex align-items-center">
                <ToggleButton name="Выполненные"
                              onClick={toggleCompletedTasksSwitch}
                              selected={Task.showCompleted}
                />
            </div>
            {Task.isShowForm && <AddTask saveCallback={() => fetchClient(Client.current.id)} />}
        </div>
        <div className="row row-meeting">
            {Client.current.tasks
            && getTasksForClient(Client.current.tasks).map((task, key, isShowAll) =>
                <div className="col-12 col-meeting"  key={key}>
                    <div className="row row-col-meeting-head">
                        <div className="col-auto">
                            <span className="whom">{getTaskWhomTitle(Client.current, task)}</span>
                        </div>
                        <div className="col-auto">
                            <span className="date">{task.creationDate?.format('DD.MM.YYYY')}</span>
                        </div>
                        <div className="col-auto">
                            {task.is_complete ?  <i className="is-complete cil-check-circle"></i> : ''}
                        </div>
                        <div className="col justify-content-end">
                            <ContextMenu>
                                <div className="context-menu-inner">
                                    <p className="h5">Задача</p>
                                    <ul className="menu">
                                        <li onClick={() => addTaskComment(task.id)}>Комментировать</li>
                                        {(isTutor() || isClientTask(Client.current, task))
                                        && <li onClick={() => editTask(task.id)}>Редактировать</li>
                                        }
                                        {(isTutor() || isClientTask(Client.current, task))
                                        && <div className="border-line"></div>
                                        }
                                        {isTutor() && (!task.is_complete
                                                ? <li onClick={() => toggleCompleteTask2(task.id)}>Отметить выполненой<i className="cil-check-circle"></i></li>
                                                : <li onClick={() => toggleCompleteTask2(task.id)}>Отметить невыполненой<i className="cil-check-circle"></i></li>
                                        )}
                                        {(isTutor() || isClientTask(Client.current, task))
                                        && <li onClick={() => deleteTask(task.id)}><span className="trash">Удалить</span><i className="cil-trash"></i></li>
                                        }
                                    </ul>
                                </div>
                            </ContextMenu>
                        </div>
                    </div>
                    <div className="row row-col-meeting-body">
                        <div className="zuzuzba">
                            <div className={active == key ? "col-auto col-task-name active" : "col-auto col-task-name "} key = {key} onClick={()=>setActive(key)}>
                                {task.is_complete ?  <span className="is-complete">{task.name}</span> : <span >{task.name}</span>}
                            </div>
                            <span className={active == key ? "close_shir close_shir2 active" : "close_shir close_shir2"} onClick={()=>setActive(key)}>Подробнее</span>

                        </div>
                        <span className={active == key ? "close_shir active" : "close_shir"} onClick={()=>setActive(null)}>Закрыть</span>

                        <ClientTaskComments task={task} client={Client.current}/>
                        {TaskComment.isShowForm && task.id === TaskComment.current.task_id && <AddComment task={task}/>}
                    </div>
                </div>
            )}
        </div>

    </Card>

)
})
