import React from 'react';
import Meeting from "../../Meeting/models/Meeting";
import AddMeeting from '../../Meeting/AddMeeting'
import {observer} from "mobx-react";
import Client from "../models/Client";
import editMeeting from "../../Meeting/actions/editMeeting";
import createMeeting from "../../Meeting/actions/createMeeting";
import isDeletedOrArchive from "../models/selectors/isDeletedOrArchive";
import getMeetingsAfterNow from "../../Meeting/models/selectors/getMeetingsAfterNow";
import fetchClient from "../actions/fetchClient";
import isTutor from "../../../common/auht/selectors/isTutor";
import mapMeetingType from "../../Meeting/models/selectors/mapMeetingType";
import ToggleButton from "../../../common/forms/components/ToggleButton";
import User from "../../../common/models/User";
import moment from "moment";


export default observer(() => {


    const togglePaymeetMeetengSwitch = () => {
        Meeting.current.paymeet = !Meeting.current.paymeet;
         let toggleclass = document.getElementsByClassName('mm0');
         for (let i = 0; i < toggleclass.length; i++)
         {
             toggleclass[i].classList.toggle('d-none');
         }
    }
    let tempdate = new Date(User.current.date_payment);
    const datelimit = moment(tempdate, 'YYYY-MM-DDTHH:mm:ss.SSSZ');
    const now = moment();
    return (
        <React.Fragment>
            <div className="col-md-auto d-flex align-items-center">
            <ToggleButton name="Оплаченые"
                          className="tooglebutpayment"
                          onClick={() => {togglePaymeetMeetengSwitch()}}
                          selected={Meeting.current.paymeet}
            />
            </div>
            <div className="row client-meeting-list-head">

                <div className="col d-flex">
                    <p className="h5">Следующая встреча</p>
                </div>
                {(!isDeletedOrArchive() && isTutor()) && !now.isAfter(datelimit) &&
                    <div className="col-auto">
                        <button className="btn add-style"
                                type="button"
                                onClick={createMeeting}
                        >
                            <i className="cil-plus"></i>Добавить
                        </button>
                    </div>
                }
                {Meeting.isShowForm && <AddMeeting saveCallback={() => fetchClient(Client.current.id)} />}
            </div>

            <div className="row row-meeting">
                {Client.current.meetings  &&  getMeetingsAfterNow(Client.current.meetings).reverse().map((meeting, key) =>

                <div className={"col-12 col-meeting mm" + meeting.paymeet} key={key} onClick={() => (isTutor() &&!isDeletedOrArchive()) && editMeeting(meeting.id)}>
                    <div className="row">
                        <div className="col-auto">
                            <span className={`client-card-time ${meeting.type == 'online' ? 'active' : '' }`}>
                                {meeting.meetingDate.format('DD.MM.YYYY')}

                            </span>
                        </div>
                        <div className="col-auto">
                            {meeting.time}
                        </div>
                        <div className="col-auto">
                            {mapMeetingType(meeting.type)}
                        </div>
                    </div>
                </div>
                )}
            </div>
        </React.Fragment>
    )
});
