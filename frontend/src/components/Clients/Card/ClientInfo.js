import {observer} from "mobx-react";
import React from "react";
import Avatar from "../../Layouts/components/Avatar/Avatar";
import Client from "../models/Client";
import Card from "../../../common/card/Card";
import isDeleted from "../models/selectors/isDeleted";
import getMeetingsTillNow from "../../Meeting/models/selectors/getMeetingsTillNow";
import moment from "moment";
import saveClientAvatar from "../actions/saveClientAvatar";
import isDeletedOrArchive from "../models/selectors/isDeletedOrArchive";

export default observer(() => {
    return (
        <Card className="borders-right-rect">
            <div className="client-info-card-top">
                <div className="row row-client-info-card">
                    <div className="col-auto client-avatar">
                        <Avatar name={Client.current.full_name}
                                avatar={Client.current?.avatar ?? ''}
                                editable={!isDeletedOrArchive()}
                                onChange={e => saveClientAvatar(e.target.files[0])}
                        />
                        {(!Client.current.deleted_at && !!Client.current.is_archive)
                            && <span className="client-status archive">Архивный</span>}
                        {isDeleted()
                            && <span className="client-status deleted">Удаленный</span>}

                    </div>
                    <div className="col-auto">
                        <p className="h3 card-title">
                            {Client.current.full_name}
                        </p>
                        <div className="phone">{Client.current.phone}</div>
                        <nav className="client-card-contacts">
                            {Client.current.whatsapp
                                ? <a className="active"
                                     href={`whatsapp://send?phone=${Client.current.whatsapp}`}>
                                    <i className="cib-whatsapp"></i>
                                </a>
                                : ''
                            }
                            {Client.current.skype
                                ? <a className="active"
                                     href={`skype:${Client.current.skype}?chat`}>
                                    <i className="cib-skype"></i>
                                </a>
                                : ''
                            }
                            <a className="active"
                               href={`mailto:${Client.current.email}`}><i className="cil-envelope-letter"></i></a>

                        </nav>
                    </div>
                </div>
            </div>
            <div className="client-info-card-bottom">
                <div className="row justify-content-between  flex-nowrap">
                    <div className="col-5">
                        <p className="time h2">
                            {Client.current.closest_meeting
                                ? <React.Fragment>
                                    {moment(Client.current.closest_meeting.meetingDate).format('DD.MM')}
                                    <span className="tooltip-wrapper">
                                        <i className="cil-clock"></i>
                                        <span className="tooltip-amount">
                                            {moment(Client.current.closest_meeting.meetingDate).format('HH:mm')}
                                        </span>
                                    </span>
                                </React.Fragment>
                                : <span>---</span>
                            }
                        </p>
                    </div>
                    <div className="col-3">
                        <p className="h2">
                            {getMeetingsTillNow(Client.current.meetings).length > 0
                                ? getMeetingsTillNow(Client.current.meetings).length
                                : <span>---</span>
                            }
                        </p>
                    </div>
                    <div className="col-4">
                        <p className="h2">{Client.current.paid_meetings}</p>
                    </div>
                </div>
                <div className="row  justify-content-between  flex-nowrap">
                    <div className="col-5">
                        <p className="text-capture pb-2">следующая встреча</p>
                    </div>
                    <div className="col-3">
                        <p className="text-capture pb-2">прошло встреч</p>
                    </div>
                    <div className="col-4">
                        <p className="text-capture pb-2">осталось оплаченных</p>
                    </div>
                </div>
            </div>
        </Card>
    )
});