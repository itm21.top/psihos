import React, {useEffect} from 'react';
import './user.scss'
import {observer} from "mobx-react";
import ClientInfo from "./ClientInfo";
import ClientHistory from "./ClientHistory";
import ClientTasks from "./ClientTasks";
import ClientInfoTabs from "./ClientInfoTabs";
import fetchMyClient from "../actions/fetchMyClient";

export default observer(() => {
    useEffect(() => {
        fetchMyClient();
    }, []);
    return (
        <div className="row justify-content-center client-card">
            <div className="col-xl-8">
                <div className="row row-info-data">
                    <div className="col-md-6 client-info">
                        <ClientInfo/>
                    </div>
                    <div className="col-md-6 client-info">
                        <ClientInfoTabs />
                    </div>
                </div>
                <ClientTasks />
            </div>
            <div className="col-xl-4">
                <ClientHistory />
            </div>
        </div>
    )
});
