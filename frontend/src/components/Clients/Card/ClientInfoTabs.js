import {observer} from "mobx-react";
import React from "react";
import ClientMeetings from "./ClientMeetings";
import Card from "../../../common/card/Card";
import {Tabs, Tab} from "../../../common/tabs";
import DeleteClient from "../DeleteClient"
import Client from "../models/Client";
import toggleDeleteAlert from "../actions/toggleDeleteAlert";
import ToggleButton from "../../../common/forms/components/ToggleButton";
import toggleArchiveClient from "../actions/toggleArchiveClient";
import isDeleted from "../models/selectors/isDeleted";
import ClientForm from '../Form';
import editClient from "../actions/editClient";
import isTutor from "../../../common/auht/selectors/isTutor";

export default observer(() => {
    const clientDataShow = {
        //email: 'E-mail',
        created_at: 'Дата регистрации',
        birthday_string: 'Дата рождения',
        phone: 'Телефон',
        whatsapp: 'Whatsapp',
        skype: 'Skype',
    }
    return (
        <Card className="client-data borders-left-rect">
            <Tabs>
                <Tab name="Инфо">
                    <ClientMeetings/>
                </Tab>
                <Tab name="Личные данные" className="client-data-personal">
                    <div className="row row-client-data-personal">
                        {Object.keys(clientDataShow).map((key, index) => Client.current[key] &&
                            <React.Fragment key={index}>
                                <div className="col-6 col-sm-5">{clientDataShow[key]}</div>
                                <div className="col-6 col-sm-5">{Client.current[key]}</div>
                            </React.Fragment>
                        )}
                    </div>
                    {isTutor() && !isDeleted() &&
                        <ToggleButton selected={Client.current.is_archive}
                                      name="Архивный клиент"
                                      onClick={toggleArchiveClient}
                        />
                    }
                    {isTutor() && !isDeleted() &&
                        <div className="bottom-block">
                            <button className="btn remove-style"
                                    type="button"
                                    onClick={toggleDeleteAlert}
                            >
                                <i className="cil-trash"></i>Удалить клиента
                            </button>
                            <button className="btn remove-style"
                                    type="button"
                                    onClick={editClient}
                            >
                                <i className="cil-pencil"></i>Редактировать клиента
                            </button>
                            {Client.isShowForm && <ClientForm/>}
                            {Client.isShowMiniPopup && <DeleteClient/>}
                        </div>
                    }
                </Tab>
            </Tabs>
        </Card>
    )
});