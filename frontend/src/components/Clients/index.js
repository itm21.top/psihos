import React from 'react';
import ClientsList from './List'
import ClientForm from './Form'
import Client from "./models/Client";
import {observer} from "mobx-react";
import Card from "../../common/card/Card";
import './client.scss';
import '../../common/dataTable/datatable.scss';
import Feetback from "../Feetback/models/Feetback";
import Form2 from "../Feetback/Form";

export default observer(() => {
    return (
        <Card className="client-main-page">
            <ClientsList/>
            {Client.isShowForm && <ClientForm/>}
            {Feetback.isShowForm && <Form2  clientExist={false}/>}

        </Card>
    );
});
