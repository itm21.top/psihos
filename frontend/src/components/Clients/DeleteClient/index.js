import React from "react";
import {observer} from "mobx-react";
import "./deleteClient.scss"
import SubmitButton from "../../../common/forms/components/SubmitButton";
import MiniPopup from "../../../common/miniPopup/MiniPopup";
import toggleDeleteAlert from "../actions/toggleDeleteAlert";
import Button from "../../../common/forms/components/Button";
import asyncHandler from "../../../common/forms/hooks/asyncHandler";
import deleteClient from "../actions/deleteClient";

export default observer(() => {
    const [submit, submitting, submitFailed] = asyncHandler(async () => {
        await deleteClient();
        toggleDeleteAlert();
    }, (requestError) => {
    });

    return (
        <MiniPopup onClose={toggleDeleteAlert}
               className="client-delete"
        >
            <p className="h5">Удалить клиента?</p>
            <p className="sub-h5">Клиент исчезнет из Вашего списка клиентов и из системы в течение 14 дней. Вы можете восстановить профиль клиента в этот срок</p>
            <div className="d-flex">
                <Button
                    className="button-style-gray"
                    name="Отменить"
                    onClick={toggleDeleteAlert}
                >
                </Button>
                <SubmitButton
                    className="button-style"
                    name="Удалить"
                    onClick={submit}
                    submitting={submitting}
                />
            </div>
        </MiniPopup>
    )
});