import Client from "../models/Client";
import api from "../../../common/api";
import mapClient from "../models/selectors/mapClient";

export default async () => {
    const response = await api.get('/client');
    Client.list = response.data.data.map(mapClient);
    Client.recordsTotal = response.data.recordsTotal;
}