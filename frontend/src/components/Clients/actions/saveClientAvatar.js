import Client from "../models/Client";
import api from "../../../common/api";
import fetchClient from "./fetchClient";

export default async (file) => {
    const formData = new FormData();
    formData.append('avatar', file);

    const response = await api.post(
        '/client/' + Client.current.id + '/avatar',
        formData,
        {headers: {'Content-Type': 'multipart/form-data'}}
    );
    fetchClient(Client.current.id);
}