import Client from "../models/Client";
import api from "../../../common/api";
import fetchClient from "./fetchClient";

export default async () => {
    const response = await api.delete('/client/' + Client.current.id);
    fetchClient(Client.current.id);
}