import Client from "../models/Client";

export default (field, value) => {
    Client.current[field] = value;
}