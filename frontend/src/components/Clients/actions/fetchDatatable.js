import Client from "../models/Client";
import mapClient from "../models/selectors/mapClient";
import fetchDatatable from "../../../common/dataTable/actions/fetchDatatable";
import listConfig from "../List/listConfig";

export default () => {
    fetchDatatable({
        url: '/client',
        map: (response) => {
            Client.list = response.data.data.map(mapClient);
            Client.recordsTotal = response.data.recordsTotal;
        },
        config: listConfig
    })
}