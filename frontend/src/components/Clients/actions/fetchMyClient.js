import Client from "../models/Client";
import api from "../../../common/api";
import mapMeeting from "../../Meeting/models/selectors/mapMeeting";
import mapClient from "../models/selectors/mapClient";

export default async (id) => {
    const response = await api.get('/client/me');
    Client.current = mapClient(response.data);
}