import Client from "../models/Client";

export default () => {
    Client.isShowMiniPopup = !Client.isShowMiniPopup;
}