import Client from "../models/Client";
import api from "../../../common/api";
import mapClient from "../models/selectors/mapClient";

export default async (id) => {
    const response = await api.get('/client/' + id);
    Client.current = mapClient(response.data);
}