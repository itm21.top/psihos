import Client from "../models/Client";
import api from "../../../common/api";
import fetchClient from "./fetchClient";
import moment from "moment";
import User from "../../../common/models/User";

export default async () => {
    if (Client.current.id)
    {
        const response = await api.put('/client/' + Client.current.id, {
            ...Client.current,
            birthday: moment(Client.current.birthday).format('YYYY-MM-DD')
        });
        await fetchClient(Client.current.id);
        Client.backUpCurrent = Client.current;
    }
    else
        {
        const response = await api.post('/client', {
            ...Client.current,
            birthday: moment(Client.current.birthday).format('YYYY-MM-DD')
        });
        Client.list = [
            response.data,
            ...Client.list
        ];
        Client.recordsTotal++;

            const response0 = await api.get('/meeting/clients');
            Client.avaliableList = response0.data;

            console.log(response0.data);
    }


}