import Client from "../models/Client";
import {toJS} from "mobx";
export default () => {
   if (Client.backUpCurrent != null) {
      Client.current = toJS(Client.backUpCurrent);
   }
   Client.isShowForm = !Client.isShowForm;
}