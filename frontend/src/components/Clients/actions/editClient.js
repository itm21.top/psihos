import Client from "../models/Client";
import toggleClientForm from "./toggleClientForm";
import {toJS} from "mobx";

export default () => {
    Client.backUpCurrent = toJS(Client.current);
    toggleClientForm();
}