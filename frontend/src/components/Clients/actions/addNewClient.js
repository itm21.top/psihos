import Client from "../models/Client";
import toggleClientForm from "./toggleClientForm";
import getDefaultClient from "../models/selectors/getDefaultClient";

export default () => {
    Client.current = getDefaultClient();
    Client.backUpCurrent = getDefaultClient();
    toggleClientForm();
}