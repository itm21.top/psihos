import Client from "../models/Client";
import saveClient from "./saveClient";
export default () => {
    Client.current.is_archive = +!Client.current.is_archive;
    saveClient();
}