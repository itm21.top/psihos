import Task from "../../Task/models/Task";
import fetchDatatable from "../../../common/dataTable2/actions/fetchDatatable";
import listConfig2 from "../../Clients/Card/listConfig2";
import mapTask from "../../Task/models/selectors/mapTask";

export default () => {
    fetchDatatable({
        url: '/task',
        map: (response) => {

            Task.list = response.data.data.map(mapTask);
            Task.recordsTotal = response.data.recordsTotal;
        },
        config: listConfig2
    })
}