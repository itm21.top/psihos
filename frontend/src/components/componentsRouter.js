import React from 'react';
import Dashboard from "./Calendar";
import Users from "./Users";
import Clients from "./Clients";
import Tasks from "./Task";
import Profile from "./Profile";
import GridExample from "./GridExample";
import ClientCard from './Clients/Card'
import {Redirect} from "react-router-dom";
import ChangeProfilePassword from './Profile/changeProfilePassword'
import MyCard from "./Clients/Card/MyCard";

export default {
    items: [
        {
            name: 'Dashboard',
            path: '/dashboard',
            component: Dashboard,
            strict: true,
            protected: true,
        },
        {
            name: 'Users',
            path: '/users',
            component: Users,
            strict: true,
            protected: true
        },
        {
            name: 'ChangeProfilePassword',
            path: '/profile/me/change-password',
            component: ChangeProfilePassword,
            strict: true,
        },
        {
            name: 'Profile',
            path: '/profile/me',
            component: Profile,
            strict: true,
        },
        {
            name: 'Clients',
            path: '/clients',
            component: Clients,
            strict: true,
            protected: true
        },
        {
            name: 'Tasks',
            path: '/tasks',
            component: Tasks,
            strict: true,
            protected: true
        },
        {
            name: 'Client Card',
            path: '/client/:id',
            component: ClientCard,
            strict: true,
            protected: true
        },
        {
            name: 'Client Card',
            path: '/my-card',
            component: MyCard,
            strict: true,
            protected: true
        },
        {
            name: 'Grid example',
            path: '/grid-example',
            component: GridExample,
            strict: true,
            protected: true,
        },
        {
            name: 'Redirect',
            from: '/',
            to : '/dashboard',
            component: (props) => (<Redirect {...props} />),
        },
    ]
}