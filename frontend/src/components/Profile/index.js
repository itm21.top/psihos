import React, {useState} from 'react';
import Avatar from "../Layouts/components/Avatar/Avatar";
import User from "../../common/models/User";
import {observer} from "mobx-react";
import useFormInput from "../../common/forms/hooks/useFormInput";
import {email as emailValidator, phone as phoneValidator,  required} from "../../common/forms/validators";
import asyncHandler from "../../common/forms/hooks/asyncHandler";
import TextInput from "../../common/forms/components/TextInput";
import SubmitButton from "../../common/forms/components/SubmitButton";
import changeCurrentUser from "../../common/auht/actions/changeCurrentUser";
import DateInput from "../../common/forms/components/DateInput";
import saveProfile from "./actions/saveProfile";
import './profile.scss';
import Card from "../../common/card/Card";
import {useHistory} from 'react-router-dom';
import {Tabs, Tab} from "../../common/tabs";
import PhoneInput from "../../common/forms/components/PhoneInput";
import isTutor from "../../common/auht/selectors/isTutor";
import Feetback from "../Feetback/models/Feetback";
import Form2 from "../Feetback/Form";
import fetchDatatable from "../Task/actions/fetchDatatable";


export default observer(() => {
   // console.log(User.current.date_payment);
    const history = useHistory();
    const goToChangeProfilePassword = () => {
        history.push('/profile/me/change-password')
    }
    const changeProfile = (field, value) => {
        changeCurrentUser(field, value)
        setSuccess(null);
        console.log(value);
    }

    const [success, setSuccess] = useState(null);
    const name = useFormInput(
        [
            required('Имя обязательное поле'),
        ],
        () => User.current.name,
        (value) => changeProfile('name', value)
    );



    const email = useFormInput(
        [
            required('Email обязательное поле'),
            emailValidator('Email введён неверно')
        ],
        () => User.current.email,
        (value) => changeProfile('email', value)
    );

    const phone = useFormInput(
        [
            phoneValidator('Неверный номер телефона')
        ],
        () => User.current.phone,
        (value) => changeProfile('phone', value)
    );

    const profession = useFormInput(
        [
            required('Специальность обязательное поле'),
        ],
        () => User.current.profession,
        (value) => changeProfile('profession', value)
    );

    const whatsapp = useFormInput(
        [
            phoneValidator('Неверный номер телефона')
        ],
        () => User.current.whatsapp,
        (value) => changeProfile('whatsapp', value)
    );

    const birthday = useFormInput(
        [],
        () => User.current.birthday,
        (value) => changeProfile('birthday', value)
    );

    const data_pay = () =>{
        console.log(User.current.date_payment);
        if(User.current.date_payment=='Не оплачено')
        {
            return 'Не оплачено';
        }
        else
        {
            const monthNames = ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября","декабря"];
            let tempdate = new Date(User.current.date_payment);
            let date = tempdate.getDate() + ' ' + monthNames[(tempdate.getMonth())] + ' ' + tempdate.getFullYear();
            return date;
        }

    }

    const dataclose = useFormInput(
        [],
        () => User.current.date_payment,
        (value) => changeProfile('date_payment', value)
    );

    const [isShowWhatsapp, setIsShowWhatsapp] = useState(0);
    const set = () => {
        setIsShowWhatsapp( 1)
    };


    const [submit, submitting] = asyncHandler(async () => {
        await saveProfile();
        setSuccess('Изменения сохранены');
    }, () => {
    });

    const [submit2] = asyncHandler(async () => {
        window.location.href='https://forms.tildacdn.com/payment/tinkoff/';
        return null;

    }, () => {
    });

    const submitButtonActive = [
        email,

    ].every(field => field.isValid);

    if (!User.current.id) {
        return null;
    }


    return (
        <Card className="client-card-settings">
            <p className="h3">Настройки</p>
            <Tabs>
                <Tab name="Личные данные">
                    <div className="row">
                        <div className="col-xl-8">
                            <Avatar avatar={User.current.avatar}
                                    name={User.current.name ?? User.current.email}
                                    editable
                            />
                            <div className="row row-form">
                                <TextInput
                                    disabled={submitting}
                                    className="col-md-6"
                                    label="Имя и фамилия"
                                    {...name}
                                />
                                <TextInput
                                    disabled={submitting}
                                    className="col-md-6"
                                    label="Email"
                                    {...email}
                                />
                                <PhoneInput
                                    disabled={submitting}
                                    className="col-md-6"
                                    label="Номер телефона"
                                    prependText=""
                                    // defaultCountry="RU"
                                    {...phone}
                                />
                                <DateInput
                                    disabled={submitting}
                                    className="col-md-6"
                                    label="Дата роджения"
                                    groupIconAppend="cil-calendar"
                                    showMonthDropdown
                                    showYearDropdown
                                    {...birthday}
                                />
                                {isTutor() &&
                                <TextInput
                                    disabled={submitting}
                                    className="col-md-6"
                                    label="Специальность"
                                    {...profession}
                                />
                                }
                                {isShowWhatsapp === 0 &&
                                <div className="input-box col-md-6">
                                    <div className="whatsapp-wrapper" onClick={set}>
                                        <i className="cib-whatsapp"></i>
                                        <p>Кликни если нужен номер для Whatsapp</p>
                                    </div>
                                </div>
                                }
                                {isShowWhatsapp === 1 &&
                                <PhoneInput
                                    disabled={submitting}
                                    className="col-md-6"
                                    label="WhatsApp"
                                    {...whatsapp}
                                />
                                }
                            </div>
                            {success &&
                            <div>
                                <div className="col-sm-12 alert alert-success" role="alert">
                                    {success}
                                </div>
                            </div>
                            }
                            <div className="row align-items-center">
                                <div className="col-sm-auto margin-top">
                                    <SubmitButton
                                        submitting={submitting}
                                        className=""
                                        disabled={!submitButtonActive}
                                        name="Сохранить"
                                        onClick={submit}
                                    />
                                </div>

                                <div className="col-sm-auto margin-top">
                                    <div onClick={goToChangeProfilePassword} className="text-link">Изменить пароль</div>
                                </div>
                            </div>
                            {/*<div className="social-button-enter-list">
                                <p className="h4">Привязка к социальным сетям</p>
                                <div className="row">
                                    <div className="col-sm-auto">
                                        <div className="social-button-enter">
                                            <div className="social-button-small facebook"></div>
                                            <div className="name">
                                                <p>{User.current.name}</p>
                                                <p className="text-link">Отвязать</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-auto">
                                        <div className="social-button google">Вход через Google</div>
                                    </div>
                                </div>
                            </div>*/}
                        </div>
                    </div>
                </Tab>
                <Tab name="Оплата">
                    <div className="row">
                        <div className="col-xl-8">
                            Дата окончания подписки -  {data_pay()}
                        </div>

                    </div>
                    <div className="row"></div>
                    <div className="row">

                        <div class="prices__card js-price-card">
                            <div class="prices__card-top" >
                                <div class="prices__card-top-col" >
                                    <span class="prices__card-name" >Месячный</span>
                                    <span class="prices__card-price prices__card-price">1300</span>
                                </div>
                            </div>
                            
                            <div class="prices__card-bottom" >
                                <div class="prices__card-bottom-col">
                                    <span class="prices__card-per-month" >1300<span> ₽</span>
                                    </span>
                                    <span class="prices__card-text">в месяц</span>
                                </div>
                                <div class="prices__card-bottom-col">
                                    <span class="prices__card-days" >30</span>
                                    <span class="prices__card-text" >дней</span>
                                </div>
                            </div>
                            <form action="https://welcome.life-story.online/pay" method="GET">

                                <input  type="text" className="prom" name="promo" placeholder='Промокод' />
                                <input className="tinkoffPayRow" type="hidden" name="amount" value="1300"/>
                                <input className="tinkoffPayRow" type="hidden" name="name_prog" value='Месячный'/>
                                <input className="tinkoffPayRow" type="hidden" name="email" value={User.current.email}/>
                                <input type='submit'  className="submit-button " name="Оплатить" value="Оплатить" />
                            </form>
                        </div>

                        <div class="prices__card js-price-card">
                            <div class="prices__card-top" >
                                <div class="prices__card-top-col" >
                                    <span class="prices__card-name" >Трёхмесячный</span>
                                    <span class="prices__card-price prices__card-price">3450</span>
                                </div>
                            </div>
                            <div class="prices__card-sale" >Экономия ≈ 12% в месяц</div>
                            <div class="prices__card-bottom" >
                                <div class="prices__card-bottom-col">
                                    <span class="prices__card-per-month" >1150<span> ₽</span>
                                    </span>
                                    <span class="prices__card-text">в месяц</span>
                                </div>
                                <div class="prices__card-bottom-col">
                                    <span class="prices__card-days" >90</span>
                                    <span class="prices__card-text" >дней</span>
                                </div>
                            </div>
                            <form action="https://welcome.life-story.online/pay" method="GET">

                                <input  type="text" className="prom" name="promo" placeholder='Промокод' />
                                <input className="tinkoffPayRow" type="hidden" name="amount" value="3450"/>
                                <input className="tinkoffPayRow" type="hidden" name="name_prog" value='Трёхмесячный'/>
                                <input className="tinkoffPayRow" type="hidden" name="email" value={User.current.email}/>
                                <input type='submit'  className="submit-button " name="Оплатить" value="Оплатить" />
                            </form>
                        </div>

                        <div class="prices__card js-price-card">
                            <div class="prices__card-top" >
                                <div class="prices__card-top-col" >
                                    <span class="prices__card-name" >Полугодовой</span>
                                    <span class="prices__card-price prices__card-price">6000</span>
                                </div>
                            </div>
                            <div class="prices__card-sale" >Экономия ≈ 23% в месяц</div>
                            <div class="prices__card-bottom" >
                                <div class="prices__card-bottom-col">
                                    <span class="prices__card-per-month" >1000<span> ₽</span>
                                    </span>
                                    <span class="prices__card-text">в месяц</span>
                                </div>
                                <div class="prices__card-bottom-col">
                                    <span class="prices__card-days" >180</span>
                                    <span class="prices__card-text" >дней</span>
                                </div>
                            </div>

                            <form action="https://welcome.life-story.online/pay" method="GET">

                                <input  type="text" className="prom" name="promo" placeholder='Промокод' />
                                <input className="tinkoffPayRow" type="hidden" name="amount" value="6000"/>
                                <input className="tinkoffPayRow" type="hidden" name="name_prog" value='Полугодовой'/>
                                <input className="tinkoffPayRow" type="hidden" name="email" value={User.current.email}/>
                                <input type='submit'  className="submit-button " name="Оплатить" value="Оплатить" />
                            </form>
                        </div>
                    </div>
                </Tab>
            </Tabs>
            {Feetback.isShowForm && <Form2 saveCallback={() => fetchDatatable()} clientExist={false}/>}

        </Card>
    )
});
