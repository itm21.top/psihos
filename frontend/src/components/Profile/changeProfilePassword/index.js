import React, {useState} from 'react';
import User from "../../../common/models/User";
import {observer} from "mobx-react";
import useFormInput from "../../../common/forms/hooks/useFormInput";
import {email as emailValidator, password as passwordValidator, required} from "../../../common/forms/validators";
import asyncHandler from "../../../common/forms/hooks/asyncHandler";
import TextInput from "../../../common/forms/components/TextInput";
import SubmitButton from "../../../common/forms/components/SubmitButton";
import changeCurrentUser from "../../../common/auht/actions/changeCurrentUser";
import saveProfile from "../actions/saveProfile";
import '../profile.scss';
import Card from "../../../common/card/Card";
import TextLink from "../../../common/forms/components/TextLink";
import restorePassword from "../../../common/auht/actions/restorePassword";


export default observer(() => {
    const [showSendPassword, setShowSendPassword] = useState(false);

    const email = useFormInput(
        [
            required('Email обязательное поле'),
            emailValidator('Email введён неверно')
        ],
        () => User.current.email,
        (value) => changeCurrentUser('email', value)
    );

    const passwordCurrent = useFormInput(
        [
            required('Password is required'),
            passwordValidator('Пароль введен неверно'),
        ],
        () => User.current.password,
        (value) => changeCurrentUser('password', value)
    );

    const passwordNew = useFormInput(
        [
            required('Password is required'),
            passwordValidator('Пароль должен содержать буквы латинского алфавита, как минимум одну заглавную букву и одну цифру'),
        ],
        () => User.current.password,
        (value) => changeCurrentUser('password', value)
    );

    const passwordConfirm = useFormInput(
        [
            required('Password is required'),
            passwordValidator('Пароль должен содержать буквы латинского алфавита, как минимум одну заглавную букву и одну цифру'),
            value => value !== User.current.password ? 'Пароли должны совпадавть' : undefined
        ],
        () => User.current.password_confirm,
        (value) => changeCurrentUser('password_confirm', value)
    )

    const [submit, submitting, submitFailed] = asyncHandler(
        async () => {
            await saveProfile();
        }, (requestError) => {
    });

    const [restoreSubmit, restoreSubmitting, restoreSubmitFailed] = asyncHandler(async () => {
        setFormError(null);
        await restorePassword();
        setShowResultMessage(true);
    }, (requestError) => {
        setFormError('Вы ввели неверный E-mail!');
    });

    const submitButtonActive = [
        email,
    ].every(field => field.isValid);

    return (
        <div className="row">
            <div className="col-xl-8">
                <Card className="client-card-change">
                    <p className="h3">Изменить пароль</p>
                    <div className="row row-form">
                        <TextInput
                            disabled={submitting}
                            className="col-md-6 order-md-1"
                            type="password"
                            label="Текущий пароль"
                            {...passwordCurrent}
                        />
                        {!showSendPassword &&
                            <div className="col-md-6 order-md-2">
                                <TextLink onClick={() => setShowSendPassword(true)}>
                                    Забыли пароль?
                                </TextLink>
                            </div>
                        }
                        {showSendPassword &&
                            <TextInput
                                disabled={submitting}
                                className="col-md-6 order-md-4"
                                label="Ваш e-mail"
                                groupIconText="Выслать"
                                {...email}
                            />
                        }
                    </div>
                    <div className="row row-form">
                        <TextInput
                            disabled={submitting}
                            className="col-md-6  order-md-3"
                            type="password"
                            label="Новый пароль"
                            {...passwordNew}
                        />

                        <TextInput
                            disabled={submitting}
                            className="col-md-6 order-md-5"
                            type="password"
                            label="Повторите пароль"
                            {...passwordConfirm}
                        />
                        <div className="col-12 order-md-6">
                            <div className="row">
                                <div className="col-md-auto">
                                    <SubmitButton
                                        submitting={submitting}
                                        className=""
                                        disabled={!submitButtonActive}
                                        name="Изменить"
                                        onClick={submit}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </Card>
            </div>
        </div>
    )
});