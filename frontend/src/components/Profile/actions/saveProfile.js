import api from "../../../common/api";
import User from "../../../common/models/User";
import mapUser from "../../../common/models/selectors/mapUser";
import moment from "moment";

export default async () => {

    const response = await api.put('/user/me', {
        ...User.current,
        birthday: moment(User.current.birthday).format('YYYY-MM-DD')
    });

    User.current = mapUser(response.data.user);
}