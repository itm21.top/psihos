import React, {useState} from 'react';
import toggleFeetbackAddForm from "../action/toggleFeetbackForm";
import {observer} from "mobx-react";
import Feetback from "../../Feetback/models/Feetback";
import asyncHandler from "../../../common/forms/hooks/asyncHandler";

import Popup from "../../../common/popup/Popup";
import SubmitButton from "../../../common/forms/components/SubmitButton";
import SelectInput from "../../../common/forms/components/SelectInput";
import TextArea from "../../../common/forms/components/TextArea";
import useFormInput from "../../../common/forms/hooks/useFormInput";

import changeCurrentFeetback from "../action/changeCurrentFeetback";
import saveFeetback from "../action/saveFeetback";
import Meeting from "../../Meeting/models/Meeting";
import api from "../../../common/api";


export default observer(() => {
    const [success, setSuccess] = useState(null);
    const meetingTypes = [
        {
            label: 'Предложения по функциналу',
            value: 'funku',

        },
        {
            label: 'Технические вопросы',
            value: 'tech',

        },
    ];

    const onChangeDate = (field, value) => {

        Feetback.current[field] = value;

    }

    const namelist = useFormInput(
        [
            /*required('Опиание встречи обязательное поле'),*/
        ],
        () => Meeting.current.message,
        (value) => onChangeDate('message', value)
    );

    const [submit] = asyncHandler(async () => {

        setSuccess('Сообщение отправлено!');

        const response = api.post('/feedback', {...Feetback.current});
        console.log(response);



    }, () => {
    });

    const type = useFormInput(
        [],
        () => meetingTypes.filter(option => option.value === Feetback.current.type),
        (value) => {
            changeCurrentFeetback('type', value.value)
        }
    );



    return (
        <Popup header={'Обратная связь'}
               onClose={toggleFeetbackAddForm}>
            {
            <p>Есть вопросы или предложения? Хотите высказаться по поводу функционала и предложить что-то свое? Воспользуйтесь формой обратной связи. Наш менеджер обязательно ответит на Ваш запрос<br/> Также по вопросам поддержки можно обратиться через WhatsApp или telegram по номеру <a href="tel:+7(969)089-04-11">+7(969)089-04-11</a></p>
            }

            <div>
                <div className="input-select-wrapper">
                    <SelectInput options={meetingTypes}
                                 label="Выьерите категорию"
                                 placeholder={'Выбрать...'}
                                 {...type}
                                 />
                </div>
            </div>

            <div className="mb-4 mesagefeetback">
                <TextArea

                    label="Сообщение"
                    {...namelist}
                />
            </div>
            {success &&
            <div className="col-sm-12 alert alert-success mt-10" role="alert">
                {success}
            </div>
            }
             <SubmitButton name={'Отправить' } onClick={submit} />

        </Popup>
    )
});
