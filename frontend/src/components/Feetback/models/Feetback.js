import {observable} from "mobx";

const Feetback = observable({
    current: {
        email: '',
        name: '',
        type: '',
        message: ''
    },
    backUpCurrent: null,
    isShowForm: false,

});

export default Feetback;