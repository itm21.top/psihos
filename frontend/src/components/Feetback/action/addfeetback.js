import Feetback from "../models/Feetback";
import {toJS} from "mobx";
export default () => {
    if (Feetback.backUpCurrent != null) {
        Feetback.current = toJS(Feetback.backUpCurrent);
    }
    Feetback.isShowForm = !Feetback.isShowForm;
}