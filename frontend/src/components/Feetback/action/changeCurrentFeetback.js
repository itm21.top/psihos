import Feetback from '../models/Feetback';

export default (field, value) => {
    Feetback.current[field] = value;
}