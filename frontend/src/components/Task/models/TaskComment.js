import {observable} from "mobx";

const TaskComment = observable({
    current: {
        task_id: 0,
        user_id: 0,
        text: '',
    },
    backUpCurrent: null,
    isShowForm: false,
    isShowMiniPopup: false,
});

export default TaskComment;

