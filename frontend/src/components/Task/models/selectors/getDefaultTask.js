import User from "../../../../common/models/User";

export default () => {
    return {
        tutor_id: User.current.id,
        client_id: 0,
        name: '',
        is_complete: 0,
        comments: [],
        deadline_date: new Date()
    }
}