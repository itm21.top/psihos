import Task from "../Task";

export default (tasks) => Task.showCompleted
    ? tasks
    : tasks.filter(task => !task.is_complete);
