import moment from "moment";
import mapTaskComment from "./mapTaskComment";

export default (task) => {
    return {
        ...task,
        deadline_date_formatted: moment(task.deadline_date).format('DD.MM.YYYY'),
        deadline_date: moment(task.deadline_date).toDate(),
        creationDate: moment(task.created_at),
        sucsekful: task.is_complete ? 'Выполнено' : '',
        comments: task.comments ? task.comments.map(mapTaskComment) : []
    }
}