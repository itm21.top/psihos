const completedTasks = (tasks) => tasks.filter(task => task.is_complete).length;

export default (tasks) => {
    return tasks.length > 0
        ? {total: tasks.length, completed: completedTasks(tasks)}
        : {total: '-', completed: '-'}
}