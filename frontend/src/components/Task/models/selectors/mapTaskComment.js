import moment from "moment";

export default (comment) => {
    return {
        ...comment,
        creationDate: moment(comment.created_at)
    }
}