import {observable} from "mobx";

const Task = observable({
    current: {
        client_id: 0,
        name: '',
        deadline_date: new Date(),
        is_complete: 0,
        comments: [],
    },
    backUpCurrent: null,
    recordsTotal: 0,
    currentTasks: [],
    list: [],
    showCompleted: false,
    isShowForm: false,
    isShowMiniPopup: false,
});

export default Task;

