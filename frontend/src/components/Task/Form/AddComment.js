import React from 'react';
import {observer} from "mobx-react";
import TextInput from "../../../common/forms/components/TextInput";
import useFormInput from "../../../common/forms/hooks/useFormInput";
import {required} from "../../../common/forms/validators";
import TaskComment from "../models/TaskComment";
import saveTaskComment from "../actions/saveTaskComment";
import toggleTaskCommentForm from "../actions/toggleTaskCommentForm";
import fetchDatatable from "../../Clients/actions/fetchDatatable2";

export default observer(({task}) => {
    const onAddComment = async () => {
        if (TaskComment.current.text.length > 0) {
            await saveTaskComment();
            toggleTaskCommentForm();
        }
    }

    const text = useFormInput(
        [
            required('Комментарий не должен быть пустым'),
        ],
        () => TaskComment.current.text,
        (value) => TaskComment.current.text = value
    );
    fetchDatatable();
    return (
        <div className="offset-1 col-11">
            <TextInput
                {...text}
                placeholder="Ваш комментарий..."
                groupIconText={TaskComment.current.id ? "Редактировать" : "Написать"}
                isGroupIconButton={true}
                onClickGroupButton={onAddComment}
            />
        </div>
    )
})