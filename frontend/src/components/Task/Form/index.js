import React, {useState} from 'react';
import toggleTaskForm from "../actions/toggleTaskForm";
import {observer} from "mobx-react";
import useFormInput from "../../../common/forms/hooks/useFormInput";
import {required} from "../../../common/forms/validators";
import Task from "../models/Task";
import asyncHandler from "../../../common/forms/hooks/asyncHandler";
import changeCurrentTask from "../actions/changeCurrentTask";
import Popup from "../../../common/popup/Popup";
import SubmitButton from "../../../common/forms/components/SubmitButton";
import saveTask from "../actions/saveTask";
import TextArea from "../../../common/forms/components/TextArea";
import DateInput from "../../../common/forms/components/DateInput";
import Client from "../../Clients/models/Client";
import SelectInput from "../../../common/forms/components/SelectInput";
import isTutor from "../../../common/auht/selectors/isTutor";
import ToggleButton from "../../../common/forms/components/ToggleButton";
import addNewClient from "../../Clients/actions/addNewClient";
import fetchDatatable from "../../Clients/actions/fetchDatatable2";

export default observer(({saveCallback, clientExists = true}) => {
        const [success, setSuccess] = useState(null);
        const [isForTutor, setIsForTutor] = useState(Task.current.id && Task.current.client_id === 0);

        const name = useFormInput(
            [
                required('Заполните текст задачи'),
            ],
            () => Task.current.name,
            (value) => changeCurrentTask('name', value)
        );

        const date = useFormInput(
            [],
            () => Task.current.deadline_date,
            (value) => changeCurrentTask('deadline_date', value)
        );

        const clientOptions = Client.avaliableList.reduce((acc, client) => {
            if ((client.deleted_at === null
                && client.is_archive === 0)
                || Task.current.client_id === client.id
            ) {
                acc.push({
                    label: client.full_name,
                    value: client.id
                });
            }
            return acc;
        }, []);

        const client = useFormInput(
            [
                required('Вы должны выбрать клиента')
            ],
            () => clientOptions.filter(option => option.value === Task.current.client_id),
            (value) => changeCurrentTask('client_id', value.value)
        );

        const [submit, submitting, submitFailed] = asyncHandler(async () => {
            const clientId = clientExists ? Client.current.id : Task.current.client_id;
            await saveTask(clientId);
            if (Task.current.id) {
                Task.isShowForm = false;
            } else {
                setSuccess('Задача добавлена!');
            }
            saveCallback();
        }, (requestError) => {
            console.log(requestError);
        });

        const submitRules = [name];
        if (!clientExists && !isForTutor) {
            submitRules.push(client);
        }
        const submitButtonActive = submitRules.every(field => field.isValid);
        {fetchDatatable()}
        return (
            <Popup header={Task.current.id ? 'Редактировать задачу' : 'Добавить задачу'}
                   onClose={toggleTaskForm}>

                {(isTutor()) && !clientExists && <ToggleButton name="назначить себе"
                                                               className="pb-3"
                                                               selected={isForTutor}
                                                               onClick={() => {
                                                                   Task.current.client_id = 0;
                                                                   setIsForTutor(!isForTutor);
                                                               }}
                />
                }

                {(!isForTutor && isTutor()) && !clientExists &&
                <div className="mt-3 mb-4">
                    <SelectInput options={clientOptions}
                                 label="Клиент *"
                                 placeholder={'Выбрать...'}
                                 {...client}
                    />
                </div>
                }

                <div className="mb-4">
                    <TextArea
                        disabled={submitting || success}
                        label="Задача *"
                        {...name}
                    />
                </div>
                <div className="mb-4">
                    <DateInput
                        disabled={submitting}
                        label="Дедлайн"
                        groupIconAppend="cil-calendar"
                        {...date}
                    />
                </div>
                {success &&
                <div className="col-sm-12 alert alert-success mt-10" role="alert">
                    {success}
                </div>
                }
                {!success
                    ? <SubmitButton name={Task.current.id ? 'Редактировать' : 'Добавить'}
                                    onClick={submit} disabled={!submitButtonActive}/>
                    : <SubmitButton name={'Закрыть'}
                                    onClick={toggleTaskForm}/>
                }
            </Popup>
        )
    }
);
