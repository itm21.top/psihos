import TaskComment from "../models/TaskComment";
import toggleTaskCommentForm from "./toggleTaskCommentForm";
import fetchDatatable from "../../Clients/actions/fetchDatatable2";

export default async (comment) => {
    TaskComment.current = comment;
    TaskComment.backUpCurrent = comment;
    toggleTaskCommentForm();
    fetchDatatable();
}