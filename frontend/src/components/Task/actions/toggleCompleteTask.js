import Task from "../models/Task";
import api from "../../../common/api";
import mapTask from "../models/selectors/mapTask";
import saveTask from "./saveTask";
import fetchDatatable from "./fetchDatatable";
import Client from "../../Clients/models/Client";

export default async (id) => {
    const response = await api.get('/task/' + id);
    const task = mapTask(response.data);
    task.is_complete = task.is_complete ? 0 : 1;
    Task.current = task;
    Task.backUpCurrent = task;
    await saveTask();

}