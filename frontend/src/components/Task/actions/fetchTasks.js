import Task from "../models/Task";
import api from "../../../common/api";
import mapTask from "../models/selectors/mapTask";

export default async () => {
    const response = await api.get('/client');
    Task.list = response.data.data.map(mapTask);
    Task.recordsTotal = response.data.recordsTotal;
}