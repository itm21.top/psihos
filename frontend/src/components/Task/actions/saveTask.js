import Task from "../models/Task";
import api from "../../../common/api";
import moment from "moment";
import Client from "../../Clients/models/Client";

export default async (clientId) => {
    if (Task.current.id) {
        const response = await api.put('/task/' + Task.current.id, {
            ...Task.current,
            deadline_date: moment(Task.current.deadline_date).format('YYYY-MM-DD')
        });

        Task.backUpCurrent = Task.current;
    } else {
        const response = await api.post('/task', {
            ...Task.current,
            client_id: clientId,
            deadline_date: moment(Task.current.deadline_date).format('YYYY-MM-DD')
        });
    }

}