import Task from "../models/Task";
import {toJS} from "mobx";
export default () => {
   if (Task.backUpCurrent != null) {
      Task.current = toJS(Task.backUpCurrent);
   }
   Task.isShowForm = !Task.isShowForm;
}