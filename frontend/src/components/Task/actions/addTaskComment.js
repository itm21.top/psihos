import TaskComment from "../models/TaskComment";
import getDefaultTaskComment from "../models/selectors/getDefaultTaskComment";
import toggleTaskCommentForm from "./toggleTaskCommentForm";

export default (taskId) => {
    TaskComment.current = getDefaultTaskComment(taskId);
    TaskComment.backUpCurrent = getDefaultTaskComment(taskId);
    toggleTaskCommentForm();
}