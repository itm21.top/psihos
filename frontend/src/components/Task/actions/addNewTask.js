import Task from "../models/Task";
import toggleTaskForm from "./toggleTaskForm";
import getDefaultTask from "../models/selectors/getDefaultTask";

export default () => {
    Task.current = getDefaultTask();
    Task.backUpCurrent = getDefaultTask();
    toggleTaskForm();
}