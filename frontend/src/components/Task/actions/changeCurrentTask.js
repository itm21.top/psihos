import Task from "../models/Task";

export default (field, value) => {
    Task.current[field] = value;
}