import Task from "../models/Task";
import fetchDatatable from "../../../common/dataTable/actions/fetchDatatable";
import listConfig from "../List/listConfig";
import mapTask from "../models/selectors/mapTask";

export default () => {
    fetchDatatable({
        url: '/task',
        map: (response) => {
            Task.list = response.data.data.map(mapTask);
            Task.recordsTotal = response.data.recordsTotal;
        },
        config: listConfig
    })
}