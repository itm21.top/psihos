import TasksDateRanges from "../constants/TasksDateRanges";
import listConfig from "../List/listConfig";
import fetchDatatable from "./fetchDatatable";


export default async (range) => {
    const newType = Object.keys(TasksDateRanges)
            .find(key => key === range.value)
        ?? 'ALL';

    listConfig.filters.dateRange = newType;
    fetchDatatable();
}