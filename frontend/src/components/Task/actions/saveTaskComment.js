import api from "../../../common/api";
import fetchClient from "../../Clients/actions/fetchClient";
import Client from "../../Clients/models/Client";
import TaskComment from "../models/TaskComment";

export default async () => {
    if (TaskComment.current.id) {
        const response = await api.put('/task/' + TaskComment.current.task_id + '/comment/' + TaskComment.current.id, {
            ...TaskComment.current,
        });

        TaskComment.backUpCurrent = TaskComment.current;
    } else {
        const response = await api.post('/task/' + TaskComment.current.task_id + '/comment', {
            ...TaskComment.current,
        });
    }
    await fetchClient(Client.current.id);
}