import api from "../../../common/api";
import fetchClient from "../../Clients/actions/fetchClient";
import Client from "../../Clients/models/Client";
import fetchDatatable from "../../Clients/actions/fetchDatatable2";

export default async (comment) => {
    await api.delete('/task/' + comment.task_id + '/comment/' + comment.id);
    await fetchClient(Client.current.id);
    fetchDatatable();
}