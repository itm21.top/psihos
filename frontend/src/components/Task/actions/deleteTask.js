import api from "../../../common/api";
import fetchClient from "../../Clients/actions/fetchClient";
import Client from "../../Clients/models/Client";

export default async (id) => {
    const response = await api.delete('/task/' + id);
    if (Client.current.id) {
        await fetchClient(Client.current.id);
    }
}