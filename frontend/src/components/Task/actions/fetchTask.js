import Task from "../models/Task";
import api from "../../../common/api";
import mapTask from "../models/selectors/mapTask";

export default async (id) => {
    const response = await api.get('/task/' + id);
    Task.current = mapTask(response.data);
    Task.current.is_changed
}