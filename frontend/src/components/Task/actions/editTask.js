import Task from "../models/Task";
import toggleTaskForm from "./toggleTaskForm";
import api from "../../../common/api";
import mapTask from "../models/selectors/mapTask";

export default async (id) => {
    const response = await api.get('/task/' + id);
    const task = mapTask(response.data);
    Task.current = task;
    Task.backUpCurrent = task;
    toggleTaskForm();
}