import {toJS} from "mobx";
import TaskComment from "../models/TaskComment";
export default () => {
   if (TaskComment.backUpCurrent != null) {
      TaskComment.current = toJS(TaskComment.backUpCurrent);
   }
   TaskComment.isShowForm = !TaskComment.isShowForm;
}