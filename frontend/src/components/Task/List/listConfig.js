import React, {useState} from "react";
import Avatar from "../../Layouts/components/Avatar/Avatar";
import makeTableConfig from "../../../common/dataTable/makeTableConfig";
import getClientRole from "../../Clients/models/selectors/getClientRole";
import moment from "moment";
import { Link } from 'react-router-dom';
import editTask from "../actions/editTask";
import toggleCompleteTask from "../actions/toggleCompleteTask";
import deleteTask from "../actions/deleteTask";
import ContextMenu from "../../../common/contextMenu/ContextMenu";
import fetchDatatable from "../actions/fetchDatatable";
import isTutor from "../../../common/auht/selectors/isTutor";
import User from "../../../common/models/User";
import getCurrentUserRole from "../../../common/auht/selectors/getCurrentUserRole";
import UserRoles from "../../../common/auht/constants/UserRoles";

const toggleTaskState = async (taskId) => {
    await toggleCompleteTask(taskId);
    await fetchDatatable();
}

const deleteTaskAction = async (taskId) => {
    await deleteTask(taskId);
    await fetchDatatable();
}
let active = 0;
const deleteTaskAction2 =
    async (a) => {
        active = a;
        let element = document.getElementById(a);
        element.classList.add('active');
        console.log(a);
    }
const deleteTaskAction3 =
    async (a) => {

        let element = document.getElementById(a);
        element.classList.remove('active');
        console.log(a);
    }

export const getConfig = () => ({
    columns: [
        {
            data: 'client.full_name',
            name: 'Клиент',
            isNotVisible: User.current.role !== UserRoles.TUTOR,
            props: {
                width: '25%',
            },
            searchable: true,
            orderable: true,
            render: (row) => <div className="row col-12">

                {row.client_id
                    ? <Link to={'/client/' + row.client.id}>
                        <Avatar avatar={row?.client.avatar}
                                name={row.client.full_name}
                                role={getClientRole(row)}
                        />
                    </Link>
                    : <Avatar avatar={User.current.avatar}
                              name={User.current.name ?? User.current.email}
                              role={getCurrentUserRole()}
                    />

                }


            </div>
        },





       /* {
            data: 'id',
            isNotVisible: User.current.role !== UserRoles.TUTOR,
            name: 'Кому',
            props: {
                width: '75px',
            },
            render: row => row.client_id ? 'Клиенту' : 'Мне'
        },*/


        {
            data: 'name',
            name: 'Задача',
            orderable: true,
            props: {
                width: 'auto',
            },
            render: (row) => <div className="col-12">
                    <div id={row.id} className="zuzuzba">
                        <div  className={active == row.id ? "col-auto col-task-name active" : "col-auto col-task-name "}  onClick={()=>deleteTaskAction2(row.id)}>
                            {row.is_complete ?  <span className="is-complete">{row.name}</span> : <span >{row.name}</span>}
                        </div>
                        <span className={active == row.id ? " close_shir2 active" : " close_shir2"} onClick={()=>deleteTaskAction2(row.id)}>Подробнее</span>
                        <span className={active == row.id ? "close_shir active" : "close_shir"} onClick={()=>deleteTaskAction3(row.id)}>Закрыть</span>
                    </div>
            </div>
        },

        {
            data: 'created_at',
            name: ' ',
            orderable: true,
            props: {
                width: '100px',
            },
            render: row => <div>
                {row.sucsekful ? <span><i className="cil-check-circle"></i>  </span> : ''}
                {/*moment(row.created_at).format('DD.MM.YYYY')*/}
            </div>
        },

       /* {
            data: 'deadline_date',
            name: 'Дедлайн',
            orderable: true,
            props: {
                width: '90px',

            },
            render: row => row.deadline_date ? row.deadline_date_formatted : '---'
        },
        {
            data: 'comments_number',
            name: 'Реакция',
            isNotVisible: User.current.role !== UserRoles.TUTOR,
            orderable: true,
            props: {
                width: '70px',
                align: 'center',
            },
            render: row => row.comments_number

        },*/

        {
            data: 'id',
            name: '',
            props: {
                width: '50px',
                align: 'flex-end',
            },
            searchable: false,
            orderable: false,
            render: (row) => isTutor() && <div className="col-1">
                <ContextMenu>
                    <div className="context-menu-inner">
                        <p className="h5">Задача</p>
                        <ul className="menu">
                            <li onClick={() => editTask(row.id)}>Редактировать</li>
                            <div className="border-line"></div>
                            {(!row.is_complete
                                    ? <li onClick={() => toggleTaskState(row.id)}>Отметить выполненой<i className="cil-check-circle"></i></li>
                                    : <li onClick={() => toggleTaskState(row.id)}>Отметить невыполненой<i className="cil-check-circle"></i></li>
                            )}
                            <li onClick={() => deleteTaskAction(row.id)}><span className="trash">Удалить</span><i className="cil-trash"></i></li>
                        </ul>
                    </div>
                </ContextMenu>
            </div>
        },

    ],
    order: [{column: 0, dir: 'desc'}],
    start: 0,
    length: 10,
    onRowClick: () => {},
    search: {
        value: "",
        regex: false
    },
    filters: {
        dateRange: 'ALL',
        completed: 0,
        deleted: 0
    }
});

export default makeTableConfig(getConfig());