import React, {useEffect} from 'react';
import Task from "../models/Task";
import {observer} from "mobx-react";
import listConfig, {getConfig as getListConfig } from "./listConfig";
import DataTable from "../../../common/dataTable";
import fetchDatatable from "../actions/fetchDatatable";
import declOfNumber from "../../../common/declOfNumber";
import TextInput from "../../../common/forms/components/TextInput";
import useFormInput from "../../../common/forms/hooks/useFormInput";
import SelectInput from "../../../common/forms/components/SelectInput";
import ToggleButton from "../../../common/forms/components/ToggleButton";
import SelectTaskDateRanges from "../constants/SelectTaskDateRanges";
import selectTasksDateRange from "../actions/selectTasksDateRange";
import Button from "../../../common/forms/components/Button";
import addNewTask from "../actions/addNewTask";
import isTutor from "../../../common/auht/selectors/isTutor";
import User from "../../../common/models/User";
import addNewClient from "../../Clients/actions/addNewClient";
import moment from "moment";

export default observer(() => {
    const search = useFormInput(
        [],
        () => listConfig.search.value,
        (value) => {
            listConfig.search.value = value;
            fetchDatatable();
        }
    );

    if (!User.current.id) {
        return ''
    }

    useEffect(() => {
        listConfig.columns = getListConfig().columns;
    }, [User.current]);
    let tempdate = new Date(User.current.date_payment);
    const datelimit = moment(tempdate, 'YYYY-MM-DDTHH:mm:ss.SSSZ');
    const now = moment();
    return (
        <React.Fragment>
            <div className="tasks-list">
                <div className="common-card-sticky-head">
                    <div className="card-head">
                        <div className="row mb-0">
                            <div className="col-md-3 mb-3">
                                <div className="saba-head">
                                    <p className="h3">Задачи</p>
                                    <p className="card-qty">
                                        <span>{Task.list.length} </span>
                                        {declOfNumber(Task.recordsTotal, ['задача', 'задачи', 'задач'])}
                                        <span> из {Task.recordsTotal}</span>
                                    </p>
                                </div>
                                {isTutor() && !now.isAfter(datelimit) &&  <Button className="button-style but-tusck za234 but-client but"
                                                                                  name="+ &nbsp; Добавить клиента"
                                                                                  onClick={() => addNewClient()}
                                />
                                }
                                {isTutor() && !now.isAfter(datelimit) && <Button className="button-style za234 but-tusck"
                                                                                 name="+ &nbsp; Добавить задачу"
                                                                                 onClick={addNewTask}/>
                                }
                            </div>
                            <div className="card-filters-wrapper col-md-9">
                                <div className="date-range-select mb-3 mb-md-0">
                                    <SelectInput options={SelectTaskDateRanges}
                                                 value={SelectTaskDateRanges.find(type => type.value === listConfig.filters.dateRange)}
                                                 onChange={range => selectTasksDateRange(range)}
                                                 placeholder={'Выбрать...'}
                                    />
                                </div>
                                <TextInput
                                    placeholder="Поиск по задачам"
                                    groupIcon="cil-search"
                                    {...search}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="card-sub-head">
                        <ToggleButton name="Показывать выполненные"
                                      onClick={() => {
                                          listConfig.filters.completed = +!listConfig.filters.completed;
                                          fetchDatatable();
                                      }}
                                      selected={listConfig.filters.completed}
                        />
                        {isTutor() && !now.isAfter(datelimit) &&  <Button className="button-style but-tusck za235 but-client but"
                                                                          name="+ &nbsp; Добавить клиента"
                                                                          onClick={() => addNewClient()}
                        />
                        }
                        {isTutor() && !now.isAfter(datelimit) && <Button className="button-style  za235 but-tusck"
                                                                         name="+ &nbsp; Добавить задачу"
                                                                         onClick={addNewTask}/>
                        }


                    </div>
                </div>
                <DataTable config={listConfig}
                           fetch={fetchDatatable}
                           data={Task.list}
                           total={Task.recordsTotal}
                />
            </div>
        </React.Fragment>
    )
});
