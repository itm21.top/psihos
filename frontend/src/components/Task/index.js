import React, {useEffect} from 'react';
import TasksList from './List'
import TaskForm from './Form'
import Task from "./models/Task";
import {observer} from "mobx-react";
import Card from "../../common/card/Card";
import './task.scss';
import '../../common/dataTable/datatable.scss';
import fetchDatatable from "./actions/fetchDatatable";
import fetchAvaliableClients from "../Calendar/actions/fetchAvaliableClients";
import Client from "../Clients/models/Client";
import Form from "../Clients/Form";
import Feetback from "../Feetback/models/Feetback";
import Form2 from "../Feetback/Form";

export default observer(() => {

    useEffect(() => {
        fetchAvaliableClients();
    }, [])
    return (
        <Card className="client-main-page">
            <TasksList/>
            {Task.isShowForm && <TaskForm saveCallback={() => fetchDatatable()} clientExists={false}/>}

            {Client.isShowForm && <Form saveCallback={() => fetchDatatable()} clientExist={false}/>}
            {Feetback.isShowForm && <Form2 saveCallback={() => fetchDatatable()} clientExist={false}/>}

        </Card>
    );
});
