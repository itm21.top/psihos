import TasksDateRanges from "./TasksDateRanges";

export default Object.keys(TasksDateRanges).map(key => {
    return {
        label: TasksDateRanges[key],
        value: key
    }
})

