import React from 'react';
import {Table, THead, THeadCell, TCell, TBody, TRow} from '../../common/tables'
export default () => {
    return (
        <Table>
            <THead>
                <THeadCell first={true}>Country</THeadCell>
                <THeadCell>Desription</THeadCell>
                <THeadCell>Time</THeadCell>
                <THeadCell>Price</THeadCell>
            </THead>
            <TBody>
                <TRow>
                    <TCell>United Kingdom</TCell>
                    <TCell>
                        Stonehenge, Windsor and Bath with Pub Lunch
                    </TCell>
                    <TCell>19 Sep, 1p.m.</TCell>
                    <TCell>US$500</TCell>
                </TRow>
                <TRow>
                    <TCell>United Kingdom</TCell>
                    <TCell>
                        Stonehenge, Windsor and Bath with Pub Lunch
                    </TCell>
                    <TCell>19 Sep, 1p.m.</TCell>
                    <TCell>US$500</TCell>
                </TRow>
                <TRow>
                    <TCell>United Kingdom</TCell>
                    <TCell>
                        Stonehenge, Windsor and Bath with Pub Lunch
                    </TCell>
                    <TCell>19 Sep, 1p.m.</TCell>
                    <TCell>US$500</TCell>
                </TRow>
            </TBody>
        </Table>
    )
}
