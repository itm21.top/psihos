import api from "../../../common/api";
import fetchClient from "../../Clients/actions/fetchClient";
import Client from "../../Clients/models/Client";
import Meeting from "../models/Meeting";

export default async (meetingId, fileId) => {
    await api.delete(`/meeting/${meetingId}/file/${fileId}`);
    await fetchClient(Client.current.id);
    Meeting.current.files = Meeting.current.files.filter(file => file.id !== fileId);

}