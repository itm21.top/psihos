import Meeting from '../models/Meeting';

export default (field, value) => {
    Meeting.current[field] = value;
}