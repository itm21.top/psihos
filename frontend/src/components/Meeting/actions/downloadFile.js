import api from "../../../common/api";
import FileDownload from 'js-file-download';

export default async (meetingId, fileId) => {
    const response = await api.get(`/meeting/${meetingId}/file/${fileId}`);
    const fileName = response.headers['filename'];
    FileDownload(response.data, fileName);
}