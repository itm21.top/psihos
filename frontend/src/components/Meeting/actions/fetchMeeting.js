import api from "../../../common/api";
import mapMeeting from "../../Meeting/models/selectors/mapMeeting";
import Meeting from "../models/Meeting";

export default async (id) => {
    const response = await api.get('/meeting/' + id);
    Meeting.current = mapMeeting(response.data);
}