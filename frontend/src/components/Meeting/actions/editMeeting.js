import Meeting from '../models/Meeting';
import toggleMeetingAddForm from "./toggleMeetingAddForm";
import moment from "moment";
import api from "../../../common/api";
import mapMeeting from "../models/selectors/mapMeeting";

export default async (id) => {
    const response = await api.get('/meeting/' + id);
    const meeting = mapMeeting(response.data);
    const scheduledDate = moment(meeting.scheduled_for);
    Meeting.current = {
        ...meeting,
        time: scheduledDate.format('HH:mm'),
        date: scheduledDate.toDate(),
    };
    toggleMeetingAddForm();
}