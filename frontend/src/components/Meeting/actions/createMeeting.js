import Meeting from '../models/Meeting';
import toggleMeetingAddForm from "./toggleMeetingAddForm";

export default () => {
    Meeting.current = {
        type: "offline",
        time: '08:00',
        timelast: '09:00',
        name: '',
        namelast: '',
        duration: '01:00',
        editDate: new Date(),
        files: [],
        uploadFiles: [],
        is_visited: 1,
        notify_by_email: 1,
        paymeet:0,
        freemeet:0,
    };
    toggleMeetingAddForm();
}