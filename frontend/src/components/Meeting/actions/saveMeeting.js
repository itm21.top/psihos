import api from "../../../common/api";
import Meeting from "../models/Meeting";
import moment from "moment";
import mapMeeting from "../models/selectors/mapMeeting";

export default async ({callback, clientId}) => {
    const makeDatetime = () => {
        return moment(Meeting.current.editDate).format('YYYY-MM-DD')
            + ' ' + Meeting.current.time;
    }
    const makeDatetime2 = () => {
        return moment(Meeting.current.editDate).format('YYYY-MM-DD')
            + ' ' + Meeting.current.timelast;
    }

    const durationToInt = (duration) => {
        const splits0 = Meeting.current.time.split(':');
        const splits01 = (splits0[0] * 60) + (splits0[1]*1);
        const splits1 = Meeting.current.timelast.split(':');
        const splits11 = (splits1[0] * 60) + (splits1[1]*1);
        const splits = splits11 - splits01;
        console.log(splits0);
        console.log(' - ');
        console.log(splits1);

        return (splits);
    }

    console.log(Meeting.current.name);

    const formData = new FormData();
    formData.append('client_id', clientId);
    formData.append('type', Meeting.current.type);
    if(Meeting.current.name!='null')
    {
        formData.append('name', Meeting.current.name);
    }
    else {
        formData.append('name', ' ');
    }

    if(Meeting.current.namelast!='null')
    {
        formData.append('namelast', Meeting.current.namelast);
    }
    else {
        formData.append('namelast', ' ');
    }


    formData.append('duration', durationToInt(Meeting.current.duration));
    formData.append('is_visited', Meeting.current.is_visited);
    formData.append('is_fixed', Meeting.current.is_fixed);
    formData.append('notify_by_email', Meeting.current.notify_by_email ? 1 : 0);
    formData.append('paymeet', Meeting.current.paymeet ? 1 : 0);
    formData.append('freemeet', Meeting.current.freemeet ? 1 : 0);
    formData.append('scheduled_for', makeDatetime());
    formData.append('scheduled_last', makeDatetime2());
    if (Meeting.current.uploadFiles.length > 0) {
        Meeting.current.uploadFiles.map((file, index) => formData.append(`files[${index}]`, file));
    }

    if (Meeting.current.id) {
        formData.append('_method', 'PUT');
        const response = await api.post(
            '/meeting/' + Meeting.current.id,
            formData,
            {headers: {'Content-Type': 'multipart/form-data'}}
        );
        Meeting.current = mapMeeting(response.data);
    } else {
        const response = await api.post(
            '/meeting',
            formData,
            {headers: {'Content-Type': 'multipart/form-data'}}
        );
    }
    await callback();
}