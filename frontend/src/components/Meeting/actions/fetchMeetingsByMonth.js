import api from "../../../common/api";
import qs from "qs";
import Meeting from "../models/Meeting";
import mapMeeting from "../models/selectors/mapMeeting";
import {toJS} from "mobx";

export default async (month) => {
    const config = {month};
    const response = await api.get('/calendar', {
        params: config, paramsSerializer: config => {
            return qs.stringify(config)
        }
    });
    Meeting.list = response.data.map(mapMeeting);
    const initAcc = {};
    Meeting.listByDays = Meeting.list.reduce((acc, meeting) => {
        let meetingDay = meeting.meetingDate.format('YYYYMMDD');
        if (!acc[meetingDay]) {
            acc[meetingDay] = [];
        }
        acc[meetingDay].push(toJS(meeting));
        return acc;
    }, initAcc);

}