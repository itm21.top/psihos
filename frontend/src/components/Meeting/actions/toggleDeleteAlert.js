import Meeting from "../models/Meeting";
export default () => {
   Meeting.isShowDeleteAlert = !Meeting.isShowDeleteAlert;
}