import api from "../../../common/api";
import Meeting from "../models/Meeting";
import CalendarType from "../../Calendar/constants/CalendarType";
import fetchMeetingsByPeriod from "../../Calendar/actions/fetchMeetingsByPeriod";
import fetchDatatable from "../../Calendar/actions/fetchDatatable";
import Calendar from "../../Calendar/model/Calendar";

export default async () => {
    await api.delete(`/meeting/${Meeting.current.id}`);

    const selectedDate = Calendar.selectedDate;
    const activeType = Calendar.activeType;
    const step = activeType === CalendarType.WEEK ? 'week' : 'month';
    await fetchMeetingsByPeriod(selectedDate, step);
    await fetchDatatable();

}