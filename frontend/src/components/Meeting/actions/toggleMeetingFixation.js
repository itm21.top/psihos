import api from "../../../common/api";
import fetchClient from "../../Clients/actions/fetchClient";
import Client from "../../Clients/models/Client";

export default async (meeting) => {

    const durationToInt = (duration) => {
        const splits = duration.split(':');
        return (splits[0] * 60) + (splits[1] * 1);
    }

    const response = await api.put(
        '/meeting/' + meeting.id,
        {...meeting, duration: durationToInt(meeting.duration), is_fixed: meeting.is_fixed ? 0 : 1}
    );
    await fetchClient(Client.current.id);
}