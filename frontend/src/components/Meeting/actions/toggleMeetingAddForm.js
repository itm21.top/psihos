import Meeting from "../models/Meeting";
export default () => {
   Meeting.isShowForm = !Meeting.isShowForm;
}