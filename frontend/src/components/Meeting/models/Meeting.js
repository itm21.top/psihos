import {observable} from "mobx";

const Meeting = observable({
    current: {
        hours: '',
        minutes: '',
        date: '',
        type: 'offline',
        client_id: '',
        tutor_id: '',
        files: [],
        uploadFiles: [],
        is_visited: 1,
        notify_by_email: 0,
        otify_by_email2: 0,
        namelast: '',
        paymeet: 0,
        freemeet: 0,
    },
    list: [],
    recordsTotal: 0,
    clients: [],
    listByDays: [],
    isShowForm: false,
    isShowDeleteAlert: false,
    filters: {
        paymeet: 1
    }
});

export default Meeting;

