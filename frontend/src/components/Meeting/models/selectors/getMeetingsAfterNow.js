import moment from "moment";

export default (meetings) => {
    return meetings.filter(meeting => moment().valueOf() <= meeting.meetingDate.valueOf())
}