import moment from "moment";

const durationToTime = (duration) => {
    const hours = duration > 60 ? Math.floor(duration / 60) : '00';
    const minutes = duration > 60 ? duration - (hours * 60) : duration;
    return hours.toString().padStart(2, '0') + ':' + minutes.toString().padStart(2, '0');
}

export default (meeting) => {
    const meetingDate = moment(meeting.scheduled_for);
    const meetingDate2 = moment(meeting.scheduled_last);
    const date = meetingDate.format('DD.MM.YYYY');
    return {
        ...meeting,
        duration: durationToTime(meeting.duration),
        durationMeetingInMinutes: meeting.duration,
        meetingDate,
        editDate: meetingDate.toDate(),
        date,
        time: moment(meetingDate).format('HH:mm'),
        timelast: moment(meetingDate2).format('HH:mm'),
        uploadFiles: []
    };
}