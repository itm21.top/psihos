import moment from "moment";

export default (meetings) => {
    return meetings ? meetings.filter(meeting => moment().valueOf() > meeting.meetingDate.valueOf()) : [];
}