const mapType = {
    online: 'Онлайн',
    offline: 'Очно',
}

export default (meetingType) => {
    return mapType[meetingType] ?? 'Онлайн';
}