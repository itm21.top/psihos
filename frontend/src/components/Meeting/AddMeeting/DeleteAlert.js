import React from "react";
import {observer} from "mobx-react";
import SubmitButton from "../../../common/forms/components/SubmitButton";
import MiniPopup from "../../../common/miniPopup/MiniPopup";
import toggleDeleteAlert from "../actions/toggleDeleteAlert";
import Button from "../../../common/forms/components/Button";
import asyncHandler from "../../../common/forms/hooks/asyncHandler";
import deleteMeeting from "../actions/deleteMeeting";
import toggleMeetingAddForm from "../actions/toggleMeetingAddForm";

export default observer(() => {
    const [submit, submitting, submitFailed] = asyncHandler(async () => {
        await deleteMeeting();
        toggleDeleteAlert();
        toggleMeetingAddForm();
    }, (requestError) => {
    });

    return (
        <MiniPopup onClose={toggleDeleteAlert}
                   className="client-delete"
        >
            <p className="h5">Удалить встречу?</p>
            <p className="sub-h5">Встреча будет удалена со всеми файлами</p>
            <div className="d-flex">
                <Button
                    className="button-style-gray"
                    name="Отменить"
                    onClick={toggleDeleteAlert}
                >
                </Button>
                <SubmitButton
                    className="button-style"
                    name="Удалить"
                    onClick={submit}
                    submitting={submitting}
                />
            </div>
        </MiniPopup>
    )
});