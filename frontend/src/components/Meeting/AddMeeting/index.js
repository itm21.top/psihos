import React, {useRef, useState} from 'react';
import {observer} from "mobx-react";
import Meeting from "../models/Meeting";
import Client from "../../Clients/models/Client";
import useFormInput from "../../../common/forms/hooks/useFormInput";
import asyncHandler from "../../../common/forms/hooks/asyncHandler";
import DateInput from "../../../common/forms/components/DateInput";
import SelectInput from "../../../common/forms/components/SelectInput";
import changeCurrentMeeting from "../actions/changeCurrentMeeting";
import saveMeeting from "../actions/saveMeeting";
import toggleMeetingAddForm from "../actions/toggleMeetingAddForm";
import {FilePond} from "react-filepond";
import "filepond/dist/filepond.min.css";
import "./addMeeting.scss"
import SubmitButton from "../../../common/forms/components/SubmitButton";
import Popup from "../../../common/popup/Popup";
import Checkbox from "../../../common/forms/components/Checkbox";
import TextLink from "../../../common/forms/components/TextLink";
import deleteFile from "../actions/deleteFile";
import downloadFile from "../actions/downloadFile";
import toggleDeleteAlert from "../actions/toggleDeleteAlert";
import DeleteAlert from "./DeleteAlert";
import TimeInput from "../../../common/forms/components/TimeInput";
import moment from "moment";
import {required} from "../../../common/forms/validators";
import {useHistory} from 'react-router-dom';
import closeMeeting from "../actions/closeMeeting";
import TextArea from "../../../common/forms/components/TextArea";
import 'react-fancybox/lib/fancybox.css';
import ReactFancyBox from "react-fancybox";
import FancyVideo from "react-videojs-fancybox";
import isTutor from "../../../common/auht/selectors/isTutor";
import {toJS} from "mobx";
import Task from "../../Task/models/Task";



export default observer(({saveCallback, clientExists = true}) => {
    const pond = useRef(null);
    const [files, setFiles] = useState([]);
    const [dateError, setDateError] = useState(null);
    const history = useHistory();
    const [errcount, setCount] = useState(0);

    const redirectToClientProfile = () => {
        //history.push('/client/' + Meeting.current.client_id);
        window.location.href = '/client/' + Meeting.current.client_id;
        closeMeeting();
    }

    const meetingTypes = [
        {
            label: 'Очно',
            value: 'offline',

        },
        {
            label: 'Онлайн',
            value: 'online',

        },
    ];

    const isDateError = () => {
        const dateTime = moment(Meeting.current.editDate).format('YYYY-MM-DD') + ' ' + Meeting.current.time;
        if (!Meeting.current.id && !moment(dateTime).isAfter(moment()) && errcount==0) {
            setDateError('Вы согласны создать встречу в прошлом?');
            return true;
        }
        const meetings = toJS(Meeting.list)?? [];
        console.log(meetings);
        meetings.map(id=>{
            console.log(this);
            }
        )

        return false;
    }
    var z = '';
    var z2 = '';
    var z3 = '';
    var z4 = '0';
    const onChangeDate2 = (field, value) => {
        setDateError(null);
        var z = value.substr(0, 2);
        console.log(z);
        var z2 = z*1 + 1;
        console.log(z2);
        var z3 = z2*1 + value.substr(2, 5);
        if((z*1)<10)
        {
            z3 = z4 + z3;
        }
        console.log(z3);
        changeCurrentMeeting(field, value);
        changeCurrentMeeting('timelast', z3);

        const meetings = toJS(Meeting.list)?? [];
        const dateTime = moment(Meeting.current.editDate).format('YYYY-MM-DD')+ ' ' + Meeting.current.time;
        console.log(meetings);
        console.log(Meeting.current.time);

        meetings.filter(option => (getParsedDate(option.date, option.time, option.timelast))==true ).map(opt =>
                (
                    za(moment(getParsedDate(opt.date)).format('YYYY-MM-DD')))
            //console.log(moment(getParsedDate(opt.date)+ ' ' + opt.time).format('YYYY-MM-DD h')))
            //setDateError('Вы согласны создать встречу в прошлом?');
            //return true;
        )

    }

    function periodOverlaps(testperiods, periods)
    {

        for (var i = 0; i<periods.length; i++)
        {
            var period = periods[i];

            if(period.start<=testperiods[0].start&&period.end>=testperiods[0].start)
            {
                console.log('1');
                za();
                return true;
            }

            if(period.start<testperiods[0].end&&period.end>=testperiods[0].end)
            {
                console.log('2');
                za();
                return true;
            }


        }
        console.log('3');
        return false;
    }

    function getParsedDate(date, timest, timelast){
        var Splitdate = String(date).split('.');
        var dd = Splitdate[0];
        var mm = Splitdate[1];
        var yyyy = Splitdate[2];
        var date1 = yyyy + '-' + mm + '-' + dd + ' ' + timest;
        var date2 = yyyy + '-' + mm + '-' + dd + ' ' + timelast;

        const dateTime1 = moment(Meeting.current.editDate).format('YYYY-MM-DD')+ ' ' + Meeting.current.time;
        const dateTime2 = moment(Meeting.current.editDate).format('YYYY-MM-DD')+ ' ' + Meeting.current.timelast;


        console.log(new Date(dateTime1));
        var periods = [{
            start: new Date(date1),
            end: new Date(date2)
        }]
        var testperiods = [{
            start: new Date(dateTime1),
            end: new Date(dateTime2)
        }]
        periodOverlaps(testperiods, periods);
    }





     function za(a){
         setDateError('Это время уже занято, записать на это время?');
     }

    const onChangeDate = (field, value) => {
        setDateError(null);
        changeCurrentMeeting(field, value);
        //console.log(Meeting.list);
        const meetings = toJS(Meeting.list)?? [];
        const dateTime = moment(Meeting.current.editDate).format('YYYY-MM-DD')+ ' ' + Meeting.current.time;
       // console.log(meetings);
        //console.log(Meeting.current.time);

        meetings.filter(option => (getParsedDate(option.date, option.time, option.timelast))==true ).map(opt =>
            (
                za(moment(getParsedDate(opt.date)).format('YYYY-MM-DD')))
                //console.log(moment(getParsedDate(opt.date)+ ' ' + opt.time).format('YYYY-MM-DD h')))
                //setDateError('Вы согласны создать встречу в прошлом?');
                //return true;
            )

    }





   

    

    const onAddedFile = async () => {
        if (Meeting.current.id) {
            await submit();
        }
    }
    const onUpdateFiles = async (fileItems) => {
        changeCurrentMeeting('uploadFiles', fileItems.map(fileItem => fileItem.file))
        if (!Meeting.current.id) {
            setFiles(fileItems);
        }
    }




    const submitMeeting = async () => {
        if (!isDateError()) {
            await submit();
            Meeting.isShowForm = false;
        }
        setCount(errcount + 1);
    }

    const name = useFormInput(
        [
            /*required('Опиание встречи обязательное поле'),*/
        ],
        () => Meeting.current.name,
        (value) => onChangeDate('name', value)
    );

    const namelast = useFormInput(
        [
            /*required('Опиание встречи обязательное поле'),*/
        ],
        () => Meeting.current.namelast,
        (value) => onChangeDate('namelast', value)
    );

    const time = useFormInput(
        [],
        () => Meeting.current.time,
        (value) => onChangeDate2('time', value)
    );

    const timelast = useFormInput(
        [],
        () => Meeting.current.timelast,
        (value) => onChangeDate('timelast', value)
    );

    const duration = useFormInput(
        [],
        () => Meeting.current.duration,
        (value) => onChangeDate('duration', value)
    );

    const type = useFormInput(
        [],
        () => meetingTypes.filter(option => option.value === Meeting.current.type),
        (value) => {
            changeCurrentMeeting('type', value.value)
        }
    );

    const clientOptions = Client.avaliableList.reduce((acc, client) => {
        if ((client.deleted_at === null
            && client.is_archive === 0)
            || Meeting.current.client_id === client.id
        ) {
            acc.push({
                label: client.full_name,
                value: client.id
            });
        }
        return acc;
    }, []);

    const client = useFormInput(
        [
            required('Вы должны выбрать пользователя с которым пройдет встреча')
        ],
        () => clientOptions.filter(option => option.value === Meeting.current.client_id),
        (value) => {
            changeCurrentMeeting('client_id', value.value)
        }
    );

    const date = useFormInput(
        [],
        () => Meeting.current.editDate,
        (value) => onChangeDate('editDate', value)
    );

    const [submit, submitting, submitFailed] = asyncHandler(async () => {
        const clientId = clientExists ? Client.current.id : Meeting.current.client_id;
        await saveMeeting({
            callback: saveCallback,
            clientId
        });
        location.href = window.location;
    }, (requestError) => {
        console.log(requestError);
    });

    const me = (a, b, c) =>{

        let myArr = b.indexOf('png');
        let myArr2 = b.indexOf('jpg');
        let myArr3 = b.indexOf('gif');
        let myArr4 = b.indexOf('svg');
        let myArr5 = b.indexOf('ttf');
        let myArr6 = b.indexOf('woff');
        let myArr16 = b.indexOf('jpeg');

        let myArr7 = b.indexOf('mp4');
        let myArr8 = b.indexOf('avi');
        let myArr9 = b.indexOf('mov');

        let myArr10 = b.indexOf('MP4');
        let myArr11 = b.indexOf('AVI');
        let myArr12 = b.indexOf('MOV');

        if(myArr>0||myArr2>0||myArr3>0||myArr4>0||myArr5>0||myArr6>0||myArr16>0)
        {
            return  <ReactFancyBox thumbnail={"/public/meeting/"+a+"/"+b} image={"/public/meeting/"+a+"/"+b}/>
        }
        else if(myArr7>0||myArr8>0||myArr9>0||myArr10>0||myArr11>0||myArr12>0)
        {
            return <video width="250" className="videocod"  autoplay loop controls playsinline >
            <source src={"/public/meeting/"+a+"/"+b} type="video/mp4"/>
            <source src={"/public/meeting/"+a+"/"+b} type="video/ogv"/>
            <source type="video/webm" src={"/public/meeting/"+a+"/"+b}/>
        </video>
        }
        else
        {
            return <div></div>
        }

    }



    const submitRules = [name];
    if (!clientExists) {
        submitRules.push(client);
    }
    const submitButtonActive = submitRules.every(field => field.isValid);


    const meetingStickerClass = (Meeting.current.type === 'online' ? 'online' : 'offline');

    const [isFancybox, setIsFancybox] = useState("");

    const [isOpen, setOpen] = useState("");



    return (
        <Popup header={Meeting.current.id ? "Редактирование встречи" : "Новая встреча"}
               onClose={toggleMeetingAddForm}
               className="meeting-add"
        >


            {(!clientExists && isTutor()) && !clientExists &&

                <SelectInput options={clientOptions}
                             label="Клиент *"
                             placeholder={'Выбрать...'}
                             {...client}
                />

            }

            <div className="row justify-content-between align-items-center pb-3">
                <div className="col-auto">
                    {Meeting.current.id ?
                        <button className="btn remove-style"
                                type="button"
                                onClick={toggleDeleteAlert}
                        >
                            <i className="cil-trash"></i>Удалить встречу
                        </button>
                        : ''
                    }
                    {Meeting.isShowDeleteAlert && <DeleteAlert/>}
                </div>
                <div className="col-auto">
                    {(!clientExists && Meeting.current.id) &&
                    <span onClick={redirectToClientProfile} className="text-link client-card-link">
                    Перейти в карточку
                </span>
                
                    }
                </div>
            </div>


            <div className="mb-4">
                <TextArea
                    disabled={submitting}
                    label="Описание встречи"
                    {...name}
                />
            </div>

            <div className="mb-4">
                <TextArea

                    label="Пометка на следующую встречу"
                    {...namelast}
                />
            </div>
            <div className="input-group-wrapper-outer">
                <DateInput
                    disabled={submitting}
                    className="input-date-wrapper"
                    label=""
                    groupIconAppend="cil-calendar"
                    {...date}
                />
                <TimeInput
                    disabled={submitting}
                    className="input-time-wrapper"
                    label=""
                    {...time}
                />
                <TimeInput
                    disabled={submitting}
                    id="timeli"
                    className="input-time-wrapper timeli"
                    label=""
                    {...timelast}
                />



            </div>
            {dateError &&
            <div className="alert alert-danger" role="alert">
                {dateError}
            </div>
            }
            <div>
                <div className="input-select-wrapper">
                    <SelectInput options={meetingTypes}
                                 label="Тип *"
                                 className={meetingStickerClass}
                                 placeholder={'Выбрать...'}
                                 {...type}/>
                </div>
            </div>

            <h5>Файлы</h5>

            {(Meeting.current.files && Meeting.current.files.length > 0) &&
            <React.Fragment>
                <ul className="files-list">
                    {Meeting.current.files && Meeting.current.files.map((file, key) =>

                        <li key={key}>
                            <TextLink onClick={() => downloadFile(Meeting.current.id, file.id)}>
                                {file.name}

                            </TextLink>


                            {me(Meeting.current.id,file.file_name,file.id)}


                            <span className="zad"></span>
                            <span className="vail"></span>
                            <span className="delete"
                                  onClick={() => deleteFile(Meeting.current.id, file.id)}
                            ></span>
                        </li>
                    )}
                </ul>
            </React.Fragment>
            }

            <FilePond
                ref={pond}
                files={files}
                allowMultiple={true}
                maxFiles={3}
                labelIdle="<span class='text-link'>Выберите файл</span> или перетяните сюда"
                onaddfile={onAddedFile}
                onupdatefiles={onUpdateFiles}
            />



            <div className="row submit-block">
                <div className="col-sm-auto pb-sm-0 col">
                    <Checkbox name="Уведомить клиента по Email"
                              checked={Meeting.current.notify_by_email2}
                              onClick={() => Meeting.current.notify_by_email2 = !Meeting.current.notify_by_email2}
                    />
                   
                    <Checkbox name="Встреча оплачена"
                              checked={Meeting.current.paymeet}
                              onClick={() => Meeting.current.paymeet = !Meeting.current.paymeet}

                    />
                 
                    <Checkbox name="Уведомить клиента по SMS"
                              onClick={() => null}
                              disabled
                    />
                </div>
                <div className="col-sm-auto">
                    <SubmitButton
                        name={Meeting.current.id ? "Oбновить встречу" : "Создать встречу"}
                        onClick={submitMeeting}
                        submitting={submitting}
                        disabled={!submitButtonActive}
                    />
                </div>
            </div>


            {/*(Client.current.meetings && Client.current.meetings.length > 0) &&
            <React.Fragment>
                <div className="row justify-content-between align-items-center pb-3">
                    <div className="col-auto">
                        <h5> Прошедшие встречи</h5>
                    </div>

                    <div className="col-auto">
                        <ToggleButton name="Встреча не посещена"
                                      selected={!Meeting.current.is_visited}
                                      onClick={() => changeCurrentMeeting('is_visited', +!Meeting.current.is_visited)}
                        />
                    </div>
                </div>
                <div className="row row-meeting">
                    {Client.current.meetings && getMeetingsTillNow(Client.current.meetings).map((meeting, key) =>
                        <div className="col-12" key={key}>
                            <div className="row">
                                <div className="col-auto">
                                    <span className={`client-card-time ${meeting.type == 'online' ? 'active' : ''}`}>
                                        {meeting.date}
                                    </span>
                                </div>
                                <div className="col-auto">
                                    {meeting.time}
                                </div>
                                <div className="col-auto">
                                    {mapMeetingType(meeting.type)}
                                </div>
                            </div>
                        </div>
                    )}
                </div>


            </React.Fragment>
            */}

        </Popup>
    )
});
