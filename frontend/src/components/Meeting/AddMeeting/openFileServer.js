import ReactFancyBox from "react-fancybox/lib/ReactFancyBox";
import 'react-fancybox/lib/fancybox.css';
import React from "react";
import api from "../../../common/api";
import FileDownload from "js-file-download";
import Meeting from "../models/Meeting";



export default  (meetingId, fileId) => {
    //const response =  api.get(`/meeting/${meetingId}/file/${fileId}`);

    return <ReactFancyBox thumbnail={"api/meeting/"+meetingId+"/file/"+fileId} image={"api/meeting/"+meetingId+"/file/"+fileId}/>
   // return response;
}