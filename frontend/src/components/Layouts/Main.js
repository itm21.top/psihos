import React, {useEffect} from 'react';
import Sidebar from "./components/Sidebar";
import Header from "./components/Header";
import Router from "../../common/router";
import componentsRoutes from "../componentsRouter";
import initUser from "../../common/auht/actions/initUser";
import User from "../../common/models/User";
import {observer} from "mobx-react";

export default observer(() => {
    useEffect(() => {
        initUser();
    }, []);

    if (!User.current.id) {
        return null;
    }

    return (
        <React.Fragment>
            <Sidebar />
            <div className="c-wrapper c-fixed-components">
                <Header />
                <div className="c-body">
                    <main className="c-main">
                        <div className="container-fluid">
                            <div className="fade-in">
                                <Router config={componentsRoutes}/>
                            </div>
                        </div>
                    </main>
                </div>
            </div>
        </React.Fragment>
    );
})