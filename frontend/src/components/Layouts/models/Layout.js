import {observable} from "mobx";

const Layout = observable({
    sidebar: {
        isOpen: true,
        toggleClass: 'c-sidebar-lg-show',
    },
    auth: {
        mode: 'mentors'
    }
});

export default Layout;

