import React from 'react';
import ToggleSidebar from "../Header/ToggleSidebar";
import Layout from "../../models/Layout";
import {observer} from "mobx-react";


export default observer(() => {

    const logoWrapperClass = 'logo-wrapper d-none '
        + (Layout.sidebar.isOpen && 'logo-wrapper d-flex')

    return (
        <div className="c-sidebar-brand">
            <div className={logoWrapperClass}>
                <div className="logo-bg"></div>
                <div className="logo-name">life-story</div>
            </div>
            <span className="">
              <ToggleSidebar/>
            </span>
        </div>
    )
});

