import React from 'react';
import {useHistory, useLocation} from "react-router-dom";
import toggleSidebar from "../../actions/toggleSidebar";
import {useWindowSize} from "@react-hook/window-size";

export default ({title, url, icon}) => {
    const history = useHistory();
    const location = useLocation();
    const [width] = useWindowSize();
    const onClick = (url) => {
        history.push(url);
        if (width < 960) {
            toggleSidebar(false);
        }
    }

    return (
        <li className="c-sidebar-nav-item">
            <a className={'c-sidebar-nav-link' + ((location.pathname === url) ? ' c-active' : '')}
               onClick={() => onClick(url)}>
                <span className="c-sidebar-nav-icon">
                    <i className={'cil-' + icon}/>
                </span>
                {title}
            </a>
        </li>
    )
}