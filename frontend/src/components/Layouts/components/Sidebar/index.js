import React, {useEffect} from 'react';
import {useWindowSize} from '@react-hook/window-size'
import './Sidebar.scss';
import Layout from "../../models/Layout";
import {observer} from "mobx-react";
import tutorConfig from "./tutorConfig";
import userConfig from "./userConfig";
import NavItem from "./NavItem";
import Brand from "./Brand";
import {useHistory} from "react-router-dom";
import logout from "../../../../common/auht/actions/logout";
import UserRoles from "../../../../common/auht/constants/UserRoles";
import User from "../../../../common/models/User";
import addfeetback from "../../../Feetback/action/addfeetback";

export default observer(() => {
    const [width] = useWindowSize();
    const sidebarClass = 'c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show '
        + (Layout.sidebar.isOpen && (width > 960 ? '' : 'c-sidebar-show'))
        + ' ' +  (!Layout.sidebar.isOpen && (width > 960 ? 'c-sidebar-minimized' : ''));

    useEffect(() => {
        if (width < 960) {
            Layout.sidebar.isOpen = false;
        }
    }, []);

    const history = useHistory();
    const userLogout = async () => {
        await logout();
        history.push('/login');
    }
    const config = User.current.id ? User.current.role === UserRoles.TUTOR ? tutorConfig : userConfig : [];
    return (
        <React.Fragment>
            <div className={sidebarClass}>
                <Brand />
                <ul className="c-sidebar-nav">
                    {config.map((item, key) =>
                        <NavItem key={key} {...item} />
                    )}
                </ul>
                <div className="to-exit to2" onClick={() => addfeetback()}>
                    <div className="to-exit-icon-wrapper">
                        <i className="cil-chat-bubble"></i>
                    </div>
                    <span>Обратная связь</span>
                </div>
                <div className="to-exit" onClick={userLogout}>
                    <div className="to-exit-icon-wrapper">
                        <i className="cil-account-logout"></i>
                    </div>
                    <span>Выход</span>
                </div>
            </div>
        </React.Fragment>
    )
});