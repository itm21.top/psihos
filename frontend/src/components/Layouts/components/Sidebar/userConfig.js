export default [
    {
        title: 'Календарь',
        url: '/dashboard',
        icon: 'calendar'
    },
    {
        title: 'Моя карточка',
        url: '/my-card',
        icon: 'people'
    },
    // {
    //     title: 'Мои группы',
    //     url: '/my-groups',
    //     icon: 'contact'
    // },
    {
        title: 'Мои задачи',
        url: '/tasks',
        icon: 'list'
    },
    {
        title: 'Настройки',
        url: '/profile/me',
        icon: 'settings'
    },
];

