export default [
    {
        title: 'Календарь',
        url: '/dashboard',
        icon: 'calendar'
    },
    // {
    //     title: 'Группы',
    //     url: '/groups',
    //     icon: 'people'
    // },
    {
        title: 'Клиенты',
        url: '/clients',
        icon: 'contact'
    },
    {
        title: 'Задачи',
        url: '/tasks',
        icon: 'list'
    },
    {
        title: 'Настройки',
        url: '/profile/me',
        icon: 'settings'
    },
];

