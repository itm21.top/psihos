import api from "../../../../../common/api";

export default async (file) => {
    const formData = new FormData();
    formData.append('avatar', file);

    const response = await api.post(
        '/me/avatar',
        formData,
        {headers: {'Content-Type': 'multipart/form-data'}}
    );
}