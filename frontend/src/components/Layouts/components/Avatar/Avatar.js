import React from "react";
import './avatar.scss';
import LetteredAvatar from 'react-lettered-avatar';
import updateAvatar from "./actions/updateAvatar";
import initUser from "../../../../common/auht/actions/initUser";

export default ({
    avatar = null,
    name = "Temp Name",
    editable = false,
    role = null,
    onChange = null
}) => {
    const onChangeAvatar = async (e) => {
        await updateAvatar(e.target.files[0]);
        await initUser();
    };

    const onChangeAvatar2 = async (e) => {
        await updateAvatar();
        await initUser();
    };

    return (
        <div className="c-avatar">
            <div className="avatar-container">
                {!!avatar
                    ? <div className="c-avatar-img-wrapper">
                        <img className="c-avatar-img"
                             src={avatar}
                             alt="user@email.com"/>
                      </div>
                    : <LetteredAvatar name={name}/>
                }
                {editable &&
                    <label htmlFor="avatar-upload" className="avatar-upload">
                        <input id="avatar-upload"
                               accept="image/*"
                               onChange={onChange ?? onChangeAvatar}
                               type="file" name="avatar-upload"
                        />
                    </label>
                }
            </div>
            <div className="name-wrapper">
                <div className="name-amount">{name}</div>
                {role && <div className="name-post">{role}</div>}
            </div>
        </div>
    );
}