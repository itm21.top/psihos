import React from 'react';
import ToggleSidebar from "./ToggleSidebar";
import isAuthorized from "../../../../common/auht/selectors/isAuthorized";
import UserMenu from "./UserMenu";
import Layout from "../../models/Layout";
import Breadcrumbs from "./Breadcrumbs";
import BackNavigation from "./BackNavigation";


export default () => {

    return (
        <header className="c-header c-header-light c-header-fixed c-header-with-subheader">
            <ToggleSidebar className="mfe-auto d-lg-none"/>

            <a className="c-header-brand d-lg-none" href="#">
                <span className="c-icon c-icon-lg">
                    <i className="cil-full"/>
                </span>
            </a>
            <div className="d-none d-lg-block mr-auto">
                <BackNavigation />
            </div>

            {/*<div className="to-chat">*/}
            {/*    <i className="cil-speech"></i>*/}
            {/*</div>*/}
            {/*<div className="d-none d-lg-block mr-auto" style="margin-left:25px; background:#f77575; padding:20px; padding-left:30px; padding-right:50px; position: relative;">
                Время подписки окончено. Пожалуйста продлите вашу подписку в <a href="/profile/me">Личном кабинете</a>
                <span style="width:20px; height: 20px; display:block; position:absolute; right:0; top:0;">
                    <span style="display:block; width:100%; height:2px; background: #525252; transform:rotate(45deg); position:relative; top:10px"></span>
                    <span style="display:block; width:100%; height:2px; background: #525252; transform:rotate(-45deg); position:relative; top:8px"></span>
                </span>
            </div>*/}
             {isAuthorized() && <UserMenu/>}
            {Layout.breadcrumbs && <Breadcrumbs/>}

            <div className="d-lg-none col-12 p-0">
                <BackNavigation />
            </div>
        </header>
    )
}