import React from 'react';

export default () => {
    return (
        <div className="c-subheader px-3">
            <ol className="breadcrumb border-0 m-0">
                <li className="breadcrumb-item">Home</li>
                <li className="breadcrumb-item"><a href="#">Admin</a></li>
                <li className="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
    )
}