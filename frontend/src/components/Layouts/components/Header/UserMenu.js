import React from 'react';
import logout from "../../../../common/auht/actions/logout";
import { useHistory } from 'react-router-dom';
import {observer} from "mobx-react";
import Avatar from "../Avatar/Avatar";
import User from "../../../../common/models/User";
import getCurrentUserRole from "../../../../common/auht/selectors/getCurrentUserRole";
import moment from "moment";

export default observer(() => {
    const history = useHistory();
    const userLogout = async () => {
        await logout();
        history.push('/login');
    }
    const goToProfile = async () => {
        history.push('/profile/me');
    }
    const data_pay = () =>{
        console.log(User.current.date_payment);
        let tempdate = new Date(User.current.date_payment);
        const datelimit = moment(tempdate, 'YYYY-MM-DDTHH:mm:ss.SSSZ');

        const now = moment();
        const datelimit4 = moment().add(14, 'days');
        console.log(datelimit);
        console.log(now);
        console.log(now.isAfter(datelimit));
        if(now.isAfter(datelimit))
        {
            const monthNames = ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября","декабря"];
            let tempdate = new Date(User.current.date_payment);
            let date = tempdate.getDate() + ' ' + monthNames[(tempdate.getMonth())] + ' ' + tempdate.getFullYear();
            return (
                <li className="c-header-nav-item dropdown show">
                    <a className="c-header-nav-link krestik" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" >Х</a>
                    <div className="dropdown-menu dropdown-menu-right pt-0 show erstran" >
                        ремя подписки истекло -  {date} .Чтобы оплатить пройдите в <a href="/profile/me/">Личный кабинет</a>
                    </div>
                </li>
            );
            //return '<li className="c-header-nav-item dropdown show"><li className="c-header-nav-item dropdown show"><a className="c-header-nav-link krestik" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" >Х</a><div className="dropdown-menu dropdown-menu-right pt-0 show erstran" > Время подписки истекло - ' + date + '.' + 'Чтобы оплатить пройдите в <a href="/profile/me/">Личный кабинет</a></div></li>'
            //return 'Время подписки истекло - ' + date + '.' + 'Чтобы оплатить пройдите в <a href="/profile/me/">Личный кабинет</a>';
        }
        else
        {
            if(datelimit4.isAfter(datelimit))
            {
                const monthNames = ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"];
                let tempdate = new Date(User.current.date_payment);
                let date = tempdate.getDate() + ' ' + monthNames[(tempdate.getMonth())] + ' ' + tempdate.getFullYear();
                return (
                    <li className="c-header-nav-item dropdown show">
                        <a className="c-header-nav-link krestik" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" >Х</a>
                        <div className="dropdown-menu dropdown-menu-right pt-0 show erstran" >
                            Дата окончания вашей подписки -  {date} .Чтобы оплатить пройдите в <a href="/profile/me/">Личный кабинет</a>
                        </div>
                    </li>
                );

               // return 'Дата окончания вашей подписки - ' + date + '.'+ 'Чтобы оплатить пройдите в <a href="/profile/me/">Личный кабинет</a><div className="dropdown-menu dropdown-menu-right pt-0 show erstran" > ';
            }
        }


    }
    return (
        <ul className="c-header-nav ml-auto mr-4">

           {data_pay()}

            <li className="c-header-nav-item dropdown2">
                <a className="c-header-nav-link" data-toggle="dropdown2" href="#" role="button" aria-haspopup="true"
                   aria-expanded="false">
                    <Avatar avatar={User.current.avatar}
                            name={User.current.name ?? User.current.email}
                            role={getCurrentUserRole()}
                    />
                </a>
                <div className="dropdown-menu dropdown-menu-right pt-0">
                    <a className="dropdown-item" onClick={goToProfile}>
                        <span className="c-icon mr-2">
                            <i className="cil-user" />
                        </span>
                        Профиль
                    </a>
                    <a className="dropdown-item" onClick={userLogout}>
                        <span className="c-icon mr-2">
                            <i className="cil-account-logout" />
                        </span>
                        Выход
                    </a>
                </div>
            </li>
        </ul>
    )
});