import React from 'react';
import {Route, useHistory} from "react-router-dom";

export default () => {
    const history = useHistory();
    const goToClients = () => {
        history.push('/clients')
    };
    return (
        <React.Fragment>
            <Route path="/client/:id">
                <button className="btn back-style d-inline-block mr-auto"
                        type="button"
                        onClick={goToClients}
                >
                    <i className="cil-arrow-left"></i><span>К списку клиентов</span>
                </button>
            </Route>
            <Route path="/profile/:id/change-password">
                <button className="btn back-style d-inline-block mr-auto"
                        type="button"
                        onClick={() => history.goBack()}
                >
                    <i className="cil-arrow-left"></i><span>Назад</span>
                </button>
            </Route>
        </React.Fragment>
    )
}
