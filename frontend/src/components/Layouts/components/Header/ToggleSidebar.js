import React from 'react';
import toggleSidebar from "../../actions/toggleSidebar";

export default ({className}) => {
    return (
        <button className={'c-header-toggler c-class-toggler ' + className}
                type="button"
                onClick={toggleSidebar}>
            <span className="c-icon c-icon-lg">
                <i className="cil-hamburger-menu" />
            </span>
        </button>
    )
}