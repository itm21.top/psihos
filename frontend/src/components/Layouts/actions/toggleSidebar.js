import Layout from "../models/Layout";

export default () => {
    Layout.sidebar.isOpen = !Layout.sidebar.isOpen;
}