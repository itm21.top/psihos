import api from "../../../common/api";
import Client from "../../Clients/models/Client";

export default async () => {
    const response = await api.get('/meeting/clients');
    Client.avaliableList = response.data;
}