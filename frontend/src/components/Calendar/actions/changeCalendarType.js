import Calendar from "../model/Calendar";
import CalendarType from "../constants/CalendarType";
import fetchMeetingsByPeriod from "./fetchMeetingsByPeriod";

export default async (selectedType) => {
    const newType = Object.keys(CalendarType)
            .find(key => CalendarType[key] === selectedType.value)
                ?? CalendarType.MONTH;

    await fetchMeetingsByPeriod(
        Calendar.selectedDate,
        CalendarType[newType] === CalendarType.WEEK ? 'week' : 'month'
    )
    Calendar.activeType = CalendarType[newType];
}