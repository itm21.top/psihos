import fetchDatatable from "../../../common/dataTable/actions/fetchDatatable";
import mapMeeting from "../../Meeting/models/selectors/mapMeeting";
import listConfig from "../components/List/listConfig";
import Calendar from "../model/Calendar";

export default () => {
    fetchDatatable({
        url: '/calendar/list',
        map: (response) => {
            Calendar.meetingsList = response.data.data.map(mapMeeting);
            Calendar.meetingsListTotal = response.data?.recordsTotal;
        },
        config: listConfig
    })
}