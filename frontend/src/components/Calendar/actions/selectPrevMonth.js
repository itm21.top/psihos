import fetchMeetingsByMonth from "../../Meeting/actions/fetchMeetingsByMonth";
import Calendar from "../model/Calendar";

export default async (month) => {
    month.add('-1', 'month');
    await fetchMeetingsByMonth(month.format('YYYY-MM'));
    Calendar.selectedDate = month.clone();
}