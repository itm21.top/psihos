import Calendar from "../model/Calendar";
import fetchMeetingsByPeriod from "./fetchMeetingsByPeriod";

export default async (selectedDate, value, step) => {
    selectedDate.add(value, step);
    await fetchMeetingsByPeriod(selectedDate, step);
    Calendar.selectedDate = selectedDate.clone();
}