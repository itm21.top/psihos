import Calendar from "../model/Calendar";
import moment from "moment";
import fetchMeetingsByPeriod from "./fetchMeetingsByPeriod";
import CalendarType from "../constants/CalendarType";

export default async () => {
    const now = moment();
    await fetchMeetingsByPeriod(
        now,
        CalendarType[Calendar.activeType] === CalendarType.WEEK ? 'week' : 'month'
    );
    Calendar.selectedDate = now;
}