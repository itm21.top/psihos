import qs from "qs";
import {toJS} from "mobx";
import api from "../../../common/api";
import Meeting from "../../Meeting/models/Meeting";
import mapMeeting from "../../Meeting/models/selectors/mapMeeting";
import listConfig from "../components/List/listConfig";
import fetchDatatable from "./fetchDatatable";

export default async (selectedDate, step) => {

    const config = {
        from: selectedDate.clone().startOf(step).format('YYYY-MM-DD'),
        to: selectedDate.clone().endOf(step).format('YYYY-MM-DD')
    };

    listConfig.filters.to = config.to;
    listConfig.filters.from = config.from;
    fetchDatatable();
    const response = await api.get('/calendar', {
        params: config, paramsSerializer: config => {
            return qs.stringify(config)
        }
    });
    Meeting.list = response.data.map(mapMeeting);
    const initAcc = {};
    Meeting.listByDays = Meeting.list.reduce((acc, meeting) => {
        let meetingDay = meeting.meetingDate.format('YYYYMMDD');
        if (!acc[meetingDay]) {
            acc[meetingDay] = [];
        }
        acc[meetingDay].push(toJS(meeting));
        return acc;
    }, initAcc);

}