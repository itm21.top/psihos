import CalendarType from "./CalendarType";

export default [
    {
        label: CalendarType.MONTH,
        value: CalendarType.MONTH,
    },
    {
        label: CalendarType.WEEK,
        value: CalendarType.WEEK,
    },
    {
        label: CalendarType.LIST,
        value: CalendarType.LIST,
    },
];