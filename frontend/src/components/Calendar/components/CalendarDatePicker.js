import React from 'react';
import Calendar from "../model/Calendar";
import CalendarType from "../constants/CalendarType";
import changeSelectedDate from "../actions/changeSelectedDate";

const getWeekDatesRange = (selectedDate) => {
    const firstDay = selectedDate.clone().startOf('week');
    const lastDay = selectedDate.clone().endOf('week');
    return firstDay.format('D.MM.YYYY') + '-' + lastDay.format('D.MM.YYYY');
}

export default ({selectedDate}) => {
    const activeType = Calendar.activeType;
    const clickNext = async (selectedDate) => {
        const step = activeType === CalendarType.WEEK ? 'week' : 'month';
        await changeSelectedDate(selectedDate, '1', step);
    }

    const clickPrev = async (selectedDate) => {
        const step = activeType === CalendarType.WEEK ? 'week' : 'month';
        await changeSelectedDate(selectedDate, '-1', step);
    }

    const datesRange = activeType === CalendarType.WEEK
        ? getWeekDatesRange(selectedDate)
        : selectedDate.format('MMMM YYYY');

    return (
        <React.Fragment>
            <div className="col-auto pad-bot">
                <div className="prev-next">
                    <i className="cil-chevron-left"
                       onClick={() => clickPrev(selectedDate)}></i>
                    <i className="cil-chevron-right"
                       onClick={() => clickNext(selectedDate)}></i>
                </div>
            </div>
            <div className="col-auto pad-bot">
                <p className="h5 selected-month">{datesRange}</p>
            </div>
        </React.Fragment>
    );
}