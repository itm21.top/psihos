import React from "react";
import Timeline from "./Timeline";
import TimelineCurrent from "./TimelineCurrent";
import {toJS} from "mobx";
import Meeting from "../../../Meeting/models/Meeting";
import MeetingSticker from "../MeetingSticker";
import {observer} from "mobx-react";
import moment from "moment";

export default observer(({day, activeMonthNumber}) => {
    const inactiveDay = activeMonthNumber !== day.format('M');
    const weekDay = moment(day).weekday();
    const dayClass = 'day-of-week '
        + (weekDay === 5||weekDay === 6 ? ' weekend ' : '')
        + (day.format('YYYYMMDD') === moment().format('YYYYMMDD') ? ' current ' : '')
        + (inactiveDay ? 'inactive' : '');

    const isActiveDay = day.format('YYYYMMDD') === moment().format('YYYYMMDD');
    const meetings = toJS(Meeting.listByDays)[day.format('YYYYMMDD')] ?? [];
    return (
        <div className="col-md-6 col-day-of-week">

            <div className={dayClass}>
                <Timeline />
                <TimelineCurrent isActiveDay={isActiveDay} />
                <div className="day-of-week-head">
                    <p className="daymonth">{day.format('DD')}<span className="dayweek"> {day.format('ddd')}</span></p>
                </div>
                {meetings.map((meeting, key) =>
                    <MeetingSticker meeting={meeting} key={key} />
                )}
            </div>
        </div>
    )
}
)