import React from 'react';
import moment from "moment";

export default ({isActiveDay}) => {
    const onePercentInM = 24 * 0.6;
    const yPercent = ((Number(moment().format('HH')))*60 + Number(moment().format('mm')))/onePercentInM;


    return (
        <React.Fragment>
            <div className="time-of-day-current" style={{top: `${yPercent}%`, width: isActiveDay ? '100%' : 0}}>
                <span>{moment().format('HH:mm')}</span>
            </div>
        </React.Fragment>
    )
}