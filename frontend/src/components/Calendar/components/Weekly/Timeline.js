import React from 'react';


export default () => {
    const timeline = [];
    for (let i = 0; i <= 24; i++) {
        timeline.push(i + ':00');
    }
    return (
        <React.Fragment>
            <div className="time-of-day">
                {timeline.map((time, key) => <p key={key} className="">{time}</p>)}
            </div>
        </React.Fragment>
    )
}