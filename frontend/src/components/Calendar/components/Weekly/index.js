import React from 'react';
import WeekDay from "./WeekDay";

export default ({selectedDate}) => {
    const firstDay = selectedDate.clone().startOf('week');
    let daysOfWeek = [];
    for (let i = 1; i <= 7; i++) {
        daysOfWeek.push(firstDay.clone());
        firstDay.add(1, 'days');
    }
    return (
        <div className="row row-day-of-week">
            {daysOfWeek.map((day, key) =>
                <WeekDay key={key} day={day} />
            )}
        </div>
    )
}