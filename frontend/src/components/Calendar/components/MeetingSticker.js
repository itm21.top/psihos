import React from 'react';
import editMeeting from "../../Meeting/actions/editMeeting";
import Calendar from "../model/Calendar";
import CalendarType from "../constants/CalendarType";
import isTutor from "../../../common/auht/selectors/isTutor";


export default ({meeting}) => {
    const date = meeting.meetingDate;

    const onePercentInM = 24 * 0.6;
    const yPercentTop = ((Number(date.format('HH'))) * 60 + Number(date.format('mm'))) / onePercentInM;
    const pxInM = 50 / 60;
    const height = Number(meeting.durationMeetingInMinutes) * pxInM;
    const meetingStickerClass = 'sticker '
        + (meeting.type === 'online' ? 'online' : 'offline') + (height <= 50 ? ' short' : '');
    const meetingStickerClassAll = meetingStickerClass + (meeting.paymeet === 1 ? ' paymeet' : '');
    const meetingStickerClassAll2 = meetingStickerClassAll + (meeting.freemeet === 1 ? ' freemeet' : '');

    return (
        <div className={meetingStickerClassAll2} onClick={() => isTutor() && editMeeting(meeting.id)}
             style={{top: `${yPercentTop}%`, height: `${height}px`}}>
            <div className="sticker-overflow">
                <p className="sticker-head">{meeting.client.full_name}</p>
                {Calendar.activeType === CalendarType.WEEK &&
                <React.Fragment>
                    <p className="time-of-meeting">{date.format('HH:mm')} - {date.clone().add(meeting.duration, 'minutes').format('HH:mm')}</p>
                </React.Fragment>
                }
            </div>
        </div>
    )
}