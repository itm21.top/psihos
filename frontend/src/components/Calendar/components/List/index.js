import React, {useEffect} from 'react';
import DataTable from "../../../../common/dataTable";
import listConfig from "./listConfig";
import fetchDatatable from "../../actions/fetchDatatable";
import Calendar from "../../model/Calendar";
import {observer} from "mobx-react";

export default observer(() => {
    useEffect(() => {
        fetchDatatable();
    }, []);
    return (
        <React.Fragment>
            <DataTable config={listConfig}
                       fetch={fetchDatatable}
                       data={Calendar.meetingsList}
                       total={Calendar.meetingsListTotal}
            />
        </React.Fragment>
    )
})