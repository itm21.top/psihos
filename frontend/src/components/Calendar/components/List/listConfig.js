import React from "react";
import editMeeting from "../../../Meeting/actions/editMeeting";
import MeetingCol from "../../../Clients/List/Cols/MeetingCol";
import Avatar from "../../../Layouts/components/Avatar/Avatar";
import makeTableConfig from "../../../../common/dataTable/makeTableConfig";
import getClientRole from "../../../Clients/models/selectors/getClientRole";
import moment from 'moment';
import isTutor from "../../../../common/auht/selectors/isTutor";

const config = makeTableConfig({
    columns: [
        {
            data: 'client.full_name',
            name: 'Клиент',
            props: {
                width: '25%',
            },
            searchable: true,
            orderable: true,
            render: (row) => <div className="row col-12">
                <Avatar avatar={row?.client?.avatar}
                        name={row.client.full_name}
                        role={getClientRole(row)}
                />
            </div>
        },
        {
            data: 'meetings.scheduled_for',
            name: 'Bстреча',
            props: {
                width: 'auto',
            },
            searchable: false,
            orderable: true,
            render: (row) => <MeetingCol meeting={row}/>
        },
        {
            data: 'files_count',
            name: 'Файлы',
            props: {
                width: '100px',
            },
            orderable: true,
            render: (row) => row.files_count
        },

    ],
    order: [{column: 1, dir: 'asc'}],
    start: 0,
    length: 10,
    onRowClick: (row) => isTutor() && editMeeting(row.id),
    search: {
        value: "",
        regex: false
    },
    filters: {
        from: moment().format('YYYY-MM-DD'),
        to: moment().format('YYYY-MM-DD'),
    }
})

export default config;