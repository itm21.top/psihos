import React from 'react';
import Day from "./Day";

export default ({selectedDate}) => {
    const activeMonthNumber = selectedDate.format('M');
    const startOfMonth = selectedDate.startOf('month').startOf('week');
    let daysOfMonth = [];
    for (let i = 1; i <= 35; i++) {
        daysOfMonth.push(startOfMonth.clone());
        startOfMonth.add(1, 'days');
    }

    return (
        <React.Fragment>
            <div className="row row-day-of-month">
                {daysOfMonth.map((day, key) => <Day key={key} day={day} activeMonthNumber={activeMonthNumber} />)}
            </div>
        </React.Fragment>
    )
}