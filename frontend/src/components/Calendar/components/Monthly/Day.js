import React from 'react';
import moment from "moment";
import {observer} from "mobx-react";
import Meeting from "../../../Meeting/models/Meeting";
import {toJS} from "mobx";
import MeetingSticker from "../MeetingSticker";

export default observer(({day, activeMonthNumber}) => {
    const inactiveDay = activeMonthNumber !== day.format('M');
    const weekDay = moment(day).weekday();
    const dayClass = 'day-of-month '
        + (weekDay === 5 || weekDay === 6 ? ' weekend ' : '')
        + (day.format('YYYYMMDD') === moment().format('YYYYMMDD') ? ' current ' : '')
        + (inactiveDay ? 'inactive' : '');

    const meetings = toJS(Meeting.listByDays)[day.format('YYYYMMDD')] ?? [];
    return (
        <div className="col-sm-6 col-md-4 col-day-of-month">
            <div className={dayClass}>
                <div className="day-of-month-head">
                    <span className="day-inside day-of-week">{day.format('ddd')}</span>
                    <span className="day-inside number">
                        {day.format('DD')}
                        <span className="month">{day.format('MMM')}</span>
                    </span>
                </div>
                <div className="day-of-month-body">
                    {!inactiveDay &&
                        meetings.map((meeting, key) =>
                            <MeetingSticker meeting={meeting} key={key} />
                    )}
                </div>
            </div>
        </div>
    )
});