import React from 'react';
import CalendarType from "../constants/CalendarType";
import WeeklyCalendar from "./Weekly";
import MonthlyCalendar from "./Monthly";
import ListCalendar from "./List";

export default ({activeType, selectedDate}) => {
    switch (activeType) {
        case CalendarType.WEEK:
            return <WeeklyCalendar selectedDate={selectedDate.clone()} />;
            break;
        case CalendarType.LIST:
            return <ListCalendar selectedDate={selectedDate.clone()} />;
            break;
        case CalendarType.MONTH:
        default:
            return <MonthlyCalendar selectedDate={selectedDate.clone()} />
    }
}