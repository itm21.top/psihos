import React, {useEffect} from 'react';
import './calendar.scss'
import Card from "../../common/card/Card";
import SelectInput from "../../common/forms/components/SelectInput";
import 'moment/locale/ru';
import {observer} from "mobx-react";
import Meeting from "../Meeting/models/Meeting";
import AddMeeting from "../Meeting/AddMeeting";
import Client from "../Clients/models/Client";
import Form from "../Clients/Form";
import createMeeting from "../Meeting/actions/createMeeting";
import Calendar from "./model/Calendar";
import selectCurrentDate from "./actions/selectCurrentDate";
import changeCalendarType from "./actions/changeCalendarType";
import CalendarTypes from "./constants/CalendarTypes";
import CalendarBody from "./components/CalendarBody";
import CalendarDatePicker from "./components/CalendarDatePicker";
import fetchMeetingsByPeriod from "./actions/fetchMeetingsByPeriod";
import CalendarType from "./constants/CalendarType";
import fetchAvaliableClients from "./actions/fetchAvaliableClients";
import fetchDatatable from "./actions/fetchDatatable";
import isTutor from "../../common/auht/selectors/isTutor";
import addNewClient from "../Clients/actions/addNewClient";
import Feetback from "../Feetback/models/Feetback";
import Form2 from "../Feetback/Form";
import moment from "moment";
import User from "../../common/models/User";

export default observer(() => {
    const selectedDate = Calendar.selectedDate;
    const activeType = Calendar.activeType;


    const fetchCallback = async () => {
        const step = activeType === CalendarType.WEEK ? 'week' : 'month';
        await fetchMeetingsByPeriod(selectedDate, step);
        await fetchDatatable();
    }

    useEffect(() => {
        const firstDay = selectedDate.clone().startOf('week');
        console.log(firstDay);
        const step = activeType === CalendarType.WEEK ? 'week' : 'month';
        fetchMeetingsByPeriod(firstDay, step);
        fetchAvaliableClients();
    }, []);

    let tempdate = new Date(User.current.date_payment);
    const datelimit = moment(tempdate, 'YYYY-MM-DDTHH:mm:ss.SSSZ');
    const now = moment();
    console.log(datelimit);
    console.log(now);
    console.log(now.isAfter(datelimit));

    return (
        <React.Fragment>
            <Card className="calendar">
                <div className="common-card-sticky-head">
                    <div className="row-calendar-head row">
                        <div className="col-xl-auto">
                            <p className="h3">Календарь</p>
                        </div>
                        <div className="input-select-wrapper col-sm-auto pad-bot cal-mob">
                            <SelectInput options={CalendarTypes}
                                         value={CalendarTypes.find(type => type.value === activeType)}
                                         onChange={type => changeCalendarType(type)}
                                         placeholder={'Выбрать...'}
                            />
                        </div>
                        <div className="col-auto pad-bot">
                            <div className="button-style-gray" onClick={selectCurrentDate}>Сегодня</div>
                        </div>
                        <CalendarDatePicker selectedDate={selectedDate}/>
                        <div className="input-select-wrapper col-sm-auto pad-bot cal-desc">
                            <SelectInput options={CalendarTypes}
                                         value={CalendarTypes.find(type => type.value === activeType)}
                                         onChange={type => changeCalendarType(type)}
                                         placeholder={'Выбрать...'}
                            />
                        </div>
                        {isTutor() && !now.isAfter(datelimit) &&
                        <div className="col-auto pad-bot">

                            <button type="button"
                                    onClick={() => createMeeting()}
                                    className="button button-style">
                                + &nbsp; Создать встречу
                            </button>

                            <button type="button"
                                    onClick={() => addNewClient()}
                                    className="button button-style but">
                                + &nbsp; Добавить клиента
                            </button>
                        </div>
                        }
                    </div>
                </div>

                <div>
                    <CalendarBody selectedDate={selectedDate} activeType={activeType}/>
                </div>
            </Card>
            {Meeting.isShowForm && <AddMeeting saveCallback={fetchCallback} clientExists={false}/>}
            {Client.isShowForm && <Form saveCallback={fetchCallback} clientExist={false}/>}
            {Feetback.isShowForm && <Form2 saveCallback={fetchCallback} clientExist={false}/>}
        </React.Fragment>
    )
})
