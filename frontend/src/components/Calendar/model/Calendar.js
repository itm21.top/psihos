import {observable} from "mobx";
import moment from "moment";
import CalendarType from "../constants/CalendarType";

const Calendar = observable({
    selectedDate: moment(),
    activeType: CalendarType.WEEK,
    meetingsList: [],
    meetingsListTotal: 0,
});

export default Calendar;

