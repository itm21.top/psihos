cd backend
echo -e '\033[42m[Run->]\033[0m Build PHP'
cp .env.local .env
docker run --rm -v $(pwd):/app composer install --ignore-platform-reqs
cd ..